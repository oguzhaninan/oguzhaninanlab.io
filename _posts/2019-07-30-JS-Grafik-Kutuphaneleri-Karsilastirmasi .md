---
layout: post
title: "JS Grafik Kütüphaneleri"
author: "Oğuzhan İNAN"
date: 2019-07-30
categories: js chart compare library
---

## İçerik
1. Kütüphaneler
    1. [amCharts](#amCharts)
    1. [AnyChart](#AnyChart)
    1. [ApexChart.JS](#ApexChart)
    1. [C3.js](#C3.js)
    1. [canvasjs](#canvasjs)
    1. [Chart.js](#chartjs)
    1. [Chartist.js](#Chartist.js)
    1. [D3.js](#D3.js)
    1. [EJSChart](#ejschart)
    1. [FusionCharts](#fusioncharts)
    1. [Google Charts](#googlechart)
    1. [Highcharts](#highcharts)
    1. [KoolChart](#KoolChart)
    1. [NVD3.js](#nvd3)
    1. [Plotly.js](#plotly)
    1. [Plottable](#plottablejs)
    1. [ZingChart](#zingchart)
1. [Genel Karşılaştırma](#genel-karsilastirma)
   
<a name="amCharts"></a> 
### 1) [amCharts](https://www.amcharts.com/){:target="_blank"}
![amchart]({{ "/assets/img/amchart.png" | absolute_url }})
Gelişmiş veri görselleştirmesi için hem çizelgeler hem de coğrafi haritalara sahip. Kullanımı kolay özellikle harita grafikleri için desteği geniş.    

Pek çok hazır harita var ve siz de kendinizinkini oluşturabiliryorsunuz. amCharts, tüm modern tarayıcılarda çalışan grafikler oluşturmak için SVG kullanıyor. TypeScript, Angular, React, Vue ve düz JavaScript uygulamaları ile entegrasyona ve wordpress eklentisine sahip. Anlaşılır ve geniş bir dokümantasyonu var. Ücretsiz ve paralı planları bulunuyor.

<a name="AnyChart"></a> 
### 2) [AnyChart](https://www.anychart.com/){:target="_blank"}
![AnyChart]({{ "/assets/img/anychart.png" | absolute_url }})
AnyChart, gömülü ve bütünleşik olarak tasarlanmış grafiklere sahip, hafif bir JavaScript grafik kütüphanesidir. AnyChart, 60+ grafik türüne sahip ve kendi grafik türlerinizi oluşturmak için özellikler sunuyor. Grafiği PDF, PNG, JPG veya SVG formatında kaydedebilirsiniz.   

Grafikler üzerinde deneme yapabileceğiniz bir [playgorund'a](https://playground.anychart.com/) sahip. Öğrenmek için anlaşılır ve geniş bir dokümantasyonu var. Kayıttan sonra filigranlı sürümünü ücretsiz olarak indirebilirsiniz. Ancak, filigrandan kurtulmak ve AnyChart'ı ticari amaçla kullanmak için bir lisans satın almanız gerekir.

<a name="ApexChart"></a> 
### 3) [ApexChart.JS](https://apexcharts.com/){:target="_blank"}
![ApexChart]({{ "/assets/img/apexchart.png" | absolute_url }})
Grafiklerin entegrasyonu, kapsamlı API dokümanları ve kullanıma hazır 100+ örnek ile hızlıca kullanmaya başlayabilirsiniz.   

Tam bir responsive desteğe ve grafiklerin görünümünü değiştirmek için ayrıntılı desteğe sahip. Çoğu kütüphaneye göre daha performanslı. Annotation ve smooth animasyon desteğine sahip. Açık kaynaklı ve ücretsiz bir kütüphane. 

<a name="C3.js"></a> 
### 4) [C3.js](https://c3js.org/){:target="_blank"}
![C3]({{ "/assets/img/c3js.png" | absolute_url }})
Web uygulamaları için D3 tabanlı tekrar kullanılabilir bir grafik kütüphanesidir. Kütüphane, her öğeye sınıflar sağlar, böylece sınıfa göre özel bir stil tanımlayabilir ve yapıyı doğrudan D3 ile genişletebilirsiniz.   

Ayrıca grafiğin durumuna erişmek için çeşitli API'ler ve __callback__ sağlar. Bunları kullanarak, grafiği oluşturulduktan sonrada güncelleyebilirsiniz. Ayrıntılı bir dokümantasyonu ve hazır birçok örneği var. Açık kaynak ve ücretsiz bir kütüphane. 

<a name="canvasjs"></a> 
### 5) [canvasjs](https://canvasjs.com/){:target="_blank"}
![canvasjs]({{ "/assets/img/canvasjs.png" | absolute_url }})
Adından da anlaşılacağı gibi Canvas.js, Canvas öğesini temel alan bir HTML5 - JavaScript grafik kütüphanesidir. Canvas, tüm cihaz türleri arasında iyi görünen zengin grafikler oluşturmanıza olanak sağlar.   

SVG grafik kütüphanelerine göre 10 kat daha hızlı olduklarını iddaa ediyorlar. Birçok hazır örneğe ve ayrınıtılı dokümana sahip, ücretsiz ve açık kaynaklı bir kütüphane.

<a name="Chart.js"></a> 
### 6) [Chart.js](https://www.chartjs.org/){:target="_blank"}
![Chart.js]({{ "/assets/img/chartjs.jpeg" | absolute_url }})
Chart.js, 8 grafik türünü destekleyen açık kaynaklı bir JavaScript kütüphanesidir. 60kb'dan küçüktür. Desteklediği türler çizgi grafikler, çubuk grafikler, alan grafikleri, radar, pasta grafikleri, kabarcık, dağılım grafikleri, karışık grafileri ve zaman serisi de desteklenmektedir. Grafik oluşturma için canvas öğesini kullanır ve responsive için pencere yeniden boyutlandırıldığında grafik boyutu güncellenir.   

IE9 ile uyumludur. IE7 ile de çalışmak için polyfills mevcuttur. Gerçek zamanlı olarak seri veya veri noktaları eklerken sorunsuzca güncellenir. Grafik seçenekleri değiştirildikten sonra değiştirilebilir ve bir __update()__ işlevi çağrılarak grafiği yeniden çizer. Yapılandırma seçenekleri, çizelgeleri oluşturmak ve değiştirmek için kullanılır. 

Chart.js açık kaynak kodlu bir kütüphanedir ve ücretsizdir. Sınırlı tür sayısı, daha gelişmiş garfik gereksinimleri için uygun olmayabilir.

<a name="Chartist.js"></a> 
### 7) [Chartist.js](https://gionkunz.github.io/chartist-js/){:target="_blank"}
![Chartist.js]({{ "/assets/img/chartist.png" | absolute_url }})
Grafikleri oluşturmak için SVG kullanır. 10KB (GZIP) boyutunda ve harhangi bir bağımlılığı olmayan hafif bir grafik kütüphanesidir. Grafik tasarımı Sass ile kontrol edilebilir ve özelleştirilebilir. Ayrıca, Chartist.js'nin modern tarayıcılarda çalışacak harika animasyonlara sahip. Anlaşılır bir dokümantasyonu var ve React, Angular, Meteor vb. entegrasyonlara sahip. 

<a name="D3.js"></a> 
### 8) [D3.js](http://d3js.org/){:target="_blank"}
![D3.js]({{ "/assets/img/d3js.png" | absolute_url }})
Grafik kütüphaneleri arandığında karşımıza ilk çıkan isim D3.js. Açık kaynak kodlu bir proje olan D3.js, mevcut kütüphanelerin çoğunda eksik olan birçok özelliğe sahip. D3.js içindeki grafikler HTML, SVG ve CSS ile oluşturulmaktadır.   

Diğer birçok JavaScript kütüphanesinden farklı olarak, D3.js önceden oluşturulmuş grafikleriyle birlikte kullanılmaz. Ancak, genel bir bakış için D3.js ile oluşturulan grafiklerin listesine bakabilirsiniz. D3.js çok çeşitli grafik türlerini desteklemektedir. Yeni başlayanlar için bir dezavantajı zor bir öğrenme eğrisi var, ancak başlamanıza yardımcı olacak birçok ders ve kaynak da var. D3.js, IE8 gibi eski tarayıcılarla iyi çalışmıyor. Ancak tarayıcılar arası uyumluluk için eklentiler kullanabilirsiniz.

<a name="ejschart"></a> 
### 9) [ejschart](http://www.ejschart.com/){:target="_blank"}
![ejschart]({{ "/assets/img/ejschart.png" | absolute_url }})
Grafikleri oluşturmak için canvas kullanan, IE6+ desteğe sahip ve herhangi bir bağımlılığa sahip olmayan bu kütüphane çok fazla bir komünite desteğine sahip değil ve dokümantasyonu da okadar iyi sayılmaz. Ama interaktif ve hızlı bir kütüphane olarak iş görebilir.  

EJSChart, XML formatındaki verileri destekler ve anında verileri yükleyebilir. Sayfa yeniden yüklenmeden yeni seri eklenebilir ve veriler gerçek zamanlı olarak güncellenebilir. Sitesinde bir çok örnek kod var. Başlangıç için hazır örnekler incelenerek hızlıca birşeyler yapılabilir. 

<a name="fusioncharts"></a>
### 10) [FusionCharts](https://fusionchart.com/){:target="_blank"}
![FusionCharts]({{ "/assets/img/fusioncharts.jpeg" | absolute_url }})
FusionCharts, grafikler için SVG kullanan, sağlam bir grafik kütüphanesidir. XML, JSON ve JavaScript dahil olmak üzere birçok veri formatını destekler, modern tarayıcılarda çalışır ve IE6'ya uyumludur. Birçok JavaScript çerçevesi ve sunucu tarafı programlama dili de desteklenmektedir. Vue, React, Angular, ReactNative, Ember, jQuery ile kullanılabilir.

Harita galerisi çok sayıda örnek içerir ve temiz bir görsel görünüme sahiptir. Belgeler, iyi API açıklamaları ve her bir grafik türünün örneklerini içerir. Grafikler konfigürasyon tabanlı seçenekler kullanılarak oluşturulmuştur ve kullanımı nispeten kolaydır. FusionCharts, grafiklere filigran eklenen hali ile kişisel kullanım için ücretsizdir. Ticari kullanım için ücretli lisans mevcuttur.
 
<a name="googlechart"></a>
### 11) [googlechart](https://developers.google.com/chart/){:target="_blank"}
![googlechart]({{ "/assets/img/googlechart.jpeg" | absolute_url }})
Google grafikleri güçlü ve kullanımı kolaydır. Her grafik türü, örneklerle ilgili özel bir öğreticiye sahiptir. Öğreticiler, ilgili özelliklerin kodunu ve API listelerini içerir. Bu, yeni bir grafik kütüphanesiyle başlamak için güzel bir seçenek. Grafikler, yapılandırma seçenekleri kullanarak özelleştirilebiliyor.   

Her bir grafik türü, türe özgü öğreticilerde listelenen benzersiz seçeneklere sahiptir. Google grafikleri ücretsizdir ancak kodu indirmeye izin vermiyor. Bir Google servisi olarak kullanabiliyorsunuz. [Ayrıntılı Bilgi](https://developers.google.com/chart/interactive/faq#localdownload)

<a name="highcharts"></a>
### 12) [highcharts](https://highcharts.com/){:target="_blank"}
![highcharts]({{ "/assets/img/highcharts.png" | absolute_url }})
Highcharts, bir başka popüler grafik kütüphanesidir ve diğerleri gibi HTML5/SVG'ye dayanmaktadır, Yani ek bir eklentiye gerek duymaz. Diğerleri arasında spline, sütun, bar, haritalar, açısal göstergeler gibi çok çeşitli grafikleri destekler.  

Çevrimiçi etkileşimli grafikler oluşturmak için Highcharts bulut adlı bir arabirim sağlar. Kişisel kullanım için tamamen ücretsizdir. Sadece ticari kullanım için bir lisans satın almanız gerekir. Ayrıntılı dokümantasyona ve bir çok hazır örneğe sahip.
 
<a name="koolchart"></a>
### 13) [koolchart](https://www.koolchart.com/){:target="_blank"}
![koolchart]({{ "/assets/img/koolchart.jpeg" | absolute_url }})
KoolChart HTML5 canvas tabanlı JavaScript grafik kütüphanesidir. Canvas kullanımı, daha iyi performans sunar. API, her özellik için örnek grafiklerle ayrıntılı olarak belgelenmiştir. Yaklaşık 200 sayfalık bir PDF kılavuzu da mevcuttur. İki aylık bir deneme süresi değerlendirme için kullanılabilir. Deneme süresi dolduktan sonra lisans almak gerekir.

<a name="nvd3"></a>
### 14) [nvd3](http://nvd3.org/){:target="_blank"}
![nvd3]({{ "/assets/img/nvd3.png" | absolute_url }})
NVD3, aynı güçlü özellikleri sağlayan, ancak kullanımı çok daha basit olan d3.js için yeniden kullanılabilir grafikler ve bileşenler oluşturmayı amaçlayan bir projedir. Karmaşık veri kümelerini yönetmenize ve gelişmiş görselleştirmeler oluşturmanıza olanak sağlar.

Web sitesinde yeterli saayıda örnek kod var. Hızlı bir başlangıç için örnek kodlar incelenebilir. Ücretsiz ve açık kaynaklı grafikleri oluşturmak için SVG kullanan, kullanımı kolay bir grafik kütüphanesi.

<a name="plotly"></a>
### 15) [plotly](https://plot.ly/javascript/){:target="_blank"}
![plotly]({{ "/assets/img/plotly.png" | absolute_url }})
Plotly.js, web için ilk bilimsel JavaScript grafik kitaplığıdır. 2015'ten beri açık kaynak kodlu,  ücretsiz olarak kullanılabilir. Plotly.js, SVG haritaları, 3D grafikler ve istatistiksel grafikler dahil olmak üzere 20 grafik türünü destekler.   

D3.js ve stack.gl üzerine inşa edilmiştir.
Plotly.js, ücretsiz ve açık kaynaklı bir üst düzey JavaScript kitaplığıdır. 3D grafikler dahil olmak üzere birçok farklı grafik türü oluşturmak için kullanılabilir.

<a name="plottablejs"></a>
### 16) [plottablejs](http://plottablejs.org/){:target="_blank"}
![plottablejs]({{ "/assets/img/plottablejs.png" | absolute_url }})
D3'ün üzerine inşa edilen Plottable, grafikler oluşturmak için birleştirebileceğiniz ve yeniden düzenleyebileceğiniz bir dizi esnek, hazır bileşen sağlar. Plottable düzen, boyutlandırma ve konumlandırma işlemlerini gerçekleştirdiğinden, Plottable grafiklerin oluşturulması yalnızca D3 ile oluşturulan grafiklerden daha kolaydır ve yeniden kullanılabilir.    

Sitesinde örnek kodlar mevcut ve ayrıntılı bir dokümantasyonu var. Ücretsiz olarak kullanabilirsiniz.

<a name="zingchart"></a>
### 17) [zingchart](http://zingchart.com/){:target="_blank"}
![zingchart]({{ "/assets/img/zingchart.png" | absolute_url }})
ZingChart hızlıca grafikler oluşturmak için esnek, etkileşimli, hızlı ve modern bir kütüphane. Ürünleri Apple, Microsoft, Adobe, Boeing ve Cisco gibi şirketler tarafından kullanılıyor ve mükemmel grafikler sunmak için hızlı bir şekilde Ajax, JSON, HTML5 kullanıyor. ZingChart, gerçek zamanlı olarak verileri de gösterebilen, 35'ten fazla duyarlı grafik türü ve modülü sunar.    
   
Popüler diller (.NET, PHP, Python, R ve Java) ve JS frameworkleri (Angular, Vue ve React) ve iOS ve Android için entegrasyonlar vardır.
  
CSS tarafından stillendirilebilir ve temalandırılabilir ve büyük verileri hızlı bir şekilde görüntüleyebilir. Ücretsiz olarak denemek için tüm grafiklere sahip filigran içeren bir sürüm mevcut, ancak filigran içermeyen çıktılar için, işletme boyutunuza göre ZingChart’ın ücretli lisanslarından birini satın almanız gerekir.

<a name="genel-karsilastirma"></a>
### Genel Karşılaştırma
![zingchart]({{ "/assets/img/chart_compare.png" | absolute_url }})

## amChart
<!-- <script async src="//jsfiddle.net/oguzhaninan/g643Lt2x/4/embed/result,js,html/"></script> -->
<p class="codepen" data-height="500" data-theme-id="dark" data-default-tab="result" data-user="oguzhaninan" data-slug-hash="KOWByj" data-preview="true" style="height: 500px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="amChart">
  <span>See the Pen <a href="https://codepen.io/oguzhaninan/pen/KOWByj/">
  amChart</a> by oguzhan (<a href="https://codepen.io/oguzhaninan">@oguzhaninan</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

## anyChart
<!-- <script async src="//jsfiddle.net/oguzhaninan/zx4bpkew/9/embed/result,js,html/"></script> -->
<p class="codepen" data-height="500" data-theme-id="dark" data-default-tab="result" data-user="oguzhaninan" data-slug-hash="JgWBLW" data-preview="true" style="height: 500px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="anychart">
  <span>See the Pen <a href="https://codepen.io/oguzhaninan/pen/JgWBLW/">
  anychart</a> by oguzhan (<a href="https://codepen.io/oguzhaninan">@oguzhaninan</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

## ApexChart
<!-- <script async src="//jsfiddle.net/oguzhaninan/gksL0d1u/2/embed/result,js,html/"></script> -->
<p class="codepen" data-height="550" data-theme-id="dark" data-default-tab="result" data-user="oguzhaninan" data-slug-hash="ymMqjL" data-preview="true" style="height: 550px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="apexchart">
  <span>See the Pen <a href="https://codepen.io/oguzhaninan/pen/ymMqjL/">
  apexchart</a> by oguzhan (<a href="https://codepen.io/oguzhaninan">@oguzhaninan</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

## c3.js
<!-- <script async src="//jsfiddle.net/oguzhaninan/ctxe62d9/3/embed/result,js,html/"></script> -->
<p class="codepen" data-height="500" data-theme-id="dark" data-default-tab="result" data-user="oguzhaninan" data-slug-hash="EqWpRN" data-preview="true" style="height: 500px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="c3js">
  <span>See the Pen <a href="https://codepen.io/oguzhaninan/pen/EqWpRN/">
  c3js</a> by oguzhan (<a href="https://codepen.io/oguzhaninan">@oguzhaninan</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

## plotly.js
<!-- <script async src="//jsfiddle.net/oguzhaninan/tx9pLg6u/1/embed/result,js,html/"></script> -->
<p class="codepen" data-height="500" data-theme-id="dark" data-default-tab="result" data-user="oguzhaninan" data-slug-hash="zgZLzr" data-preview="true" style="box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="plotly.js">
  <span>See the Pen <a href="https://codepen.io/oguzhaninan/pen/zgZLzr/">
  plotly.js</a> by oguzhan (<a href="https://codepen.io/oguzhaninan">@oguzhaninan</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>
