---
layout: post
title: "Konteyner İşletim Sistemleri"
author: "Oğuzhan İNAN"
date: 2019-09-16
categories: docker os ubuntu alpine busybox rancheros
---

![docker]({{ "/assets/img/container-os.svg" | absolute_url }})

Bir konteyner dağıtımı durumunda geliştiriciler, hangi işletim sistemi özelliklerinin ve işlevlerinin yayınlanacak uygulamalar için kritik önem taşıdığını ve bir işletim sistemini neden seçmesi gerektiğine doğru karar vermelidir.

Aşağıdaki 3 temel işletim sistemi türününden ikişer örnek vericeğiz:

- Tam özellikli işletim sistemleri
- Genel amaçlı minimal işletim sistemleri
- Konteynere özgü işletim sistemleri

## Tam Özellikli İşletim Sistemleri

Bilinmesi gereken ilk şey, bu işletim sistemlerinin her şeyi yapabildiğidir. Bir uygulamanın bir özellik veya işleve ihtiyacı varsa, bunlardan birinin buna sahip olması muhtemeldir. Bu "hazırlıklı" esnekliğin bir maliyeti var: bu işletim sistemleri, depolama, bellek ve CPU kaynakları söz konusu olduğunda, çok fazla kaynak kullanıyor. Bu özellikler aynı zamanda işletim sisteminin saldırı yüzeyini de arttırır ve potansiyel saldırganlara işlerini yapacakları çok daha fazla alan sağlar. 

### Ubuntu
![ubuntu]({{ "/assets/img/ubuntu-logo.jpg" | absolute_url }})

[Ubuntu](https://ubuntu.com/), birçok kuruluş için sunucu, bulut ve hatta masaüstünde varsayılan işletim sistemi haline geldi. [Canonical](https://canonical.com/) tarafından iyi bir şekilde desteklenen Ubuntu, IoT'yi, konteynerleri, sunucuları veya bulutları destekleyen dağıtımlar için gerekli olan yardımcı paketler, işlevler ve özellikler ile birlikte çeşitli indirilebilir biçimlerde mevcuttur.

### Centos
![centos]({{ "/assets/img/centos.png" | absolute_url }})

CentOS [Red Hat](https://www.redhat.com) firmasının dağıtımı olan Red Hat Enterprise Linux kaynak kodları üzerine kurulu ve bu dağıtım ile uyumlu bir linux dağıtımıdır.     

Başka bir "güvenli" seçim için açık kaynaklı, topluluk odaklı diğer alternatiftir. CentOS’u büyük kuruluşlarda, laboratuvarlarda ve büyük bulut sağlayıcılardaki sunucularda bulabilirsiniz. Ancak Ubuntu, daha eski olmasına rağmen çok iyi test edilmiş paketleri içeren CentOS'tan daha hızlı güncellemeler yayınlar.

## Minimal İşletim Sistemleri

Konteynerler, eksiksiz bir uygulama oluşturmak için bir araya getirilen az işleve sahip yapılar olarak ortaya çıktı. "Tam Özellikli" Linux dağıtımlarında bulunan hangi özellikler bu minimum işletim sistemlerinde eksik - ve uygulamanız için önemli mi? Bunları kullanmanın avantajları nedir?

Planlı bir hazırlık yapılmadan, minimal işletim sistemlerinden beklenen boyut ve karmaşıklık avantajları, belirli uygulamalar için gereken yardımcı programların, işlevlerin ve uygulamaların eklenmesiyle kaybedilebilir.

[BusyBox](https://busybox.net/) ve [Alpine Linux](https://alpinelinux.org/) kullanarak doğru koşullarda getirebilecekleri avantajlara bakalım. İki işletim sistemi birbiriyle ilgilidir - Alpine BusyBox'a dayanıyor - ancak belirli dağıtımlar için diğerini seçmesye yol açabilecek temel farklılıklar var. Bu farklılıklar sadece belirli yetenekleri değil aynı zamanda her birinin destek topluluğu ve ekosistemini içerir.

### BusyBox
BusyBox, konteyner dağıtımları için kullanışlıdır, çünkü konteynerler düşünülerek tasarlanmamıştır. Geliştiricileri tarafından “İsviçre Gömülü Linux Ordusu Bıçağı” (The Swiss Army Knife of Embedded Linux) olarak adlandırılan BusyBox, çoğu gömülü uygulamanın gerektirdiği tüm işlevselliği içeren, küçük ve tek kullanımlık bir yürütülebilir dosya olarak tasarlandı.

BusyBox, tek bir çalıştırılabilir dosyada birkaç Unix yardımcı programı sağlayan bir yazılım paketidir. Linux, Android ve FreeBSD gibi çeşitli POSIX ortamlarında çalışır, ancak sağladığı araçların çoğu Linux çekirdeği tarafından sağlanan arabirimlerle çalışmak üzere tasarlanmıştır. 

### Alpine Linux
![alpine]({{ "/assets/img/alpine.png" | absolute_url }})

Alpine Linux, güvenlik, basitlik ve kaynak verimliliği için tasarlanmış [musl](https://www.musl-libc.org/) ve BusyBox tabanlı bir Linux dağıtımıdır. Güçlendirilmiş bir çekirdeği kullanır ve tüm kullanıcı alanı ikili dosyalarını yığın çökertme korumasına sahip pozisyondan bağımsız çalıştırılabilirler olarak derler.

Alpine Linux ayrıca geliştiricilerin BusyBox'tan daha fazla işlevsellik eklemesini kolaylaştırıyor. Geliştiricilere işlevsellik eklemeye ve kompakt dağıtım paketleri oluşturmaya izin veriyor. Bu, dağıtım için çok küçük konteyner imajları üretebilen minimum bir işletim sistemidir ve güçlendirilmiş çekirdek, üretim ve geliştirme, dağıtım için uygun hale getirir.

## Konteyner İşletim Sistemleri
Bir konteyner işletim sistemi, yerleşik olarak otomasyon ve konteyner düzenleme ile birlikte gelir. Alpine ve BusyBox gibi konteyner işletim sistemlerinin barındırıldığı işletim sistemi "host" işletim sistemleri olarak tasarlanır ve üretilir.

Konteynır işletim sistemleri, sadece konteynerleri destekleyen bir yazılım olmakla kalmaz, aynı zamanda konteynerler kullanılarak dağıtılan yazılımlarla da ayırt edilir.

[Rancher OS](https://rancher.com/rancher-os/) ve [Container Linux](https://coreos.com/os/docs/latest/), konteyner işletim sistemi arayanlar için iki ana seçenek. Her birinin özelliklerini anlamak, geliştiricilerin sahip oldukları avantajları ve tek mantıklı tercihleri ​​olan durumları anlamalarına yardımcı olacak.

### Rancher OS
![rancher]({{ "/assets/img/rancher.png" | absolute_url }})

[RancherOS](https://rancher.com/rancher-os/) içindeki her işlem Docker tarafından yönetilen ayrı bir kapta gerçekleştirilir. Docker için optimizasyon ve bağımlılık RancherOS'un çok hızlı bir önyükleme süresiyle çok küçük olmasını sağlar. Temel performans avantajlarının ötesinde, RancherOS'un lehine olabilecek konuşlandırma faktörleri var.

RancherOS sistem servisleri [Docker Compose](https://docs.docker.com/compose/) tarafından tanımlanmakta ve yapılandırılmaktadır. Bu güven, yalnızca uygulama için ihtiyaç duyulan hizmetlerin yüklenmesini ve kullanılmasını, dağıtımın daha da hızlandırılmasını ve basitleştirilmesini sağlar.

### Container Linux
![containerlinux]({{ "/assets/img/containerlinux.png" | absolute_url }})

[CoreOS](https://coreos.com/)’un [Container Linux](https://coreos.com/os/docs/latest/)'u bulut bazında konteyner dağıtımı için tasarlanmıştır. Artık Red Hat'in bir parçası olan Container Linux, genel veya özel bulut altyapıları arasında küme dağıtımları için optimize edilmiştir.

Container Linux, çekirdek ve temel yardımcı programlarla, diğer tüm yardımcı programlarla ve konteynerlerde dağıtılan işlevlerle tek bir çalıştırılabilir dosyada dağıtılır.

Konteyner Linux uzun zamandır geniş bir kullanım alanına sahiptir ve çoğu genel bulutun üzerinde konuşlandırmak için destek sağlamaktadır. Red Hat tarafından satın alınması, yayılmasını yavaşlatmadı ve bazı kurumları kullanmak için daha rahat hale getirdi. Konteyner Linux açık kaynaklı bir lisans ile dağıtılmaktadır ve aktif bir geliştirici topluluğuna sahiptir.

[Kaynak 1](https://en.wikipedia.org/wiki/BusyBox)     
[Kaynak 2](https://tr.wikipedia.org/wiki/CentOS)     
[Kaynak 3](https://rancher.com/blog/2019/comparison-of-container-operating-systems)    
[Kaynak 4](https://en.wikipedia.org/wiki/Musl)    
