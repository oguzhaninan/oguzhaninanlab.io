---
layout: post
title: "TestNG Group Test Nedir"
author: "Oğuzhan İNAN"
date: 2018-01-28
categories: java testng
---

Grup testi JUnit frameworkünde bulunmayan yeni bir özelliktir.
Metotları uygun şekilde gruplamaya ve karmaşıklığı azaltmaya yarar.
@Test açıklamasının `groups` parametresine grup isimleri yazılır. Birden fazla grup verilebilir.

`@BeforeGroups` ve `@AfterGroups` açıklamalarına isim verirken `@BeforeGroups("isim")` şeklinde verildiğinde hata alınmaz fakat çalışmazlar.
Bunun yerine `@BeforeGroups(groups = "isim")` şeklinde kullanılmalıdır.

### Metotlarla ilgili Gruplar

```java
public class TestGroup {

    @BeforeGroups(groups = "veritabani")
    public void veritabaniBaglan() {
        System.out.println("Veritanına Bağlan()");
    }

    @AfterGroups(groups = "veritabani")
    public void veritabaniBaglantiBitir() {
        System.out.println("Veritabanı Bağlantısı Bitir");
    }

    @Test(groups = "islem")
    public void islem1() {
        System.out.println("Islem1()");
    }

    @Test(groups = "islem")
    public void islem2() {
        System.out.println("Islem2()");
    }

    @Test(groups = "veritabani")
    public void oracleDbBaglan() {
        System.out.println("oracleDbBaglan()");
    }

    @Test(groups = "veritabani")
    public void mysqlDbBaglan() {
        System.out.println("mysqlDbBaglan()");
    }

    @Test(dependsOnGroups = {"veritabani", "islem"})
    public void sonIslem() {
        System.out.println("Son Islem");
    }

}
```

`sonIslem` metodunun __@Test__ açıklamasına verilen `dependsOnGroups` parametresi belirtilen gruplar sorunsuz çalışmadan metodun çalışmayacağını belirtir. `sonIslem` metodu `veritabani` ve `islem` grubuna dahil metodlara bağımlıdır.


textng.xml dosyası.

```xml
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd">
<suite name="Testler">
    <test name="islem">
        <groups>
            <run>
                <include name="islem" />
                <include name="veritabani"/>
            </run>
        </groups>
        <classes>
            <class name="com.testng.app.TestGroup" />
        </classes>
    </test>
</suite>
```

Çıktı.

```
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running TestSuite
Islem1()
Islem2()
Veritanına Bağlan()
mysqlDbBaglan()
oracleDbBaglan()
Veritabanı Bağlantısı Bitir
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.303 
```

### Sınıflardaki Gruplar

Sınıf düzeyind grup tanımlanabilir. `TestCase` sınıfının bütün metotları islem grubuna aittir.
```java 
@Test(groups = "islem")
public class TestCase {

    public void islem1() {
        System.out.println("Islem1()");
    }

    public void islem2() {
        System.out.println("Islem2()");
    }
}
```

```xml
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >
<suite name="Testler">
    <test name="testgroup">
        <classes>
            <class name="com.testng.app.TestGroup"/>
            <class name="com.testng.app.TestCase"/>
        </classes>
    </test>

    <test name="textgroupexclude">
        <groups>
            <run>
                <exclude name="veritabani"/>
            </run>
        </groups>
        <classes>
            <class name="com.testng.app.TestGroup"/>
        </classes>
    </test>

    <test name="islem">
        <groups>
            <run>
                <include name="islem"/>
            </run>
        </groups>
        <classes>
            <class name="com.testng.app.TestGroup"/>
            <class name="com.testng.app.TestCase"/>
        </classes>
    </test>

</suite>

```
`testgroup` isimli testteki bütün test metotları çalıştı.

`textgroupexclude` isimli testteki `exclude` etiketiyle belirttiğimiz `veritabani` grubuna sahip testler __çalışmadı.__

`islem` adlı testte ise `include` etiketiyle belirttiğimiz `islem` grubuna sahip testler çalıştı.

```
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running TestSuite
// testgroup isimli test
IslemCase1()
IslemCase1()
Islem1()
Islem2()
Veritanına Bağlan()
mysqlDbBaglan()
oracleDbBaglan()
Veritabanı Bağlantısı Bitir
Son Islem
// testgroupexclude isimli test
Islem1()
Islem2()
// islem isimli test
IslemCase1()
IslemCase1()
Islem1()
Islem2()
Tests run: 14, Failures: 0, Errors: 0, Skipped: 1, Time elapsed: 0.386 sec
```


