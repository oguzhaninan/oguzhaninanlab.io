---
layout: post
title: "Linux Dosya Sistemi Düzeni"
author: "Oğuzhan İNAN"
date: 2018-01-29
categories: linux
---

## Bölme Tabloları (Partition Tables)
Her mimari, disk sürücülerini bölümlere ayırma ve önyükleme kodunu (Boot Code) uygun yere yerleştirme konusunda farklı yöntemlere sahiptir.

### Tekil veya Çoklu Önyükleme
Herhengi bir Linux dağıtımı kurarken dikkat edilecek ilk durum sistemin tekil veya çoğul önyükleme yöntemlerinden hangisini kullanacağıdır. Genelde Windows ve  Linux aynı makinede çalıştırıyor. Çoklu önyükleyici kullanıldığında makine çalışmaya başlarken bir menü aracılığıyla hangi sistemin kullanılacağı sorulur. 

Diğer bir seçenek Xen'i veya benzeri bir sanallaştırma yazılımı kullanarak aynı anda önyükleme yapmaktır. Üçüncü bir seçenek olarak QEMU, KVM (Kernel Based Virtual Machine), Virtualbox, VMware benzeri yazılımlar kullanarak yapılabilir. QEMU ve KVM daha hızlı fakat kullanımları diğerlerine göre biraz daha karmaşıktır. 

### BIOS ve UEFI
1990'ların sonuna kadar BIOS(Basic Input/Output System) tüm intel tabanlı sistemlerin önyükleme biçimiydi. Disk bölümleme bilgisi, önyüklenebilir bölümlerin her birinin ilk bölümünde ek kod bulunan bir Ana Önyükleme Kaydı'nda (MBR - Master Boot Record) tutuldu.
BIOS'un yetersiz kalmasından dolayı UEFI (Unified Extensible Firmware Interface) geliştirildi. UEFI'nin avantajları;
- 2TB'dan fazla boyuttaki diskleri kullanma becerisi
- CPU bağımsız mimari
- CPU bağımsız sürücüler
- Moduler tasarım

### BIOS altında önyükleme kodu
BIOS, geleneksel önyükleme sistemidir. Önyükleme kodunu yerleştirmenin birkaç yolu vardır. Sık kullanılan yöntem, MBR'ye yerleştirilmesidir. Bununla birlikte, Linux olmayan önyükleyici tercih edildiği durumlar vardır. Örneğin, Windows olmayan bir bootloader yüklüyse bazı Windows yüklemeleri düzgün şekilde güncellenmez. Bu durumda, Linux önyükleme kodu, Linux'un önyükleyicisinin genellikle bulabileceği ve önyükleme menüsünde bir alternatif olarak sunabileceği sürücünün başındaki MBR yerine, Linux önyükleme bölümün başına yerleştirilebilir.

### UEFI altında önyükleme kodu
UEFI, yüklü tüm bellenimin imzalanmasını ya da yüklenmesini gerektirmeyen, güvenli önyüklemeyi destekleyen Intel ve Microsoft tarafından geliştirilen yeni bir önyükleme biçimidir. UEFI altında, önyükleme kodu bir alt bölümde özel bir bölüme yerleştirilir. 

## Dosya Sistemi Tipleri
Bir dosya sistemi seçmek, Linux'u yüklemeden önceki diğer bir adımdır. Desteklenen bazı formatlar ext2, ext3, ext4, JFS, XFS, ReiserFS ve Btrfs bulunur. İlk üçü aslında Linux için geliştirilmiş genişletilmiş dosya sisteminin (ext) aşamalı sürümleridir.

### Journaled File System (Günlük Dosya Sistemi)
Unix benzeri AIX işletim sistemi için IBM tarafından geliştirilen ve GPL lisansı altında yayınlanarak ext ve ext2 dosya sistemlerine alternatif olarak sunulmuştur. Kararlı ve esnek olmanın yanında daha az kaynak kullanır.

## SGI'in XFS Dosya Sistemi
XFS, 1993 yılında Silicon Graphics tarafından geliştirilen bir başka alternatiftir. Paralel giriş/ çıkış üzerine yoğunlaşan yüksek hızlı bir JFS'dir. NASA İleri Süper Hesaplama Bölümü 300 terabyte'dan fazla depolama sunucularında bunu kullanıyor.

### Logical Volume Management (Mantıksal Hacim Yönetimi)
Mantıksal hacimler isteğe göre yeniden boyutlandırılabilirler ve birden çok diske yayılabilir. Hizmetleri kesintiye uğratmadan farklı disklere yayılabilirler. RAID 0 ve RAID 1'e benzer şeritleme ve yansıtma özellikleri de vardır. LVM, temel bölümlemeden daha karmaşıktır ve büyük depolama yüklemeleri dışında yaygın olarak kullanılmamaktadır. Teknik olarak LVM, fiziksel disk bölümlemesinin üzerine yerleştirilen bir yapıdır.

### Swap (Takas) Bölümü
Bir takas dosyası veya bölümü, Linux diske bellekleme yapması gerektiğinde kullanılır. Takas dosyaları mevcut dosya sisteminde oluşturulmuş tek dosyalardır. Takas bölümü diskin özel olarak biçimlendirilmiş bir bölümüdür. Takas dosyaları ek takas alanı gerektiğinde kullanılır.


