---
layout: post
title: "Redis Nedir Ne Değildir"
author: "Oğuzhan İNAN"
date: 2018-04-24
categories: redis
---

## Redis (Remote Dictionary Service) Nedir

C ile yazılmış, key-value şeklinde tasarlanmış bir nosql veritabanıdır. Veriyi bellekte tuttuğu için çok hızlı okuma ve yazma yapılır. Veriyi şifreleyerek depolamaması dezavanatajıdır.

### Veri yapısına göre NoSQL veritabanları

- Döküman Tabanlı   
    En önemli özellikleri esnek olmalarıdır. Bir anahtara karşılık gelen veriler
    döküman adı verilen nesnelerde tutulur. Dökümanlar çok sayıda alan içerebilir ve
    farklı alanlara sahip olabilirler. Nesneler genelde JSON formatındalardır.

- Anahtar / Dğer Tabanlı   
    Küçük veriler için çok sayıda okuma ve yazma için uygundurlar. Caching (önbellek)
    için kullanılırlar.

- Çizge (Graph) Tabanlı   
    Veriler düğümler (node), ilişkiler (edge), özellikler (properties) şeklinde tutulurlar.
    Veriler arasındaki ilişkilerde saklanabilir. Kullanım alanları daha kısıtlıdır.

- Kolon Tabanlı   
    Yüksek okuma ve erişilebilirlik için tasarlanmıştır. Birden çok sunucuda dağıtık 
    olarak çalışabilirler. Yazma işleminde kesinti yaşanmaz fakat dağıtık yapsınıdan dolayı 
    kıas süreli veri tutarsızlığı (inconsistency) yaşanabilir.

![CAP](https://phaven-prod.s3.amazonaws.com/files/image_part/asset/607361/CausfGVcU2tskB-TR5b8CMm8Keg/medium_media_httpfarm5static_mevIk.png)

## CAP Teoremi   
Bu teorem dağıtık bir sistemin aşağıdakilerden sadece ikisine sahip olabileceğini söyler.
-Tutarlılık (Consistency)
Dağıtık sistemlerde verinin tüm düğümlerde her zaman aynı olmasıdır.
-Ulaşılabilirlik (Availability)
Sistemin tüm isteklere her zaman yanıt vermesidir.
-Bölünebilme Toleransı (Partition Tolerance)
Düğümlerden birinin çalışmaması durumunda sistemin sorunsuz çalışmaya devam etmesidir.

### (CA) İse Neden (P) olamıyor
Sunucularınızın arasındaki ağ bağlantısı gitmiş. Siz gittiniz birinci sunucuya yazdınız,
yukarıdaki resimde görüldüğü gibi diğer sunuculara verinin güncel halini gönderemedi. 
2., 3. sunucuya veriyi sorduğunuzda verinin olmadığını veya 1. sunucu ile aynı 
olamadığını göreceksiniz. Yani sistem CA olduğu zaman P(Partition Tolerance) olamıyor. 
Veriniz parçalanmaya toleranslı değil. Verinin doğruluğunun kesin olması gereken, müşteri işlemleri, finansal işlemler 
vb. bu çok önemlidir. İlişkisel veritabanları ve transactional işlemler buna uygun veriler tutar.
### (AP) İse Neden (C) olamıyor
2 sunucunuzun olduğunu düşünelim X değeri 2 sunucuda da bulunuyor. Sunucular arası ağınız çöktü. 
Sunuculardan birinde X değerini güncellediniz. Sorguladığınızda iki sunucu ayrı değer vereceği için
bu durumda C(Consistent) olamıyor. Bir sunucu X değeri için 5 , bir diğeri 4 değerini dönebilir.
Sosyal medyadan twitter veya facebook akan verilerde bunlar kullanılabilir. Like sayısını düşünün
o anda her kişinin doğru şekilde görmesi/görmemesi önemli değildir. Burada bu veriler parçalanarak
tutulabilir. NoSQL veritabanlarından Cassandra, Dynamo gibi sistemler buna uygundur. Eventual Consistency
yöntemi ile arkaplanda Node’lardaki veriler eşleştirilir. Bu eşleştirmeden önce yapılan bir sorgularda
veri değerlerinde tutarsızlıklar olabilir.
### (CP) İse Neden (A) olamıyor
Eğer amacınız hem tutarlılık, hemde verinin parçalara bölünerek kaydedilebilmesi ise. 3 sunucu arasında 
bağlantı koptuğu andan itibaren A(Availibility) yazma özelliğiniz ortadan kalkacaktır. Eğer ki yazarsanız tutarlılığı bozarsınız. 
Bu yüzden sadece okuma yapabilirsiniz bu durumda.
MongoDB veritabanı gibi sistemler CP uygundur. Default’ta strongly consistent’ dır.

Redis veriler anahtar değer olarak tutar. Anahtarlar üzerinden değerlere ulaşılır.

- SET anahtar_adi deger       _// atama yapılır._
- GET anahtar_adi             _// değer çağrılır._
- DEL anahtar_adi             _// anahtar silinir._
- EXISTS anahtar_adi          _// anahtarın varlığını kontrol eder. 0 veya 1 döner._
- EXPIRE anahtar_adi dakika   _// anahtarın var olma süresini belirler._
- KEYS *                      _// var olan anahtarları listeler._
- KEYS user*                  _// şeklinde kullanılırsa user ile başlayan anahtarlar listelenir._
- RENAME eski_ad yeni_ad      _// var olan anahtarı yeniden isimlendirir._

### VERİ TİPLERİ
1. String
2. Hash
3. List
4. Set
5. Sorted Set

### STRING
Temel veri tipidir. Maximum 512 mb string depolar.
- KOMUTLAR
- SET anahtar_adi deger       > atama yapılır.
- GET anahtar_adi            > değer çağrılır.
- DEL anahtar_adi            > anahtar silinir.
- MSET anahatar1 deger1 anahtar2 deger2 > çoklu atama yapılır
- MGET anahtar1 anahtar2     > çoklu okuma yapılır.
    
### HASH
Birden fazla ilgili anahtarı tek tek eklemek yerine harita olarak oluşturabiliriz.
Anahtar içinde ufak anhtarlar oluşturur.

### LIST 
Değerleri liste halinde tutar.
