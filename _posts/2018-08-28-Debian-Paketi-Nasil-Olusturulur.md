---
layout: post
title: "Debian (.deb) Paketi Nasıl Oluşturulur"
author: "Oğuzhan İNAN"
date: 2018-08-28
categories: linux debian package
---


`dh_make` ve `debuild` aracı ile linux debian dağıtımına paket oluşturmak için aşağıdaki iki paketi kurmalıyız. `debuild` aracı `devscript` paketi ile birlikte gelir.

    sudo apt-get install dh-make devscripts


İlk adım projenin doğru adlandırılması gerekli _&lt;paket_adi&gt;_ - _&lt;versiyon&gt;_ şeklinde olmalıdır.

    mkdir ornek-1.0
    cd ornek-1.0
    touch ornek.sh

Örnek için bir shell dosyası oluşturduk. `dh_make` aracı oluşturacağı dosyalarda kullanmak için email adresi ve isme ihtiyaç duyar. Bunları doğru şekilde kullanması için aşağıdaki komut kullanılan shell'in başladığı zaman çalıştırdığı (bash için .bashrc, zsh için .zshrc v.b) dosyaya eklenmeli ve dosya tekrar çalıştırılmalı. (`source ~/.bashrc`)

    export DEBEMAIL="oguzhan3488@gmail.com"
    export DEBFULLNAME="Oguzhan INAN"

Şimdi klasörün içinde aşağıdaki komutu çalıştırıyoruz.

```
dh_make --indep --createorig
```

- __--indep__ parametresi ile paketin tüm CPU mimarileri için çalışabilir olduğunu belirtiyoruz.

- __--createorig__ parametresi ile paketi oluşturmak için gerekli olan _ornek_1.0.orig.tar.xz_ isimli bir arşiv dosyasını klasörün üst dizininde oluşturur.

`dh_make` komutu çalışdıktan sonra paket klasörünün içinde debian adında bir klasör oluşur. `dh_make` birçok dosya oluşturur. Kullanmak istenilen dosyaların sonundaki .ex uzantısı kaldırılarak amaca göre kullanılabilir. debian klasörünün içinde oluşturulan dosyalardan bazılarının kullanım amaçları:

|   |   |   |   |   |
|---|---|---|---|---|
|__control__| bu dosya paketin açıklama, bağımlılık, kategori v.b ayarlarının yapıldığı yerdir. Ayrıntılı bilgi [debian control fields](https://www.debian.org/doc/debian-policy/ch-controlfields.html) |
|__changelog__| paketteki yapılan sürüm değişiklikleri bu dosyaya yazılır. |
|__rules__| projenin nasıl derleneceğinin belirtildiği yer. |
|__postinst__| kurulum tamamlandıktan sonra yapılması gereken varsa bu shell script dosyasına yazılır. |
|__postrm__|   kurulum kaldırıldıktan sonra yapılması gereken varsa bu shell script dosyasına yazılır. |
|__preinst__|  kurulum yapılmadan önce yapılması gereken varsa bu shell script dosyasına yazılır. |
|__prerm__|    kurulum kaldırılmadan önce yapılması gereken varsa bu shell script dosyasına yazılır. |
|__paket_adi.cron.d__| paket cron job kullanması gerekiyorsa bunun içine yazılır. |

<br/>
Aşağıdaki dosyaların olması gereklidir. Dosyalar pakete göre düzenlenmeli.

        |-- ornek-1.0
        |   |-- debian
        |   |   |-- changelog
        |   |   |-- compat
        |   |   |-- control
        |   |   |-- copyright
        |   |   |-- rules
        |   |   `-- source
        |           `-- format
        |   `-- ornek.sh
        `-- ornek_1.0.orig.tar.gz

`dh_make` aracı debian klasörünün içine __install__ adında bir dosya eklemedi bizim eklememiz gerekli. Bu dosya klasör içerisindeki yüklenmeasi gereken dosyaların nereye yükleneciğinin belirlendiği yerdir. Eklemek için:

    touch debian/install

Örnek için içerik aşağıdaki gibidir.

    ornek.sh usr/bin

Paket içeriği hazır paketi derlemek için ornek-1.0 klasörü içerisinde aşağıdaki komut çalıştırılır.

    debuild -us -uc

Bir üst dizinde oluşan `ornek_1.0-1_all.deb` paketini kurmak için:

    sudo dpkg -i ornek_1.0-1_all.deb

komutu kullanılır.

