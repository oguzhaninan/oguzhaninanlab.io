---
layout: post
title: "Varnish Nasıl Çalışır?"
author: "Oğuzhan İNAN"
date: 2018-12-21
categories: linux varnish
---

### Nesneler nasıl saklanıyor

- Nesne: HTTP cevapların lokalde depolanmış halidir.
- Nesneler hash anahtarları kullanılarak yönetilir.
- Hash anahtarları duruma göre değiştirilebilir.
- Birden fazla nesne aynı hash anahtara sahip olabilir.

Nesneler bir hash anahtarı ile eşleştirilir ve bellekte saklanırlar. Bellekteki nesnelere yapılan referanslar bir hash ağacında tutulur. Varnish'in eşsiz bir özelliği, karma algoritmanın girişini kontrol etmenize izin vermesidir. Anahtar, varsayılan olarak HTTP Host üstbilgisi (Header) ve tipik durumlar için yeterli ve önerilen URL'den yapılır. Ancak, anahtarı başka bir şeyden oluşturabilirsiniz. Örneğin, bir hash anahtarı oluşturmak için çerezleri (cookies) kullanabilirsiniz. HTTP, istemcinin tercihlerine bağlı olarak birden fazla nesnenin aynı URL'den sunulabileceğini belirtir. Örneğin, gzip biçimindeki içerik yalnızca gzip desteğini gösteren istemcilere gönderilir. Varnish, çeşitli objeleri tek anahtar ile depolar. Bir istemci talebi üzerine, Varnish, istemci tercihleriyle eşleşen nesneyi seçer.

### Nesnelerin Yaşam Süresi

![Varnish Nesne Lifetime]( {{ "/assets/img/varnish-object-lifetime.png" | absolute_url }})
Şekil önbellekteki nesnelerin ömrünü göstermektedir. Önbelleğe alınmış bir nesnenin `t_origin` ve üç adet süre niteliği vardır.
1. TTL (time-to-live)
2. grace
3. keep   

`t_origin` bir nesnenin yaratıldığı zamandır.
Bir nesne `TTL` + `grace` + `keep` süresi dolana kadar önbellekte kalır. Bu süre bittikten sonra nesne Varnish servisi (daemon) tarafından kaldırılır. Zaman çizelgesinde, TTL'deki nesneler taze (fresh) nesneler olarak kabul edilir. Eski (stale) nesneler `ttl` ve `grace` süresi içerisindeki nesnelerdir. `t_origin` ve `keep` içindeki nesneler, `If-Modified-Since` HTTP başlığı uygulanırsa belirtilen koşula göre uygulanır.