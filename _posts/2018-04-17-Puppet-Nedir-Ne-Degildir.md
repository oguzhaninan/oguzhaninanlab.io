---
layout: post
title: "Puppet Nedir Ne Degildir"
author: "Oğuzhan İNAN"
date: 2018-04-17
categories: puppet devops
---

![Puppet]({{ "/assets/img/puppet.jpg" | absolute_url }})

### Puppet Nedir?   
Puppet, sunucuların dağıtımı, yapılandırılması ve yönetilmesi için kullanılan bir Yapılandırma Yönetimi aracıdır. Birkaç özelliği:  
* Her bir host için ayrı konfigürasyonların tanımlanması ve gerekli konfigürasyonun yerinde olup olmadığının ve değiştirilmediğinin sürekli olarak kontrolünü sağlar.
* Tüm yapılandırılmış makineleriniz üzerinde kontrol sağlayarak merkezi değişiklik otomatik olarak diğer makinelerede yayılır.

Puppet hakkında birkaç bilgi:   
* __Büyük kullanıcı kitlesi:__   
Puppet, Google, RedHat, Siemens gibi dünya çağında 30.000'den fazla şirket tarafından kullanılmaktadır.
* __Büyük Geliştirici kitlesi:__   
Puppet çok büyük bir geliştirici desteğini sahiptir.
* __Ticari Geçmiş:__   
Pupper, 2005'ten beri ticari olarak kullanılmaktadır ve sürekli olarak geliştirilmektedir.
* __Dokümantaston:__   
Puppet, yüzlerce sayfa dokümantasyon ve hem dil hem de kaynak türleri için kapsamlı referanslar ile geniş bir kullanıcı kitlesi tarafından geliştirilen wiki sayfalarına sahiptir.
* __Platform Desteği:__   
Puppet Server, Ruby dilinin kullanıldığı her platformda çalışır. 

## Konfigürasyon Yönetimi
Sistem yöneticileri genellikle sunucu kurulumu, kurulan sunucuların yapılandırılması vb. tekrar eden işler ile uğraşırlar. Bu işleri komut dosyaları yazarak otomatik hale getirebilirler, ancak büyük bir altyapı üzerinde çalışırken bu çok kolay olmaz.

Bu sorunu çözmek için Konfigürasyon yönetimi araçları kullanılır. Konfigürasyon Yönetimi, sistemin mevcut tasarım ve inşa durumunun iyi ve güvenilir olmasından sorumludur. 

Örnek:
New York Borsasında (NYSE) bir yazılımsal aksaklık neredeyse 90 dakika boyunca hisse senedi alımına engel oldu. Bu sorun milyonlarca dolar kayba yol açtı. Yeni bir yazılımın yüklenmesi bu soruna neden oldu. Bu yazılım 20 sunucunun 8'inde kuruldu ve sistem önceki gece test edildi. Ancak sabah saat saatlerinde düzgün çalışmadılar. Böylece eski yazılıma geri dönme ihtiyacı oluştu. NYSE uygun bir konfigürasyon yönetimi aracı kullanması sayesinde bu durumdan 90 dakika sonra hızlı bir şekilde kurtuldu.