---
layout: post
title: "Fluentd ve Fluentbit Nedir?"
author: "Oğuzhan İNAN"
date: 2019-08-28
categories: kubernetes fluentd fluentbit log
---

![Fluentd]({{ "/assets/img/fluentd-architecture.png" | absolute_url }})

# Fluentd Nedir?   

[Fluentd](https://www.fluentd.org/), 2011 yılında [Treasure Data](https://www.treasuredata.com/)'daki kişiler tarafından oluşturulan açık kaynaklı bir log toplayıcı ve işleyicidir. Ruby'de yazılan Fluentd, birden fazla kaynaktan veri toplayabilen, farklı biçimde biçimlendirilmiş verileri JSON nesnelerine birleştiren ve farklı çıktı hedeflerine yönlendiren bir araç olarak kullanılıyor.

Sade bir Fluentd yaklaşık 40 MB'lık bir hafıza kullanır çalışır ve saniyede 10.000 olayın üzerinde işlem yapabilir. Yeni girdiler veya çıktılar eklemek basittir ve performans üzerinde çok az etkisi vardır. Fluentd, aktarım hatalarını veya aşırı yüklenmeyi yönetirken tamponlama ve kuyruklama için disk veya bellek kullanır ve daha esnek bir veri hattı sağlamak için çoklu yapılandırma seçeneklerini destekler.

Fluentd işlevselliğini artıran 700'den fazla farklı eklentiden oluşan zengin bir ekosisteme sahip. Fluentd, en yaygın kullanılan docker imajlarından biridir.

Bir [ELK](https://www.elastic.co/what-is/elk-stack) kullanıcısıysanız, tüm bu bahsedilenler [Logstash](https://www.elastic.co/products/logstash)'in yaptığı şeylere benziyor.

# Fluentbit Nedir?   

![Fluentbit]({{ "/assets/img/fluentbit.png" | absolute_url }})

[Fluent Bit](https://fluentbit.io/), 2015 yılında yine aynı firma tarafından oluşturulan açık kaynaklı bir yazılımdır.

Aynı amaca hizmet etmek için Fluent Bit yüksek performans için tasarlandı ve sadece ~450 KB hafıza kullanan çok hafif bir kaynak kullanımına sahip. Soyutlanmış bir G/Ç işleyicisi, eşzamansız ve olaya dayalı (event-driven) okuma/yazma işlemlerine izin verir. Esneklik ve güvenilirlik için, yeniden denemeleri ve tampon limitini tanımlamak için çeşitli konfigürasyon seçenekleri mevcuttur.

Fluent Bit de genişletilebilir ancak Fluentd ile karşılaştırıldığında daha küçük bir ekosisteme sahip. Girdi olarak syslog, tcp, systemd/journald ve ayrıca CPU, hafıza ve disk içerir. Çıktı olarak [Elasticsearch](https://www.elastic.co/), [InfluxDB](https://www.influxdata.com/), dosya ve HTTP. [Kubernetes](https://kubernetes.io/) dağıtımlarında, özel bir filtre eklentisine sahiptir.

# Fluentd ve Fluent Bit Karşılaştırması
Hem Fluentd hem Fluent Bit, Treasure Data tarafından kullanıcıların merkezi, güvenilir ve verimli log boru hatları oluşturmasına yardımcı olmak için geliştirilmiştir. Fluentd ve  Fluent Bit'in oluşturulmasındaki amaç, log kayıtlarının kaydedilmesindeki bazı zorlukların üstesinden gelmeye yardımcı olmaktı. Bunlar yapılandırılmamış verileri biçimlendirmek, birden fazla veri kaynağından toplama, esneklik ve güvenlik.

İki araç arasında mimari ve tasarım benzerlikleri olsa da, ikisi arasında seçim yaparken göz önünde bulundurulması gereken bazı temel farklılıklar da vardır.
Aşağıda Fluent Bit dokümanından alınmış bir karşılaştırma var.

![Fluentbit]({{ "/assets/img/compare-fluentd-fluentbit.png" | absolute_url }})

### Performans   

Yukarıdaki tabloda görüldüğü gibi Fluentd, diğer alternatiflerine göre avantajlı olsada, Fluent Bit ondan daha iyi bir durumda. Farkı görmek için, iki aracı Kubernetes'de çalıştırmak için önerilen kaynak isteğine bakın.

**Fluentd:**
```yaml
resources:
  limits:
    memory: 500Mi
  requests:
    cpu: 100m
    memory: 200Mi
```

**Fluent Bit:**
```yaml
resources:
  requests:
    cpu: 5m
    memory: 10Mi
  limits:
    cpu: 50m
    memory: 60Mi
```
Yüzlerce sunucudan oluşan bir ortamda, CPU ve bellek kullanımının toplam etkisi önemlidir.

### Toplama

Fluent Bit, bir toplayıcı ve iletici olarak görev yapar ve yukarıda açıklandığı gibi performans dikkate alınarak tasarlanmıştır. Fluentd, çok sayıda girdiden toplanan verileri işleyen ve farklı çıktılara yönlendirmek yaratılmıştır. Fluent Bit, daha fazla miktarda giriş ve çıkış kaynağıyla entegre edilebilen Fluentd kadar esnek değildir.

### İzleme

Fluent Bit, dağıtıldıkları ortamdan metrik toplama için yerel destekle birlikte gelir. CPU ve disk gibi çeşitli giriş eklentileri CPU ve bellek kullanımı hakkında veri toplayacak ve seçili bir çıktıya iletecektir. Sürüm 0.13 ayrıca Prometheus ölçümlerini de desteklemektedir.

## Fluentd ve Fluent Bit Kullanım Senaryosu

Örneğin, Kubernetes'de, Fluent Bit, node başına bir `DaemonSet` olarak dağıtılacak, küme başına dağıtılan bir Fluentd'ye veri toplayıp iletecek ve verileri işleyen ve etiketleri temel alan farklı kaynaklara yönlendiren bir toplayıcı olarak görev yapacaktır.

![FluentChart]({{ "/assets/img/fluent-chart.png" | absolute_url }})

Fluent Bit elbette kendi başına kullanılabilir, ancak toplama yetenekleri ve diğer çözümlerle entegrasyon için çok daha az miktarda eklenti ile sunabileceği çok az şeyi vardır.

[Kaynak 1](https://www.fluentd.org/architecture)   
[Kaynak 2](https://fluentbit.io/documentation/)   
[Kaynak 3](https://github.com/fluent/fluent-bit/)    
[Kaynak 4](https://github.com/fluent/fluentd)   
[Kaynak 5](https://docs.fluentd.org/)    