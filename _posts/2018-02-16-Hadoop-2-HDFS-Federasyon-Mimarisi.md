---
layout: post
title: "Hadoop 2.0 HDFS Federasyon Mimarisi"
author: "Oğuzhan İNAN"
date: 2018-02-16
categories: bigdata hadoop
---

## İçerik
1. [Giriş](#giris)
2. [Mevcut HDFS Mimarisine Genel Bakış](#mevcut-hdfs-mimarisi-genel-bakis)
3. [Mevcut HDFS'nin Sınırları](#mevcut-hdfs-sinirlari)
4. [HDFS Federasyon Mimarisi](#hdfs-federasyon-mimarisi)
5. [Blok Havuzu](#blok-havuzu)
6. [İsim Alanı Hacmi](#isim-alani-hacmi)

<a name="giris"></a>
## Giriş
NameNode, DataNode'ların aksine birden fazla olmadığı için, belli bir süre sonra yavaşlık ve erişim sorunları yaşanabilir. Fakat NameNode'un her an sorunsuz erişime sahip olması gereklidir. NameNode aracılığıyla tüm Hadoop kümesi kontrol edildiği için, bu soruna bir çözüm geliştirildi. 

<a name="mevcut-hdfs-mimarisi-genel-bakis"></a>
## Mevcut HDFS Mimarisine Genel Bakış
![Mevcut HDFS Mimarisine Genel Bakış]({{ "/assets/img/hdfs-mimari-genel-bakis.png" | absolute_url }})

- __HDFS İsim Alanı (Namespace):__   
Bu katman dizinleri, dosyaları ve blokları yönetmek ile sorumludur. Dosya veya dosya dizinleri oluşturma, silme ve değiştirme gibi tüm dosya sistemi işlemlerini yapar.
- __Depolama Katmanı:__   
	- __Blok Yönetimi:__   
		* DataNode'ların kalp atışlarını periyodik olarak kontrol eder.
		* Blok raporlarını yönetir.
		* Blok yerinin oluşturulması, değiştirilmesi, silinmesi ve tahsisi gibi blok işlemlerini destekler.
		* Küme boyunca çoğaltma faktörünü korur.
	- __Fiziksel Depolama:__   
		Veri saklamaktan sorumlu olan DataNode'lar tarafından yönetilir ve böylece HDFS'de depolanan verilere Okuma/Yazma erişimi sağlar.

Mevcut HDFS Mimarisi, bir küme için tek bir İsim Alanına sahip olmanızı sağlar. Bu şekilde bir kümede İsim Alanının yönetiminde tek bir NameNode sorumludur. Bu mimari küçük kümeler için idealdir.

<a name="mevcut-hdfs-sinirlari"></a>
## Mevcut HDFS'nin Sınırları
1. İsim Alanı DataNode'lar gibi ölçeklenebilir değildir. Bu nedenle, kümede tek bir NameNode'un işleyebildiği çok sayıda DataNode'a sahip olabiliriz.   
2. Tüm Hadoop Sisteminin performansı, NameNode'un belli bir zamanda kaç görev yapabileceği ile sınırlıdır.   
3. NameNode, hızlı erişim için tüm İsim Alanını RAM'e depolar. Bu, bellek boyutu bir yerden sonra sınıra dayandığı için dosya ve blok sınırlamasına yol açar.   
4. HDFS dağıtımına sahip kuruluşların çoğu, birden fazla kuruluşun kendi küme İsim Alanını kullanmasına izin verir. Yani, isim alanının ayrılığı yoktur ve bu nedenle, kümeyi kullanan kuruluş ile arasında herhangi bir izolasyon yoktur.

<a name="hdfs-federasyon-mimarisi"></a>
## HDFS Federasyon Mimarisi
- Bu mimaride, İsim Alanının yatay ölçeklenebilirliği vardır. Bu nedenle, birden fazla ve birbirinden NameNode'a sahip olunabilir.   
- DataNode'lar altta bulunur, yani temel depolama alanı.  
- Her DataNode, kümedeki tüm NameNode'lar tarafından yönetilebilir.  
- Birden çok İsim Alanı vardır ve her biri kendi NameNode'u tarafından yönetilir.  
- Her İsim Alanının kendi blok havuzu vardır.  

<a name="blok-havuzu"></a>
### Blok Havuzu  
Blok havuzu, belirli bir İsim Alanına ait bloklardan başka bir şey değildir. Yani, her blok havuzunun diğerinden bağımsız olarak yönetildiği bir blok havuzu koleksiyonu vardır. Tüm blok havuzunda bulunan veri blokları, tüm DataNode'larda saklanır. Temelde, blok havuzu, DataNode'larda bulunan veri bloklarının belirli bir İsim Alanına karşılık gelene kadar gruplanabilmesi için bir soyutlamadır. 

<a name="isim-alani-hacmi"></a>
### İsim Alanı Hacmi  
Blok havuzuyla birlikte İsim Alanından başka bir şey değildir. Bu nedenle, HDFS Federsayon Mimarisinde birden fazla İsim Alanı Hacmi bulunmaktadır. Bu, bağımsız bir yönetim birimi sağlar yani her İsim Alanı Hazmi bağımsız olarak çalışabilir. Bir NameNode ve İsim Alanı silinirse, DataNode'larda bulunan blok havuzu da silinir.
