---
layout: post
title: "Scala Fonksiyon Kullanımı"
author: "Oğuzhan İNAN"
date: 2018-04-04
categories: scala
---

### Fonksiyonlar (Functions)

__Fonksiyon Tanımlama__     
Fonksiyon tanımlarken dönüş tipi belirtmezseniz döndürdüğünüz değere göre otomatik olarak tanımlanır. Fonksiyon bir değer döndürmezse metod olarak `Unit` tipini alır. Fonksiyon parametrelerini belirtirken var veya val anahtar kelimeleri kullanılmaz. Parametre ismi ve tipi gereklidir.
```scala
def fonksiyonIsmi ([parametreler]) : [dönüş tipi] = {

}
```
__Örnek Fonksiyon Tanımlama__    
```scala
def yas = 34
def yas2(i: Int) = { var yas = i * 2; println(yas) }
def yas3(i: Int): Int = i + 2
def yas4(i: Int, x: Int): Int = return i * x
```
__Örnek Fonksiyon Kullanımı__    
```scala
yas2(23)    /* 56 */    yas2 (23)    //56
yas4(2, 5)  /* 10 */    yas4 (2, 5)  //10
```
İlk örnekte tip belirtmediğimiz için fonksiyonun tipi döndürdüğümüz değer tipine göre belirlendi. İkinci örnekte parametreli bir fonksiyon tanımladık ve herhangi bir değer döndürmediğimiz için tipi `Unit` oldu. Üçüncü örnekte dönüş tipini tanımlarken Int olarak belirttik. Dördüncü örnekte return anahtar kelimesini kullanarak değeri döndürdük. return anahtar kelimesi zorunlu değildir fakat kullanmak için fonksiyon tanımlanırken tipi belirtilmelidir aksi halde hata verir.

__Default Değerler__    
Fonksiyon tanımlarken parametrelere default değer verebiliriz. Default değer verdiğimiz parametreleri fonksiyonu kullanırken girmezsek default olarak verdiğimiz değer kullanılır.
```scala
def bilgiler(ad: String, soyad: String = "Yılmaz", yas: Int = 30): Unit = {
    println(s"Adınız: $ad Soyadınız: $soyad Yaşınız: $yas")
}

bilgiler("Martin")                 // Adınız: Martin Soyadınız: Yılmaz Yaşınız: 30
bilgiler("Martin", "Odersky", 57)  // Adınız: Martin Soyadınız: Odersky Yaşınız: 57
bilgiler(ad = "Mehmet", yas = 23)  // Adınız: Mehmet Soyadınız: Yılmaz Yaşınız: 23
```
Son örnekte görüldüğü gibi parametre girerken parametre adını belirterek parametreleri sırayla girmek yerine istediğimiz sırada girebiliriz. Yukarıdaki örnekte sırayla girmeye kalkarsak yası girmek için soyadını girmemiz gerekir. Soyad değerinin default olarak kullanılmasını istersek bu şekilde kullanabiliriz.

__Çoklu Parametre ( * Operatörü )__   
Fonksiyonlara parametre girerken bazen kaç parametre gireceğimiz belli olmayabilir. Buna çözüm olarak bir dizi parametre olarak verilebilir. Parametre olarak dizi vermek yerine daha orjinal bir çözüm olan ( * ) operatörü kullanılır.
```scala
def topla(elemanlar: Int*): Int = {
    var toplam: Int = _
    for (x <- elemanlar) toplam += x
    toplam
}

topla(12, 34, 45, 68) // 159
topla(10, 23)         // 33
```

__Parametre Gruplama__   
```scala
def adres(mah: String, sok: String)(no: Int, daire: Int) = {
    println(s"$mah $sok $no $daire")
}

adres("Atatürk Mah.", "Namık Sok.")(23, 4) // Atatürk Mah. Namık Sok. 23, 4
```