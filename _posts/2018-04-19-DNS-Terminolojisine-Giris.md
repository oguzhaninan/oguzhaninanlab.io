---
layout: post
title: "DNS Terminolojisine Giriş"
author: "Oğuzhan İNAN"
date: 2018-04-19
categories: network introduction linux dns
---

### Alan Adı   
Bir alan adı, bir internet kaynağıyla ilişkilendirmek için kullandığımız anlaşılabilir addır. Örneğin __gitlab.com__ bir alan adıdır. __gitlab.com__ gitlab şirketinin sunucuları ile ilişkilendirilmiştir. __gitlab.com__ adresine gitmek için tarayıcıya adresi yazdığımız zaman DNS yardımıyla sunucuların IP adreslerine ulaşılır ve web sitesi getirilir.

### IP Adresi   
Bir IP adresi, _ağ adreslenebilir konum_ olarak adlandırılan şeydir. Her IP adresi kendi ağı içerisinde benzersiz olmalıdır. Web sitelerini düşünürsek bu ağ tüm internettir. Adreslerin en aygın biçimi olan IPv4, her bir kümeninin bir nokta ile ayrıldığı ,her biri üç haneye kadar olan dört sayı kümesi ile yazılır. Örneğin, __11.222.133.44__ geçerli bir IPv4 adresidir. DNS ile, bu adrese hatırlanabilir bir ad ilişkilendiriyoruz.

### Top-Level Domain (Üst Düzey Alan)   
Üst düzey bir etki alanı, etki alanının en genel parçasıdır. Üst düzey alan, bir alan adındaki en sağda bulunan bölümdür. Ortak üst düzey alanlar _com_, _net_, _org_, _gov_, _edu_, _io_.

### Sunucular  
Bir alan adı içinde alan sahibi, bir alan adıyla erişilebilen bilgisayarları tanımlayabilir. Çoğu alan adı sahibi web sunucularını alan adları üzerinden erişilebilir kılar.

### SubDomain (Alt Alan Adı)  
Üst düzey alanların altında birçok alan olabilir. Örneğin, "com" üst düzey alanının altında hem __gitlab.com__ hem de __github.com__ var. Bu durumda, __gitlab.com__ __com__ alanının alt etki alanı olduğu söylenebilir. Buna etki alanı adı verilir veya __gitlab__ kısmına SLD (Second-Level Domain) İkinci düzey etki alanı denir.

Benzer şekilde, her etki alanı altında __alt alan adlarını__ barındırabilir. Örneğin, __about_gitlab.com__ örneğinde _about_ bir alt alan adıdır. Bir ana bilgisayar adı ve bir alt alan arasındaki fark, ana bilgisayarın bir bilgisayarı veya kaynağı tanımlaması, alt alanın ise ana alanı genişletmesidir. Bu alanın kendisini bölmek için bir yöntemdir.

### Name Server (Ad Sunucusu)    
Ad sunucusu, alan adlarını IP adreslerine çevirmek için tasarlanmış bir sunucudur. Bu sunucular DNS sistemindeki işin çoğunu yapar. Alan adı çevirilerinin sayısı çok fazla olduğu için, her sunucu isteği diğer ad sunucularına yönlendirebilir veya sorumlu oldukları alt alan kümesi için sorumluluğu devredebilir.

### Zone File (Bölge Dosyası)   
Bir zone file, alan adları ile IP adresleri arasındaki ilişkileri içeren basit bir metin dosyasıdır. DNS sistemi, bir kullanıcı belirli bir etki alanı adını istediğinde, hangi IP adresi ile iletişim kurulacağını bu şekilde öğrenir. Bölge dosyaları isim sunucularında bulunur ve genellikle belirli bir alan adı altında bulunan kaynakları veya bu bilgileri almak için gidebileceği yerleri tanımlar.

### Records (Kayıtlar)   
Bir bölge dosyasında kayıtlar (records) tutulur. En basit haliyle, bir kayıt temel olarak bir kaynak ile bir isim arasındaki tek bir bağlantıdır. Bunlar bir alan adını bir IP adresine eşleyebilir, alan adına ait isim sunucularını tanımlayabilir, alan adı için posta sunucularını tanımlayabilir, vb.

## DNS Nasıl Çalışır?   

## Root (Kök) Sunucular   
DNS özünde hiyerarşik bir sistemdir. Bu sistemin en üstünde _root sunucuları_ bulunur. Bu sunucular çeşitli kuruluşlar tarafında kontrol edilir. Ancak, çözülmesi gereken çok fazla alan adı olduğu için, bu sunucuların her biri aslında yansıtılır. Bu kurulumla ilgili ilginç olan tek şey, tek bir kök sunucunun aynalarının her birinin aynı IP adresini paylaşmasıdır. Belirli bir kök sunucu için istek yapıldığında, istek bu kök sunucunun en yakın olanına yönlendirilir. Bu kök sunucuları ne yapar? Kök sunucuları, Üst düzey alanlar hakkında bilgi isteme işlemlerini gerçekleştirir.  

Kök sunucuya _www.gitlab.com_ için bir istek yapıldığınıda, kök sunucu sonucu kendi kayıtlarında bulamaz. Adres ile eşleşen bir liste için kendi zone file (bölge dosyalarını) kontrol edecek ama bulamayacaktır. Bunun yerine _com_ TLD için bir kayıt bulacaktır ve talep eden kişiye _com_ adreslerinden sorumlu ad sunucularının adresini verecektir.

### TLD Sunucular   
İstekte bulunan kişi, isteğin üst düzey etki alanından sorumlu olan IP adresine (kök sunucu tarafından verilen) yeni bir istek gönderirir. Örneğe devam edersek _gitlab.com_ adresinin nerede olduğunu bilip bilmediğini görmek için _com_ alan adlarını bilmekle sorumlu ad sunucusuna bir istek gönderirir. Ancak zone dosyalarında bulunamaz. Ancak, _gitlab.com_ dan sorumlu ad sunucusunun IP adresini listeleyen bir kayıt bulacaksınız.

### Alan Adı Seviye Sunucuları   
Bu noktada, istekte, kaynağın gerçek IP adresini bilmekten sorumlu ad sunucusunun IP adresi bulunur. _gitlab.com_ ismini çözebilirse, ad sunucusuna yeni bir istek gönderirir. Ad sunucusu bölge dosyalarını kontrol eder ve _gitlab.com_ ile ilişkili bir bölge dosyasına sahip olduğunu bulur. Bu dosyanın içinde _www_ host için bir kayıt var. Bu kayıt, ana bilgisayarın bulunduğu IP adresini bildirir. İsim sunucusu, son yanıtı istekte bulunan kişiye gönderirir.

### Resolver Name Server (Çözümleyici Ad Sunucusu)   
Çözümleyici bir sunucu, diğer sunucuların sorularını sormak üzere yapılandırılmıştır. Temel olarak, bir kullanıcı için hızını artırmak için önceki sorgu sonuçlarını önbelleğe alan ve daha önce bilmediğini şeyler için yapılan istekleri _çözebilecek_ kök sunucuların adreslerini bilen bir aracıdır. 

Temel olarak, bir kullanıcı genellikle bilgisayar sistemlerinde yapılandırılmış birkaç çözümleyici ad suncusuna sahip olur. Resolver Name Server'lar genellikle bir ISS veya başka kuruluşlar tarafından sağlanır.  

Tarayıcının adres çubuğuna bir URL yazıldığında, öncelikle kaynağın bulunduğu konumun yerel olarak bulunup bulunamayacağını kontrol eder. Bilgisayardaki __hosts__ dosyasını ve diğer birkaç konumu kontrol eder. Ardından, isteği çözümleyici ad sunucusuna gönderir ve kaynağın IP adresini almak için bekler. Çözümleyici ad sunucusu daha sonra yanıt için önbelleğini kontrol eder. Eğer bulamazsa, yukarıda belirtilen adımları uygular.

### Bölge Dosyaları   
Bölge dosyaları (zone files), ad sunucularının bildikleri alanlarla ilgili bilgileri depolamanının yoludur. Bir ad sunucusunun bildiği her alan bir bölge dosyasında saklanır. Yinelenen sorguları işlemek için yapılandırılmışsa, çözümleyici bir isim sunucusu gibi cevabı bulur ve geri gönderir. Aksi takdirde, istekte bulunan tarafa bir sonraki duracak yeri söyleyecektir. 

Bir bölge dosyası temel olarak tüm DNS adlandırma sisteminin bir alt kümesi olan bir DNS __bölgesi__ ni tanımlar. Genellikle tek bir alanı yapılandırmak için kullanılır. Söz konusu alan için kaynakların nerede olduğunu tanımlayan bir dizi kayıt içerebilir.

`$ORIGIN` varsayılan olarak bölgenin en yüksek yetki düzeyine eşit parametresidir.

## Kayıt Türleri  

### SOA Kayıtları   
Yetki başlığı veya SOA kaydı, tüm bölge dosyalarında zorunlu bir kayıttır. Bir dosyadaki ilk gerçek kayıt olmalıdır.

```
domain.com. IN SOA ns1.domain.com. admin.domain.com. (
                        12083   ; seri numarası
                        3h      ; yenileme aralığı
                        30m     ; tekrar deneme aralığı
                        3w      ; son kullanma süresi
                        1 saat  ; negatif TTL
)
```

* __domain.com.__ :   
Bu bölgenin köküdür. Bu, bölge dosyasının __domain.com.__ etki alanı için olduğunu belirtir. Çoğu zaman, bunun yerine `@` koyulduğu görülür, bu, `$ORIGIN` yukarıda gördüğümüz değişkenin içeriğini işaret eden bir değişken olarak kullanılır.

* __IN SOA__:   
_IN_ kısmı internet demektir. SOA, bunun bir yetki kaydı olduğunu gösteririr.

* __ns1.domain.com.__:    
Bu, etki alanının, birincil ad sunucusu adını tanımlar. Ad sunucuları ya ana (master) ya da köle (slave) olabilir ve dinamik DDNS yapılandırılmışsa, bir sunucunun burada _birincil ana_ (primary master) olması gerekir. Dinamik DNS'yi yapılandırmadıysanız, bu yalnızca ana ad sunucularınızdan biridir.

* __admin.domain.com.__:   
Bu zone file için yöneticinin e-posta adresidir. @ işareti "." ile değiştirilir. E-posta adresi içinde "." varsa onun yerine "\" kullanılır. Örnek, __ad.soyad@gmail.com__ yerine __ad\soyad.gmail.com__ kullanılır.

* __12083__:   
Bu bölge dosyasının seri numarasıdır. Bir bölge dosyası her düzenlendiğinde, bölge numarası bir artırılır. Bağımlı sunucular, ana sunucunun bir bölge için seri numarasının sistemlerinde sahip olduklarından büyük olup olmadığını kontrol eder. Büyükse, değiştirilmiş bölge dosyasını isterler, aynı ise birşey yapılmaz.

* __3h__:   
Bu bölge için yenileme aralığıdır. Bu, slave (köle) suncunun bölge dosyasındaki değişiklikler için sorgulama sıklık periyodudur. Bu örnek için 3 saatte bir sorgulama yapılır.

* __30m__:   
Bu bölge için tekrar deneme aralığıdır. Slave suncunu yenileme süresi dolduğunda ana sunucuya bağlanamazsa, belirtilen (30m) süre kadar bekler ve tekrar sorgular.

* __1h__:   
Bu dosyada istenen ad bulunamazsa ad sunucusunun bir ad hatasını önbelleğe alacağı süredir. Her sorguda dosya kontrol edilmek yerine önbelleğe alınan hata gösterilir.

### A ve AAAA Kayıtları   
Bu kayıtların her ikiside bir ana bilgisayarı bi IP adresine eşler. "A" kaydı, bir ana bilgisayarı bir IPv4 adresine eşlemek için kullanılırken, "AAAA" kayıtlar, bir ana bilgisayarı bir IPv6 adresine eşlemek için kullanılır.

Bu kayıtların genel formatı şudur:  
```
host    IN A        IPv4_address
host    IN AAAA     IPv6_address
```

Örnek: 
```
ns1     IN  A   11.23.44.222
```
Tam adı vermek zorunda değiliz. ns1 değerinden sonra kalan alanı DNS sunucusu `$ORIGIN` değeri ile dolduracaktır. Ancak anlaşılabilir olması istenirse tüm alan kullanılabilir.
```
ns1.domain.com.     IN  A   11.23.44.222
```

### CNAME (Canonical Name - Alias) Kayıtları   
CNAME kayıtları, sunucunuz için bir takma adı tanımlamaya yarar. Örneğin, __server1__ ana bilgisayarını tanımlayan bir _A_ ad kaydı alıp _www_ takma adını bu sunucu için kullanabiliriz.
```
server1  IN  A      11.23.44.222
www      IN  CNAME  server1
```
Bu takma adlar bazı performans kayıplarıyla birlikte gelir çünkü bunlar sunucuya ek bir sorgu gerektirir. Çoğu zaman ek A veya AAAA kayıtları kullanılarak da aynı iş yapılabilir.
Geçerli bir bölgenin dışındaki bir kaynağa ait bir takma ad sağlamak daha doğru olur.

## MX Kayıtları   
MX kayıtları, bir e-posta hesabına gelen e-postaların kullanıcının e-posta hesaplarını barındırdığı sunucuya yönlendirilmesini sağlayan alan adınız ile ilişkilendirilmiş DNS kayıtlarıdır.

Örnek    
```
    IN MX 10 mail.domain.com.
```
Başlangıçta ana bilgisayar adı bulunmaz. Fazladan koyulan sayı değeri, bilgisayarların tanımlanan birden çok posta sunucusu varsa, hangi sunucunun posta göndereceğine karar vermesine yardımcı olan tercih sayısıdır. Düşük sayılar daha yüksek bir önceliğe sahiptir.

MX kaydı, genellikle bir CNAME tarafından tanımlanan takma ada değil, A veya AAAA kaydı tarafından tanımlanan bir ana bilgisayara işaret etmelidir.

Örneğin iki posta sunucumuz olduğunu varsayalım.
```
        IN  MX   10   mail1.domain.com.
        IN  MX   30   mail1.domain.com.
mail1   IN  A         11.32.4.44
mail2   IN  A         22.3.44.23
```
__domain.com.__ kısımlarını yazmasak da bir sorun olmaz.

### NS (Name Server) Kayıtları   
Bu kayıt tipi, bu bölge için kullanılan ad sunucularını tanımlar.

```
        IN  NS      ns1.domain.com.
        IN  NS      ns2.domain.com.
ns1     IN A        11.2.3.5
ns2     IN A        23.4.6.2
```

Tek bir sunucuyla ilgili bir sorun varsa, doğru bir şekilde çalışması için her bölge dosyasında tanımlanmış en az iki isim sunucunuz olmalıdır. Çoğu DNS sunucu yazılımı, yalnızsa tek bir ad sunucusu varsa, zone dosyasının geçersiz olduğunu düşünür. Bu nedenle en az iki sunucu kesin eklenmelidir.

