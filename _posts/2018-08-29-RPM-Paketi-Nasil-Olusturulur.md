---
layout: post
title: "RPM (.rpm) Paketi Nasıl Oluşturulur"
author: "Oğuzhan İNAN"
date: 2018-08-29
categories: linux rpm package
---

`rpmbuild` aracını kullanarak RPM paketi derlemek için önce aşağıdaki kurulumlar yapılmalı.

Debian tabanlı dağıtımlar:

    sudo apt-get install rpm

Fedora tabanlı dağıtımlar:

    sudo yum install rpm-build

Kurulumdan sonra `rpmbuild` için home dizininde `.rpmmacros` isimli bir dosya oluşturarak `rpmbuild` aracına çeşitli ayarlar verilebilir. Paketi oluşturmak için gerekli bileşenleri bulabileceği klasörü `rpmbuild` aracına göstermek için `%_topdir` değişkeni kullanılır. `.rpmmacros` dosyası içerisine aşağıdaki satır eklenerek yapılabilir.

    %_topdir    /home/username/rpmbuild

Yukarıda belirtilen klasörü ve içinde olması gerekenleri oluşturmak için:

    mkdir -p ~/rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

Örnek olarak ilk adım paket için bir klasör oluşturalım.

    mkdir ornek-1.0
    cd ornek-1.0

Program `ornek.py` dosyamız olacak.

    #!/usr/bin/python

    print "Ornek Program"

Şimdi paket klasörünü sıkıştırıyoruz.

    tar -czvf ornek-1.0.tar.gz ornek-1.0/

`rpmbuild` aracı SOURCES dizinine bakacaktır. Yukarıdaki gibi `%_topdir` makrosunu kullandıysanız dizin `~/rpmbuild/SOURCES` olacak. Bu yüzden sıkıştırılan dosyayı bu dizine taşıyoruz.

    mv ornek-1.0.tar.gz ~/rpmbuild/SOURCES

Şimdi bir `.spec` dosyası oluşturmalıyız. Bir SPEC dosyası, yazılımın nasıl oluşturulacağını ve paketleneceğini anlatır. 

    Name:           ornek
    Version:        1.0
    Release:        1%{?dist}
    Summary:        Ornek program

    License:        MIT
    URL:            https://oguzhaninan.gitlab.io
    Source0:        ornek-1.0.tar.gz

    %description
    Ornek bir program.

    %prep
    %setup

    %install
    mkdir -p %{buildroot}/usr/bin
    install -m 0755 ornek.py %{buildroot}/usr/bin/ornek

    %files
    %{_bindir}/ornek

RPM paketini derlemek için SPEC dizininde aşağıdaki komutu çalıştırılır.

    rpmbuild -ba ornek-1.0.spec

Derleme işlemi tamamlandıktan sonra `~/RPMS/x86_64/ornek-1.0-1.x86_64.rpm` dizinindeki rpm paketi ile kurulum yapabiliriz.

SPEC dosyasında kullanılabilecek değişkenler:

    %{_sysconfdir}        /etc
    %{_prefix}            /usr
    %{_exec_prefix}       %{_prefix}
    %{_bindir}            %{_exec_prefix}/bin
    %{_libdir}            %{_exec_prefix}/%{_lib}
    %{_libexecdir}        %{_exec_prefix}/libexec
    %{_sbindir}           %{_exec_prefix}/sbin
    %{_sharedstatedir}    /var/lib
    %{_datarootdir}       %{_prefix}/share
    %{_datadir}           %{_datarootdir}
    %{_includedir}        %{_prefix}/include
    %{_infodir}           /usr/share/info
    %{_mandir}            /usr/share/man
    %{_localstatedir}     /var
    %{_initddir}          %{_sysconfdir}/rc.d/init.d

    %{_var}               /var
    %{_tmppath}           %{_var}/tmp
    %{_usr}               /usr
    %{_usrsrc}            %{_usr}/src
    %{_lib}               lib (lib64 on 64bit multilib systems)
    %{_docdir}            %{_datadir}/doc
    %{buildroot}          %{_buildrootdir}/%{name}-%{version}-%{release}.%{_arch}
    $RPM_BUILD_ROOT       %{buildroot}

`.rpmmacros` dosyasında kullanılabilecek değişkenler.

    %{_topdir}            %{getenv:HOME}/rpmbuild
    %{_builddir}          %{_topdir}/BUILD
    %{_rpmdir}            %{_topdir}/RPMS
    %{_sourcedir}         %{_topdir}/SOURCES
    %{_specdir}           %{_topdir}/SPECS
    %{_srcrpmdir}         %{_topdir}/SRPMS
    %{_buildrootdir}      %{_topdir}/BUILDROOT