---
layout: post
title: "Scala For Yapısı"
author: "Oğuzhan İNAN"
date: 2018-04-01
categories: scala
---

__For Döngü Yapısı__    
**Yapı 1**
```scala
for( tanimlayici <- baslangis [to | until] bitis by artis ) {

}
```
**Yapı  2**

```scala
for( tanimlayici <- yineleyici ) {

}
```

__Örnek 1__   
```scala
for (x <- 1 to 10 ) println(x)              // 1 2 3 4 5 6 7 8 9 10
for (x <- 1 to 10 by 2 ) println(x)         // 1 3 5 7 9
for (x <- 1 to 20 if x%3 == 0) println(x)   // 3 6 9 12 15 18
```

__Örnek 2__   
```scala
var isimler = "ahmet,mehmet,ali,burak,can,ayşe,ceren,beyza"
for { isim <- isimler.split(",")
  if isim != null
  if isim.length > 4
} { println(isim) }  
//ahmet mehmet burak ceren beyza
```

__Örnek 3__   
```scala
var gunler =  for ( i <- 1 to 7 ) yield s"$i.Gün" 
//gunler =  Vector(1.Gün, 2.Gün, 3.Gün, 4.Gün, 5.Gün, 6.Gün, 7.Gün)

for (g <- gunler) println(g)
//1.Gün, 2.Gün, 3.Gün, 4.Gün, 5.Gün, 6.Gün, 7.Gün
```
Yukarıda ki örnekte görüldüğü gibi yield anahtar kelimesi Vector değer döndürür.