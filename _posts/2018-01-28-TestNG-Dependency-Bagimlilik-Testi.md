---
layout: post
title: "TestNG Dependency (Bağımlılık) Testi Nedir"
author: "Oğuzhan İNAN"
date: 2018-01-28
categories: java testng
---

Bazen, belirli bir sırayla testleri yapmamız gerekir. Yani bir testin çalışması için bazı testlerin önceden çalışması gerekebilir. Böyle durumlarda TestNG'nin bize sağladığı bağımlılık testlerini kullanırız. İki çeşit bağımlılık testi vardır.

- Yöntem Bağımlılığı `@Test(dependsOnMethods = {  })`
- Grup Bağımlılığı `@Test(dependsOnGroups = {  })`

### Yöntem Bağımlılığı

Bir testin çalışması için başka bir testin çalışması gerekirse kullanılabilir.

```java
public class Test {

    @Test
    public void test1() {
        System.out.println("Test1()");
    }

    @Test
    public void test2() {
        System.out.println("Test2()");
    }

    @Test(dependsOnMethods = { "test1", "test2" })
    public void test3() {
        System.out.println("Test3()");
    }

    @Test(dependsOnMethods = "test3")
    public void test4() {
        System.out.println("Test4()");
    }
}
```

```xml 
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >
<suite name="test">
    <test name="TestDependencyOnMethods">
        <classes>
            <class name="com.testng.app.Test"/>
        </classes>
    </test>
</suite>
```

Çıktı
```
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running TestSuite
Test2()
Test1()
Test3()
Test4()
Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.28 sec
```

### Grup Bağımlılığı
