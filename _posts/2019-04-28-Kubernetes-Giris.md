---
layout: post
title: "Kubernetes Giriş"
author: "Oğuzhan İNAN"
date: 2019-04-28
categories: linux kubernetes k8s docker rkt runc
---

## İçerik
1. [Kubernetes Nedir?](#kubernetes-nedir)
2. [Kubernetes Mimarisi](#kubernetes-mimarisi)
3. [Ana Sunucu (Master) Bileşenleri](#ana-sunucu-bilesenleri)
    1. [etcd](#etcd)
    2. [kube-apiserver](#kube-apiserver)
    3. [kube-control-manager](#kube-control-manager)
    4. [kube-scheduler](#kube-scheduler)
    5. [cloud-controller-manager](#cloud-controller-manager)
4. [Düğüm Sunucu Bileşenleri](#dugum-sunucu-bilesenleri)
    1. [Konteyner Platformu](#konteyner-platform)
    2. [kubelet](#kubelet)
    3. [kube-proxy](#kube-proxy)

<a name="kubernetes-nedir"></a> 
## Kubernetes Nedir?
Temel düzeyde, bir veya daha fazla makinede çalışan konteynerleri (docker, rkt) çalıştırmak ve yönetmek için Google tarafından tasarlanmış bir sistemdir. Öngörülebilirlik, ölçeklenebilirlik ve yüksek kullanılabilirlik sağlayan yöntemler kullanarak konteynerlerin yaşam döngüsünü yönetmek için kullanılır. Uygulamalarınızı yatay veya dikey ölçekleyebilir, sıfır kesinti ile güncelleyebilirsiniz.

<a name="kubernetes-mimarisi"></a>
## Kubernetes Mimarisi
Kubernetes'in bunları nasıl yaptığını anlamak için nasıl tasarlandığını anlamak önemli.   
Temelinde Kubernetes, fiziksel ve sanal makineleri, her sunucu arasında iletişim kurabilmek için bir ağ kurarak tek bir küme haline getirir. Bu küme, tüm Kubernetes bileşenlerinin, işlevlerinin ve iş yüklerinin yapılandırıldığı fiziksel platformdur.   
Kümedeki makinelerin her birini Kubernetes ekosisteminde bir rol verilmiştir. Bir sunucu ana (master) sunucu olarak kullanılır. Bu sunucu, tüm küme için bir beyin görevi görür. Diğer sunucuların sağlık kontrolünü yaparak en iyi nasıl dağıtım yapılacağına karar verir. Ana sunucu, küme ile ilk temas noktası olarak kullanılır.    
Kümedeki diğer makineler düğüm (Node) olarak adlandırılır. Her bir düğüm konteyner çalıştırabilecek (Docker, RKT) durumda olmalıdır. Uygulama ve hizmetleri düğümler çalıştırır. Düğümler ana sunucudan aldığı talimatlar ile uygun şekilde konteynerleri oluşturur veya yok eder.

<a name="ana-sunucu-bilesenleri"></a>
## Ana Sunucu (Master) Bileşenleri

<a name="etcd"></a>
### etcd  
CoreOS ekibi tarafından geliştirilen, birden fazla düğüme yayılabilecek şekilde yapılandırılabilen anahtar-değer (key-value) deposudur. Kubernetes kümedeki her bir düğüm tarafından erişilebilen yapılandırma verilerini depolamak için etcd kullanır. Bileşenlerin güncel bilgilere göre kendilerini yapılandırmasına yardımcı olur. Basit bir HTTP/JSON API sağlayarak, değerlerin ayarlanması veya ulaşılmasını sağlar. Tüm kubernetes düğümlerine erişebilmesi gereklidir.

<a name="kube-apiserver"></a>
### kube-apiserver   
En önemli ana servislerden biriside API sunucusudur. Bu, bir kullanıcının Kubernetes'in tüm birimlerini yapılandırmasına izin verdiği için tüm kümenin ana yönetim noktasıdır. Bir RESTful arayüz oluşturur, bu da birçok farklı araç ve kütüphanenin kendisiyle iletişim kurabilmesini sağlar. __kubectl__ isimli araç ile herhangi bir bilgisayardan kubernetes kümesiyle iletişime geçilebilir.

<a name="kube-control-manager"></a>
### kube-control-manager   
Kümenin durumu düzenleyen, iş yükü yaşam döngülerini yöneten ve rutin işler yapan farklı denetleyicileri yönetir. Örneğin, bir replication-controller, bir pod için tanımlanan replica (kopya) sayısının kümede halihazırda dağıtılan ile aynı olmasını sağlar. Bir işlemlerin ayrıntıları, kube-controller-manager'ın API sunucusu üzerinden değişiklikleri izlediği etcd'ye yazılır. 

Bir değişiklik görüldüğünde, controller yeni bilgileri okur ve istenen durumu yerine getiren prosedürü uygular. Bu, bir uygulamanın yukarı veya aşağı ölçeklendirilmesini, uç noktaların (endpoints) ayarlanmasını vb. içerebilir.

<a name="kube-scheduler"></a>
### kube-scheduler   
Bu servis bir iş yükünün gereksinimlerine bakıyor, mevcut altyapıyı inceliyor ve işi yükünü uygun düğüm veya düğümlere yerleştiriyor.

İş yüklerinin mevcut kaynaklardan daha fazla gereksinimi olmadığından emin olmak için her ana bigisayardaki kullanılabilir kapasitenin izlenmesinden sorumludur. Toplam kapasiteyi ve her sunucudaki mevcut iş yüklerine tahsis edilen kaynakları bilmelidir.

<a name="cloud-controller-manager"></a>
### cloud-controller-manager   
Kubernetes'in bulut sağlayıcıları (cloud provider) ile uyumlu olmasını sağlar. Google Cloud Platform, AWS, Azure gibi sağlayıcıların sağladığı ekstra özellikleri kubernetes içinde kullanmayı sağlar.

<a name="dugum-sunucu-bilesenleri"></a>
## Düğüm Sunucu Bileşenleri

<a name="konteyner-platform"></a>
### Konteyner Platformu
İlk öncelik konteyner çalıştırma ortamıdır. Bu ihtiyaç genel olarak [Docker](http://docker.io) ile karşılanır fakat alternatif olarak [rkt](https://coreos.com/rkt/) ve [runc](https://github.com/opencontainers/runc) kullanılabilir.

<a name="kubelet"></a>
### kubelet   
Her bir düğüm için ana temas noktası, kubelet adı verilen küçük bir bileşendir. Bu bileşen hizmetlerden gelen bilgileri iletmekten ve yapılandırma ayrıntılarını okumak ya da yeni değerler yazmak için etcd store ile etkileşime geçmekten sorumludur.

Kubelet bileşeni küme ile iletişime geçmek, komutları almak ve çalıştırmak için ana bileşenler ile iletişim kurar. Gerektiğinde pod'ları başlatır veya yok eder.

<a name="kube proxy"></a>
### kube-proxy   
Ana bilgisayar alt ağlarını yönetmek ve diğer bileşenlere hizmet sağlamak için, her düğüm sunucusunda kube-proxy adlı küçük bir proxy hizmeti çalıştırılır. Bu işlem talepleri doğru konteynerlere iletir, ilkel bir şekilde yük dengelemesi yapabilir ve genellikle ağ ortamının öngörülebilir ve erişilebilir olmasını sağlamaktan sorumludur.