---
layout: post
title: "Linux Nedir, Ne Yapar ?"
author: "Oğuzhan İNAN"
date: 2018-01-28
categories: linux
---

Linux, bir işletim sistemidir. İşletim sistemleri bilgisayarınızı yöneten ve üzerinde uygulamaları çalıştırmanızı sağlayan yazılımdır. Görevleri:

* __Donanım Algılama ve Hazırlama__  Bilgisayar açıldığında bilgisayardaki bileşenleri (CPU, sabit sürücü, ağ kartları vb.) yükler ve bu donanımlara ulaşmak onları kullanmak için gerekli yazılımları (sürücü ve modülleri) yükler.

* __İşlemleri Yönetme__ İşletim sistemi, aynı anda çalışan birden çok işlemi izlemeli ve sistem kaynaklarına (CPU, Bellek) nasıl ve ne zaman erişebileceğine karar vermelidir. Sistem bunu yanıda süreçleri yönetmeyi (durdurma, değiştirme vb.) yollar sunmalıdır.

* __Belleğin Yönetimi__ RAM ve takas alanı (genişletilmiş bellek), ihtiyaç duyulan uygulamalara tahsis edilmelidir. Bellğin nasıl tahsis edildiğiniden işletim sistemi sorumludur.

* __Kullanıcı Arayüzü Sağlanması__ İşletim sistemi, sisteme erişmenin yollarını da sağlamalıdır. İlk işletim sistemlerinde, kabuk adı verilen bir komut satırı yorumlayıcısı vasıtasıyla erişim sağlanırdı. Günümüzde grafik masaüstü arayüzleri kullanılır.

* __Dosya Sistemlerini Yönetme__ Dosya sistemi yapıları işetim sistemine dahildir veya modüller halinde yüklenirler. İşletim sistemi, dosya sistemlerinin içerdiği dosyalar ve dizinlere sahipliklerini ve bu dosyalara erişimi denetler.

* __Kullanıcı Erişimi ve Kimlik Duğrulama__  Kullanıcı hesapları oluşrturma ve kullanıcılar arasında sınırların belirlenmesine izin verilmesi Linux'un temel bir özelliğidir. Ayrı kullanıcı ve grup hesapları, kullanıcıların kendi dosya ve süreçlerini kontrol etmesini sağlar.

* __Yönetim için Yardımcı Programlar Sunma__ Linux'da yüzlerce komut ve grafik arayüz kullanıcı ekleme, diskleri yönetme, ağın izlenmesi gibi işler için kullanılır.

* __Servislerin Başlatılması__ Yazıcıları kullanmak, günlük mesajları işlemek ve çeşitli sistem ve ağ hizmetleri sunmak için arka planda _daemon_ süreçleri çağrılır. Linux bu hizmetleri başlatmanın ve durdurmanın farklı yollarını sunar. Linux, web sayfalarını görüntülemek için web tarayıcısı içeriyor olsa da, web yayını yapan bilgisayarda olabilir.
