---
layout: post
title: "Scala Değişkenler Hakkında"
author: "Oğuzhan İNAN"
date: 2018-03-26
categories: scala
---

__Değişebilen Değişkenler (Mutable Fields):__   
Tanımlandıktan sonra değeri değişebilen değişkenlerdir. `var` anahtar kelimesi ile tanımlanırlar.
```scala
var fiyat: Int = 45
var fiyat = 45
fiyat = 50
```
İki örnekte aynı şeydir. Değişken tanımlarken veri tipini belirtmek zorunlu değildir. Veri tipi belirtilmeden tanımlanan değişkenlerin tipi otomatik olarak atanır.

__Değişmez Değişkenler (Immutable Fields)__   
Değişmez değişkenler, tanımlandıktan sonra değeri değişmeyen değişkenlerdir. `val` anahtar kelimesi ile tanımlanırlar.
```scala
val fiyat: Int = 45
val fiyat = 45 
fiyat = 50 // hata verir
```

__Çoklu Değişken Tanımlama__   
```scala
val (fiyat: Int, renk: String) = Pair(45, "Kırmızı") 
val (fiyat, renk) = Pair(45, "Kırmızı")
```

__( _ ) Operatörü__    
Scala'da `var` ile tanımlanan ve tanımlanırken tipi belirtilen değişkenlerin varsayılan değer almasını sağlar. Tek kullanım alanı bu değildir ama değişken tanımlarken bu amaçla kullanılır.
```scala
var yas: Int         = _  // 0 değeri atandı
var isim: String     = _  // null değeri atandı
var fiyat: Double    = _  // 0.0 değeri atandı
var satilik: Boolean = _  // false değeri atandı
```

__Scala Class Hiyerarşisi__     
![Class Hierarchy of Scala]({{ "/assets/img/class-hierarchy-of-scala.png" | absolute_url }})

__Strings__    
Örnek String tanımlamalarına bakalım;
```scala
var isim: String = "Burak"
var paragraf: String = """Birden fazla satırlı
                        String tanımlamak için 3 
                        tırnak işareti kullanılır."""
```

__String Önekleri__    
__s__ Öneki: String değerin içinde değişken kullanmak için kullanılır. `$` işareti ile beraber değişken ismini yazarak kullanılır. `${ }` şeklinde kullanarak da parantez içinde işlem yapılabilir.
```scala
var isim: String  = "Burak"
var onek: String  = s"Benim adım: $isim"
var yas: Int      = 18
var onek2: String = s"Bir ay sonra ${yas + 1} yaşıma giricem."
//Bir ay sonra 19 yaşıma giricem.
```

__f__ Öneki: C programlama dilindeki printf fonksiyonu gibi kullanım için kullanılır.
```scala
var fiyat: Int  = 20
var onek3: String = f"Kitabın fiyatı $fiyat%.2f TL"
// Kitabın fiyatı 20.00 TL
```

__raw__ Öneki: String değerin içindek kaçış(escape) karakterlerinin kullanımını engeller. Yani değerin içinde ne varsa direk onu yazar.
```scala
var onek4: String = raw"Bugün hava çok güzel.\n Yarında böyle \t olursa iyi olur."
// Bugün hava çok güzel.\n Yarında böyle \t olursa iyi olur.
```
Yukardaki örnekte alt satır inmesi ve bir tab boşluk bırakması gerekirken direk kaçış karakterlerini ekrana bastı.

__String Metodları__   
```scala
var str: String = "buralar yanar"
```

| Metod | Kullanım | Sonuç |
|-------|:---------|:------|
|capitalize | str.capitalize | Buralar yanar |
|count | str.count(_.equals('a')) | 4 |
|diff | str.diff("bura") | lar yanar |
|distinct | str.distinct | bural yn |
|drop | str.drop(3) | alar yanar |
|dropRight | str.dropRight(2) | buralar yan |
|dropWhile | str.dropWhile(_ != 'l') | lar yanar |
|endsWith | str.endsWith("ar") | true |
|filter | str.filter(_ != 'a') | burlr ynr |
|getBytes | str.getBytes | Array(98, 117 ... 114) |
|head | str.head | b |
|headOption | str.headOption | Some(b) |
|indexOf | str.indexOf("a") | 3 |
|isEmpty | str.isEmpty | false |
|lastIndexOf | str.lastIndexOf("r") | 12 |
|length | str.length | 13 |
|map | str.map(_.toUpper) | BURALAR YANAR |
|mkString | str.mkString("-") | b-u-r-a-l-a-r- -y-a-n-a-r |
|nonEmpty | str.nonEmpty | true |
|partition | str.partition(_> 'a') | (burlrynr, aa aa) |
|replace | str.replace("a", "e") | bureler yener |
|replaceAll | str.replaceAll("a", "e") | bureler yener |
|replaceFirst | str.replaceFirst("a", "e") | burelar yanar |
|reverse | str.reverse | ranay ralarub |
|size | str.size | 13 |
|slice | str.slice(2, 6) | rala |
|sorted | str.sorted | aaaablnrrruy |
|sortWith | str.sortWith(_ < _) | yurrrnlbaaaa |
|split | str.split(" ") | Array(buralar, yanar) |
|splitAt | str.splitAt(3) | (bur,alar yanar) |
|substring | str.substring(0,3) | bur |
|tail | str.tail | uralar yanar |
|take | str.take(3) | bur |
|takeRight | str.takeRight(3) | nar |
|takeWhile | str.takeWhile(_ != 'r') | bu |
|toLowerCase | str.toLowerCase | buralar yanar |
|toUpperCase | str.toUpperCase | BURALAR YANAR |
|zip(0 to 12) | str.zip(0 to 10) | Vector((b, 12), (u, 11), ... (r, 0)) |
|zipWithIndex | str.zipWithIndex | Vector((b, 0), (u, 1), ... (r, 12)) |