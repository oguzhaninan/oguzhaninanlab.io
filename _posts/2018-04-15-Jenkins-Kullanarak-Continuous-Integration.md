---
layout: post
title: "Jenkins Kullanarak Continuous Integration"
author: "Oğuzhan İNAN"
date: 2018-04-15
categories: java jenkins ci
---

## Jenkins Mimarisi   
![Jenkins Integration]({{ "/assets/img/jenkins-diagram.png" | absolute_url }})

Örnek bir Jenkins yapısı yukarıda gösterildiği gibidir. Bu tek Jenkins sunucusu, aşağıdaki gibi gereksinimleri karşılayacak yeterliliğe sahip değildi.

- Bazı durumlarda, yapılarınızı test etmek için birkaç farklı ortama ihtiyacınız olabilir. Bu ihtiyacı tek Jenkins sunucusu ile karşılayamazsınız.
- Çok büyük projeler her commit için build edildiğinde tek sunucu tüm yükü idare edemez.

Yukarıdaki belirtilen ihtiyaçları karşılamak için Jenkins dağıtık mimari ortaya çıkmıştır.

## Jenkins Dağıtık Mimari   

Jenkins, dağıtık yapıları yönetmek için Master-Slave mimarisi kullanır. Bu mimaride Master ve Slave TCP/IP protokolü üzerinden iletişim kurar.

### Jenkins Master   
Ana Jenkins sunucusudur. Görevleri:
* Build işlemlerini zamanlar.
* Build işlemleri için Slave oluşturur.
* Slave sunucuları izlemek.
* Build sonuçlarını sunar.
* Doğrudan build işlemi yapabilir.

### Jenkins Slave   
* Jenkins Master sunucudan gelen istekleri takip eder.
* Çeşitli işletim sistemlerinde çalışabilir.
* Master sunucudan gelen build işlemlerini istenildiği şekilde gerçekleştirir.

Aşağıdaki şemada 3 Slave sunucu yöneten bir Jenkins Master sunucu var.

![Jenkins Master Slave]({{ "/assets/img/jenkins-master-slave.png" | absolute_url }})
