---
layout: post
title: "Varnish Subroutin Nedir - 2"
author: "Oğuzhan İNAN"
date: 2019-04-11
categories: linux varnish varnishd subroutin
---

Aşağıdaki çizelgede Varnish'in bir HTTP isteğini nasıl karşıladığı gösterilmiştir. 

![varnish subroutines]({{ "/assets/img/varnish_subroutines.png" | absolute_url }})

### vcl_hash   
Varnish'in önbelleğe alınmış bir nesneyi diğerinden ayıran alt yordamıdır. Önbelleğe alınan verilere kimlik atar ve kontrol etmek için burda oluşturulan kimlik kullanılır. İstekler, önceki bir alt yordam (yani vcl_recv) bir istek için __hash__ `return (hash)` döndürdüğünde buraya yönlendirilir. Bu yordam, Varnish'in her nesneye sayısal benzersiz bir tanımlayıcı verdiği yerdir. Yerleşik VCL, nesnenin IP adresini ve URL'sini önbellek karmasına ekleyecektir. Bu bölümde, kullanıcıya özel veriler için tanımlayıcılar da ekleyebilirsiniz (örneğin, çerezler), ancak burda yanlışlıkla kullanıcıya özel verileri paylaşmamanız için dikkatli yapmanız önerilir.    
Vcl_hash, bir alt yordam olmayan ancak belirtilen nesnenin önbellekte olup olmadığına bakan __lookup__ değerini döndürerek sonlanır. Bir öğe önbellekte ise, istek vcl_hit ve vcl_deliver'a gidecektir. Öğe önbellekte değilse, önbelleğe eklenir, sonra (önbelleğe alınabileceğini varsayarsak) vcl_miss ve vcl_fetch yordamlarına gider ve önbelleğe eklenir.
Vcl_hash arama işlemini tamamladıktan sonra isteği genellikle `vcl_hit` veya `vcl_miss` olacak bir sonraki alt rutine geçirir.

### vcl_hit
Bu rutine, vcl_hash belirtilen nesneyi önbellekte başarıyla bulduğunda ve çoğu durumda değiştirilmesi gerekmediğinde erişilir. vcl_hit genellikle üç anahtar kelimeden biriyle sonlandırılır: `deliver`, `restart` veya `synth`, ancak aynı zamanda `fetch` veya `pass` ile de sonlanabilir.    
`deliver` __vcl_deliver__ yordamına gider ve önbellek nesnesini, yaşam süresi (TTL) ve izin verme süresi (grace time) dolmadığı sürece istemciye gönderir. Nesne eskiyse, nesnenin 0 veya negatif TTL olduğu anlamına gelir, yalnızca grace veya kalan süreyi korur, aynı anda arka planda bir kopyasını yeniden alarak müşteriye teslim eder.
Grace time, önbelleğe alınan öğelerin kullanım sürelerinin dolmasından sonra, yeni halini almaya çalıştığında eğer ulaşamaz ise son önbelleği ne kadar süre daha istemciye sunacağını belirtir.   
`restart`dönüş değeri işlemi yeniden başlatır ve en fazla yeniden başlatmaya ulaşıldığında "guru medidation error" döndürür. Sürekli döngüden kaçınmak için ayarlanır.   
`synth` dönüş değeri istemciye uygun durum kodunu ve sonlandırma nedenini döndürerek isteği durdurur.   
`fetch` dönüş değeri, arka planda istenen nesnenin yeni bir kopyasını alacak ve `pass` moduna geçecek ve __vcl_pass__'a geçecektir.   

### vcl_miss
`vcl_hit`'e benzer şekilde, `vcl_miss` genellikle değiştirilmek zorunda değildir. Bu alt yordam, `vcl_hash` önbellekte istenen nesneyi bulamadığında çağrılır ve ardından söz konusu nesneyi arka uç sunucudan alıp almayacağına ya da önbelleğe alınamazsa `vcl_pass` değerini döndürmeye karar verir. `vcl_miss` yukarıda açıklandığı gibi `fetch`, `pass`, `restart` veya `synth` ile sonlandırılır. Nesne henüz önbellekte olmadığından, nesne arka uçtan alınmadan önce teslim edilmeyecektir.

### vcl_pass
Bu yordam, bir öğe önbelleğe alınmadığında ve önbelleklenebilir olarak etiketlenmediğinde çağrılır. Bu durumda, istek, `vcl_backend_fetch` aracılığıyla arka uca iletilecek ve daha sonra istemciye, `vcl_deliver` aracılığıyla önbelleğe kaydetmeden teslim edilecektir. `vcl_hit` veya `vcl_miss`'te yakalanmayan istekler genellikle bu duruma geçer. `vcl_pass`, `fetch`, `restart` veya `synth` ile sonlandırılabilir ve genellikle değiştirilmesi gerekmez.

### vcl_backend_fetch
İstenen nesneyi arka uçtan almadan önce çağrılır. İsteği, bu bölümde arka uca gitmeden önce değiştirebilir veya isteği `vcl_synth`'e gönderen ve istemciye 503 hatası gönderecek olan `abandon` sona erdirir.

### vcl_backend_response
`vcl_backend_fetch`'ten sonra, Varnish'in arka uçtan yanıtı başarıyla aldığında gerçekleşir. Bu bölüm, Varnish'te isteklerin önbelleğe alınabilmesi için maximum age: 0” gibi ayarları geçersiz kılmak için başlıkları düzenleyebilirsiniz. Birçok kaynak, içeriği önbelleğe alacak şekilde yapılandırılmadığından çoğu zaman bu bölümde düzenlemeler yapmanız gerekir.   
Bu bölüm `deliver`, `abandon` veya `retry` ile sona erebilir. `deliver`, dönüş değeri yanıtın önbelleğe alınabilir olup olmadığına bakacak önbelleklenebilir ise belirtilen TTL ile bir önbellek nesnesi oluşturacaktır. `abandon` isteği durduracak ve 503 koduyla `vcl_synth`’e gönderecek. `retry` bu noktada `vcl_backend_error`'a ileteceği maksimum deneme sayısına ulaşana kadar yeniden deneyecek.

### vcl_deliver
İstemciye bir istek teslim edilmeden önce çağrılan son alt yordamdır ve istemciye gönderilmeden önce yanıt başlıklarını değiştirmek için kullanılabilir. Bu rutin, `deliver`, `restart` veya `synth` ile sonlandırılabilir. Burada geçersiz kılmak veya oluşturmak için `set` kullanılarak değiştirilebilecek değişkenlerin bazıları ve geri almak veya silmek için `unset` ifadesi, istemci başlıkları için `resp.http`, durum kodu için `resp.status` ve önbellek sayısı için `obj.hits`.