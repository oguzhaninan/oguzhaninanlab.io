---
layout: post
title: "Kubernetes Ağ Yapısı"
author: "Oğuzhan İNAN"
date: 2020-03-04
categories: docker kubernetes network pod
---

Kubernetes, konteyner uygulamalarının sunucu kümeleri arasında dağıtımını ve çalışmasını yönetebilen bi araçtır. Konteyner iş yüklerini koordine etmenin yanı sıra Kubernetes, uygulamalarınız ve hizmetleriniz arasında güvenilir ağ bağlantısını sağlamak için gerekli altyapıyı ve araçları sağlar.   

## Pod Ağı

Kubernetes'te, bir `pod` en temel organizasyon birimidir: hepsi birbiriyle yakından ilişkili olan ve tek bir hizmet gerçekleştiren bir grup konteynerden oluşur.

Her `pod` bölmeye tek bir benzersiz IP adresi verilir ve `pod` içindeki tüm konteynerler bu adresi paylaşır ve `lo` *loopback* arabirimi üzerinden *localhost* ana bilgisayar adını kullanarak birbirleriyle iletişim kurar. Bu işlem, `pod`'un konteynerlerinin tümünü aynı ağ yığınına atayarak gerçekleştirilir. Tüm servislerin dinlemek için benzersiz bir port kullanması gerekir.

## Pod'dan Pod'a İletişim
Çoğu Kubernetes kümesi `node` başına birden çok `pod` dağıtır. Aynı `pod` içerisinde iki konteyner arasında veya iki farklı `node` arasında gerçekleşebilir.

### Tek Node İçinde Pod'dan Pod'a İletişim
Tek bir `node`'da, birbirleriyle doğrudan iletişim kurması gereken birden fazla `pod`'a sahip olabilirsiniz. Bir paketin `pod`'lar arasındaki yolunu izlemeden önce, bir düğümün ağ kurulumuna bakalım.
![pod-network]({{ "/assets/img/pod-to-pod-network.png" | absolute_url }})
*Resim: (digitalocean.com)*
Her `node`un, Kubernetes küme ağına bağlı bir ağ arabirimi (bu örnekte `eth0`) vardır. Bu arabirim, `node'un `root` ağ ad alanında bulunur. Bu, Linux'ta ağ aygıtları için varsayılan ad alanıdır.

Nasıl işlem ad alanları kapsayıcıların çalışan uygulamaları birbirinden ayırmasına izin verirse, ağ ad alanları da arabirimler ve köprüler gibi ağ aygıtlarını izole eder. Bir `node`'daki her `pod`'a kendi yalıtılmış ağ ad alanı atanır.

`Pod` ad alanları, sanal bir `ethernet` çifti ile `root` ad alanına geri bağlanır, aslında her iki uçta bir arabirime sahip iki ad alanı arasında bir boru (burada `root` ad alanında `veth1` ve bölmede `eth0` kullanıyoruz).

Son olarak, `pod`'lar birbirine ve düğümün `eth0` arayüzüne bir köprü, `br0` (`node`, `cbr0` veya `docker0` gibi bir şey kullanabilir) aracılığıyla bağlanır . Bir köprü, trafiği yönlendirmek için diğer yerel arabirimleri aramak için `ARP`(address resolution protocol) veya IP tabanlı yönlendirme kullanan fiziksel bir ethernet anahtarı gibi çalışır.

`pod1`'den `pod2`'ye bir paket adımları:
1. `pod1`, `pod2`'nin IP'sini hedef olarak içeren bir paket oluşturur
1. Paket, sanal ethernet çifti üzerinden `root` ağ ad alanına gider
1. Paket `br0` köprüsüne devam ediyor
1. Hedef bölme aynı düğümde olduğundan, köprü paketi `pod2`'nin sanal ethernet çiftine gönderir
1. Paket sanal ethernet çifti üzerinden `pod2`'nin ağ ad alanına ve pod'un `eth0` ağ arayüzüne geçer

### İki Düğüm Arasında Pod'dan Pod'a İletişim
Bir kümedeki her `pod`'un benzersiz bir IP'si olduğundan ve her bölme diğer tüm bölmelerle doğrudan iletişim kurabildiğinden, iki farklı `node`'daki bölmeler arasında hareket eden bir paket önceki senaryoya çok benzer.

Farklı bir düğümde bulunan `pod1`'den `pod3`'e bir paketin adımları:
![pod-network]({{ "/assets/img/pod-to-pod-network-2.png" | absolute_url }})
*Resim: (digitalocean.com)*

1. `pod1`, `pod3`'ün IP'sini hedef olarak içeren bir paket oluşturur
1. Paket, sanal ethernet çifti üzerinden `root` ağ ad alanına gider
1. Paket `br0` köprüsüne devam ediyor
1. Köprü, yönlendirilecek hiçbir yerel arabirim bulamadığı için paket `eth0` yönündeki varsayılan yolu gönderir
1. (Eğer): Kümeniz paketleri düzgün bir şekilde `node`'lara yönlendirmek için bir ağ katmanı gerektiriyorsa, ağa gitmeden önce paket bir [VXLAN](https://en.wikipedia.org/wiki/Virtual_Extensible_LAN) paketine (veya başka bir ağ sanallaştırma tekniğine) yerleştirilebilir. Alternatif olarak, ağın kendisi uygun statik yollarla kurulabilir, bu durumda paket eth0'a  gider ve ağı değiştirmeden dışarı çıkar.
1. Paket küme ağına girer ve doğru `node`'a yönlendirilir.
1. Paket `eth0` üzerindeki hedef `node`'a girer
1. (Eğer): paketiniz kapsüllenmişse, bu noktada kapsülden çıkarılacaktır
1. Paket `br0` köprüsüne devam ediyor
1. Köprü, paketi hedef `pod`'un sanal ethernet çiftine yönlendirir
1. Paket, sanal ethernet çiftinden `pod`'un `eth0` arayüzüne geçer

## Pod - Servis İletişimi
Kubernetes kümesinin dinamik yapısı, `pod`'ları hareket ettirilebileceği, yeniden başlatılabileceği, yükseltilebileceği veya ölçeklendirilebileceği anlamına geldiği için, yalnızca `pod` IP'leri kullanarak belirli bir uygulamaya trafik göndermek zor olacaktır. Buna ek olarak, bazı hizmetlerin çok sayıda kopyası olacaktır, bu nedenle aralarında yüke dengelemek için bir yol bulmak gerekir.

Kubernetes bu sorunu Servisler ile çözer. Servis, tek bir sanal IP adresini (Virtual IP) bir dizi `pod` IP'si ile eşleyen bir API nesnesidir. Ek olarak, Kubernetes her servisin adı ve sanal IP'si için bir DNS kaydı sağlar, böylece servisler adıyla kolayca ele alınabilir.

Sanal IP'lerin küme içindeki `pod` IP'lerine eşlenmesi her düğümdeki `kube-proxy` aracı ile yapılır. Bu işlem, paketi küme ağına göndermeden önce VIP'leri otomatik olarak `pod` IP'lerine çevirmek için `iptables` veya [IPVS](https://en.wikipedia.org/wiki/IP_Virtual_Server)'yi ayarlar. Ayrı bağlantılar izlenir, böylece paketler geri döndüklerinde düzgün bir şekilde çevrilebilir. IPVS ve `iptables` her ikisi de tek bir servis sanal IP'sinin birden fazla `pod` IP'sine yük dengelemesi yapabilir, ancak IPVS'nin kullanabileceği yük dengeleme algoritmalarında çok daha fazla esnekliğe sahiptir.

`pod1`'den `service1`'e giden bir paketin adımları:
![pod-network]({{ "/assets/img/pod-to-pod-network-2.png" | absolute_url }})
*Resim: (digitalocean.com)*

1. `pod1`, `service1`'in IP adresini hedef olarak içeren bir paket oluşturur
1. Paket, sanal ethernet çifti üzerinden `root` ağ ad alanına gider
1. Paket `br0` köprüsüne devam ediyor
1. Köprü, paketi yönlendirmek için hiçbir yerel arabirim bulamadığından, paket `eth0` yönündeki varsayılan yolu gönderir.
1. kube-proxy tarafından ayarlanan Iptables veya IPVS, paketin hedef IP'sini eşleştirir ve hangi yük dengeleme algoritmalarının kullanılabileceğini veya belirtildiğini kullanarak sanal bir IP'den hizmetini pod IP'lerinden birine dönüştürür
1. (İsteğe bağlı): paketiniz bu noktada kapsüllenebilir
1. Paket küme ağına girer ve doğru `node`'a yönlendirilir.
1. Paket `eth0` üzerindeki hedef `node`'a girer
1. (İsteğe bağlı): paketiniz kapsüllenmişse, bu noktada kapsülden çıkarılacaktır
1. Paket `br0` köprüsüne devam ediyor
1. Paket `veth1` aracılığıyla sanal ethernet çiftine gönderilir
1. Paket, sanal ethernet çiftinden geçer ve `eth0` ağ arabirimi aracılığıyla `pod` ağı ad alanına girer

[Kaynak 1](https://tr.wikipedia.org/wiki/Adres_%C3%87%C3%B6z%C3%BCmleme_Protokol%C3%BC) - 
[Kaynak 2](https://www.digitalocean.com/community/tutorials/kubernetes-networking-under-the-hood) - 
[Kaynak 3](https://en.wikipedia.org/wiki/Virtual_Extensible_LAN) - 
[Kaynak 4](https://en.wikipedia.org/wiki/IP_Virtual_Server)
