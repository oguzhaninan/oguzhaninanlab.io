---
layout: post
title: "Scala Nedir Ne Değildir"
author: "Oğuzhan İNAN"
date: 2018-03-22
categories: scala
---

Scala ismi "scalable language" yani ölçeklenebilir dil'den gelir. __Scala programlama dili__  [Martin Odersky](https://twitter.com/odersky) tarafından 2001 yılında başlatılmış bir projedir. 20 Ocak 2004 tarihinde yayınlanmıştır.
### __Neden Scala?__
#### __JVM Dili:__
JVM üzerinde çalışan java ile yazılan kütüphaneleri kullanabilen bir dildir.
#### __Statik Bir Dildir:__
Statik diller veri türleri kullanır. Dinamik dillerin aksine daha güvenli ve hızlıdırlar. Dinamik tipli diller veri türü kullanmadıkları için program çalışırken tip kontrolü yapılır bu da hız kaybına yol açar. Statik diller derlenirken kontrol edildikleri için çalışırken ekstra bir kontrol yapılmaz. Ayrıca dinamik dillerde tip kontrolü çalışma esnasında yapıldığı için tip bilgileri bellekte tutulur bu da bellek kaybına yol açar statik dillerde böyle birşey yoktur.
#### __Karma Paradigma - Nesneye Yönelik Programlama:__
Scala nesneye yönelikli programlamayı destekler javanın aksine herşey bir nesnedir.
#### __Karma Paradigma - Fonksiyonel Programlama:__
Scala fonksiyonel programlamayı destekler. Fonksiyonel programlama daha büyük ölçekli programlar yazmak için idealdir.
#### __Esnek Sintaks:__
Java'dan daha esnek bir sintaksı vardır. Daha az kod ile daha çok iş yaparsınız.
#### __SBT - Standart Build Tool Kurulumu:__
Aşağıdaki adresden kurulumu basit bir şekilde yapabilirsiniz.

[http://www.scala-sbt.org/download.html](http://www.scala-sbt.org/download.html)

Kurduktan sonra konsolu açıp "sbt test" komutuyla gerekli 3. parti uygulamaları kurmasını sağlıyoruz. Kurulumlar bittikten sonra konsol ekranına "sbt" yazarak kullanmaya başlayabiliriz.

<span style="color: #339966;">__$ sbt__</span>

|Komut | Açıklama |
|-------------|:------------|
|__<span style="color: #339966;">&gt; help</span>__ | <span style="color: #ff9900;">Komut yardımı için kullanılır.</span> |
|__<span style="color: #339966;">&gt; tasks</span>__ | <span style="color: #ff9900;">En yaygın kullanılan mevcut görevleri gösterir.</span> |
|__<span style="color: #339966;">&gt; tasks -V</span>__ | <span style="color: #ff9900;">Bütün mevcut görevleri göster.</span> |
|__<span style="color: #339966;">&gt; compile</span>__ | <span style="color: #ff9900;">Adım adım kod derlemek için kullanılır.</span> |
|__<span style="color: #339966;">&gt; test</span>__ | <span style="color: #ff9900;"> Adım adım çalıştırıp test etmek için kullanılır.</span> |
|__<span style="color: #339966;">&gt; clean</span>__ | <span style="color: #ff9900;">Tüm yapıları siler.</span> |
|__<span style="color: #339966;">&gt; console</span>__ | <span style="color: #ff9900;">Scala REPL(Read, Eval, Print, Loop) çalıştırır.</span> |
|__<span style="color: #339966;">&gt; run</span>__ | <span style="color: #ff9900;">Derlenmiş class dosyasını çalıştırır.</span> |
|__<span style="color: #339966;">&gt; show</span>__ x | <span style="color: #ff9900;">Belirtilen değişkeni gösterir.</span> |
|__<span style="color: #339966;">&gt; eclipse</span>__ | <span style="color: #ff9900;">Eclipse projesi oluşturur.</span>|

#### __Scala Projesi Oluşturma:__
SBT ile proje oluşturmak için konsol ekranında `sbt new sbt/scala-seed.g8` komutunu çalıştıralım. Bizden bir proje ismi isteyecek ismi (küçük harfler ile) verdikten sonra projemiz hazır.


Projemizin bulunduğu dizine girerek __sbt__ komutunu ardından __run__ komutu ile projemizi derleyip çalıştırıyoruz.
### __Scala ve Java__
__Scala__
```scala
object ScalaTest{
    def main(args: Array[String]): Unit = {
        println("New Project")
    }
 }
```
__Java__
```java
public class ScalaTest {
    public static void main(String args[]){
        System.out.println("New Project");
    }
 }
```
