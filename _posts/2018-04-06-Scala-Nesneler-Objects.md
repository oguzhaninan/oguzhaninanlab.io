---
layout: post
title: "Scala (Nesneler) Objects"
author: "Oğuzhan İNAN"
date: 2018-04-06
categories: scala
---

__Object Tanımlama__   
```scala
object Nesne {
  def main(args: Array[String]){
    println("Object")
  }
}
```
Yukarıda nesne isminde bir object tanımladık. Objectler extends edilemezler. Object içindeki değişkenlere ve fonksiyonlara direk ulaşılır. Yukarıdaki kodun JVM tarafından derlendiği haline bakalım.
```scala
public final class Nesne {
  public static void main(java.lang.String[]);
}
```
Java'da final olarak tanımlanan classlar da extend edilemez.

__Örnek:__
```scala
object Adres {
  var sokak: String = ""
  var mah: String = ""
  def yazdir(): Unit = println(s"Sokak: $sokak Mahalle: $mah")
}

object Ornek {
  //Uygulamanın giriş noktası javada ki main metodu ile aynı
  def main(args: Array[String]): Unit = {  
    Adres.sokak = "Kurtuluş"
    Adres.mah   = "Atatürk"
    Adres.yazdir  // Sokak: Kurtuluş Mahalle: Atatürk
  }
}
```

__Örnek 2__   
```scala
class Islemler {
  def Ortalama(sayilar: Int*): Double = {
    var ort: Double = 0
    for(x <- sayilar)
    ort += x.toDouble / sayilar.length

    ort
  }
}

object app extends Islemler {
  def main(args: Array[String]): Unit = {
    val ort: Double = Ortalama(2,6,4,3,7,4,4,35,34,4,65,5,34,6)
    println(f"Ortalama: $ort%.2f ")  // Ortalama: 15.21
  }
}
```