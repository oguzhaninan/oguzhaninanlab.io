---
layout: post
title: "TestNG Exception Testi Nedir"
author: "Oğuzhan İNAN"
date: 2018-01-28
categories: java testng
---

TestNG yazılan kodun istisna (exception) durumlarını kontrol etme imkanı verir.
Yazılan kodun istenen istisnayı fırlatıp fırlatmadığını kontrol edebiliriz.
@Test açıklamasının `expectedExceptions` parametresine verilecek istisnalar testte fırlatılmazsa test başarısız olur.

```java 
public class ExceptionTest {

    @Test(expectedExceptions = {ArithmeticException.class})
    public void exceptionTest() {
        int a = 1 / 0;
    }

}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >
<suite name="suite">
   <test name="exceptionTest">
      <classes>
         <class name="com.testng.app.ExceptionTest" />
      </classes>
   </test>
</suite>	
```

Yukarıdaki durumda test başarılı olur. 
1 / 5 yaparsak eğer bir hata olmayacağı için test başarısız olur.

```java 
public class ExceptionTest {

    @Test(expectedExceptions = {ArithmeticException.class})
    public void exceptionTest() {
        int a = 1 / 5;
    }

}
```
