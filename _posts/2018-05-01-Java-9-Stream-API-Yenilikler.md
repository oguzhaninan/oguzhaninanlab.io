---
layout: post
title: "Java 9 Stream API Yenilikler"
author: "Oğuzhan İNAN"
date: 2018-05-01
categories: java java9
---

Java Stream API'ye eklenen yeni metodlar:

## Stream.takeWhile()   
Bir Stream'den okuma yapılırken belirtilen koşula uymayana kadar veriler alınır. Koşula uymaan bir durum olduğunda okuma işlemi durur.

```java
List<Integer> numbers = List.of(4,8,12,34,55,12,4);

numbers.stream().takeWhile(n-> n % 2 == 0).forEach(System::out::println);

// çıktı => 4 8 12 34
```

12 ve 4 sayılarıda 2 ye bölünebilir ama 2 ye bölünemeyen 55 sayısından sonra daha okuma yapılmaz. 

## Stream.dropWhile()    
`Stream.takeWhile()` metodunun tam tersidir. Bir Stream'den okuma yapılırken belirtile koşula ilk uymayan yerden sonrasını döndürür.

```java
List<Integer> numbers = List.of(4,8,12,34,55,12,4);

numbers.stream().dropWhile(n-> n % 2 == 0).forEach(System::out::println);

// 55 12 4
```
   
55 sayısı 2 ye bölünmediği için o ve ondan sonraki elemanlar döndürülür.

## Stream.ofNullable()   

```java
Stream.ofNullable("Example").forEach(System::out::println);
// Example

Stream.ofNullable(null).forEach(System::out::println);
```
   
## Stream.iterate()   
Stream API `iterate` metoduna sahiptir. Bu metod verilen koşula uygun Stream oluşturur. Örnek kullanımı:

```java
Stream.iterate(1, i -> i+1);

// 1,2,3,4,5,...
```

İstenirse `limit` metodu kullanılarak üretilecek eleman sayısı sınırlanabilir.

```java
Stream.iterate(1, i -> i+1).limit(5).forEach(System::out::println);

// 1 2 3 4 5
```

`limit` metodu bir yere kadar iyi fakat bazı durumlarda belli bir koşula göre durulması istenebilir. Java 9 ile koşula göre Stream oluşturma özelliği geldi.

```java 
Stream.iterate(1, i  -> i <= 5, i -> i+1).forEach(System::out::println);

// 1 2 3 4 5
```

