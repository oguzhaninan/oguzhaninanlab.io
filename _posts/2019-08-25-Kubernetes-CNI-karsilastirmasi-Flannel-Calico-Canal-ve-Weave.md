---
layout: post
title: "Kubernetes CNI Karşılaştırması: Flannel, Calico, Canal ve Weave"
author: "Oğuzhan İNAN"
date: 2019-08-25
categories: kubernetes cni flannal calico
---

![cni]({{ "/assets/img/k8s-cni.png" | absolute_url }})
![cni]({{ "/assets/img/k8s-cni.png" | absolute_url }})

# Giriş

Ağ mimarisi, Kubernetes kurulumunun en karmaşık yönlerinden biridir. Kubernetes ağ modelinin kendisi [belli ağ özelliklerini](https://kubernetes.io/docs/concepts/cluster-administration/networking/#the-kubernetes-network-model) gerektiriyor ancak uygulama konusunda bir miktar esneklik sağlıyor. Sonuç olarak, belirli ortamlara ve gereksinimlere yönelik çeşitli projeler yayınlanmıştır.

Bu yazıda en popüler CNI eklentilerini inceleyeceğiz: flannel, calico, weave, and canal. CNI, konteynerler oluşturulduğunda veya imha edildiğinde konteyner ağını yapılandırmayı kolaylaştırmak için tasarlanmış bir standart olan konteyner ağı arayüzü ([Container Network Interface](https://github.com/containernetworking/cni)) anlamına gelir. Bu eklentiler, Kubernetes'in ağ gereksinimlerinin karşılandığından emin olmak ve küme yöneticilerinin ihtiyaç duyduğu ağ iletişimi özelliklerini sağlamak için kullanılır.

# Arka Plan

Konteyner ağı, konteynerlerin isteğe bağlı olarak diğer konteynerlere, ana bilgisayara ve internet gibi dış ağlara bağlanabildiği mekanizmadır. Konteyner çalışma zamanları (runtime), her biri farklı bir ihtiyacı karşılayan sonuçlanan ağ modları sunar. Örneğin [Docker](https://www.docker.com/), bir konteyner için aşağıdaki ağları varsayılan olarak yapılandırabilir:

- **none** : Konteyneri, bağlantısı olmayan bir konteynere özgü ağ yığınına ekler.
- **host** : Konteyneri hiçbir yalıtımı olmadan ana makinenin ağ yığınına ekler.
- **default bridge** : Varsayılan ağ modudur. Her bir konteyner IP adresi ile birbirleriyle bağlantı kurabilir.
- **custom bridge** : Ek esneklik, yalıtım ve kolaylık özelliklerine sahip, kullanıcı tanımlı köprü ağları.

Docker ayrıca, ek sürücüler ve eklentilerle çok ana bilgisayarlı ağlar da dahil olmak üzere daha gelişmiş ağ yapılandırmanıza olanak tanır.

CNI ihtiyacı, konteynerler başladığında veya yok edildiğinde uygun ağ yapılandırmasını ve kaynaklarını dinamik olarak yapılandırmak için ortaya çıkmıştır.

Eklentiler, arayüze bir IP adresi sağlamaktan ve yönetmekten sorumludur ve genellikle IP yönetimi, konteyner başına IP atama ve çok ana bilgisayar bağlantısı ile ilgili işlevsellik sağlar. Konteyner çalışma zamanı, IP adreslerini tahsis etmek ve konteyner başladığında ağı yapılandırmak için ağ eklentilerini çağırır ve bu kaynakları temizlemek için konteyner silindiğinde yeniden çağırır.

Çalışma zamanı veya orkestratör (kubernetes, swarm) ağa bir konteynerin katılması gerektiğine ve çağırması gereken eklentiye karar verir. 

Kubernetes kullanıldığında, `kubelet` bulduğu eklentileri uygun zamanlarda arayarak başlattığı `pod`'lar için ağı otomatik olarak yapılandırmaya izin verir .

# Terminoloji
Mevcut CNI eklentilerine bir göz atmadan önce, CNI ile ilgili yazıları okurken görebileceğiniz bazı terimleri inceleyelim.

- **Layer 2 networking**     
OSI (Open Systems Interconnection) ağ modelinin "veri bağlantısı" (data link) katmanıdır. Network katmanından aldığı paketleri frame'lere bölerek fiziksel katmana iletir. Bu bölme işlemini yaparken frame'lere CRC (Hata  Kontrol Kodu) kodunu, kaynak ve hedef MAC adreslerini de ekler. Ağ kartı ile doğrudan iletişim kurar. Bu katmanda switch ve bridge'ler çalışır. 

- **Layer 3 networking**    
OSI ağ modelinin "ağ" katmanı. Bu katmanın ana görevi yönlendirmedir (routing). Yönlendirme işlemi yerel ağ dışında bulunan diğer  ağlara paketlerin iletilmesini sağlar. Buradaki işlemler yönlendirme protokolleri ile sağlanmaktadır. Bu katmanda fiziksel olarak  router (yönlendiriceler) cihazları çalışmaktadır.

- **VXLAN**    
"Sanal Genişletilebilir LAN" (Virtual Extensible LAN) anlamına gelir. VXLAN, geniş çaplı bulut bilişim dağıtımlarında görülen ağların ölçeklenebilirlik sorunlarına bir çözüm amacıyla tasarlanan yeni bir ağ sanallaştırma tekniği olarak tanımlanmaktadır. VXLAN teknik olarak Ethernet protokolünün tünellenmesi yöntemi ile çalışır. Tünelleme işlemini, UDP paketine veya ilgili cihazlarla uyumlu bir taşıma mekanizması içine eklenmiş yeni bir Ethernet frame (header bilgisi olarak ekstradan birkaç yeni byte eklenmiş şekilde) gerçekleştirir.

- **Overlay network**    
Mevcut bir ağın üzerine kurulmuş sanal, mantıksal bir ağdır. Genellikle mevcut ağların üstüne faydalı soyutlamalar sağlamak ve farklı mantıksal ağları ayırmak ve güvence altına almak için kullanılır.

- **Encapsulation**    
Kapsülleme, ek bağlam ve bilgi sağlamak için ağ paketlerini ek katmanlara sarma işlemidir. **Overlay** ağlarında, kapsülleme, sanal ağdan altta yatan adres alanına çevirmek için farklı bir konuma yönlendirmede kullanılır (burada paket kapsüllenebilir ve hedefine devam edilebilir).

- **Mesh network**    
Her bir düğümün, yönlendirme konusunda işbirliği yapmak ve daha fazla bağlantı sağlamak için diğer birçok düğüme bağlandığı ağdır. Birden çok yoldan yönlendirmeye izin vererek daha güvenilir bir ağ iletişimi sağlar.

# CNI Karşılaştırma

## Flannel

![flannel]({{ "/assets/img/flannel.jpeg" | absolute_url }})

[CoreOS](https://coreos.com/) tarafından geliştirilen bir proje olan [Flannel](https://github.com/coreos/flannel), belki de en basit ve popüler CNI eklentisidir. Daha iyi konteynerler arası ve host arası ağ iletişimi sağlamak için tasarlanan, konteyner orkestrasyon sistemleri için en olgun örneklerden biridir.

Diğer bazı seçeneklerle karşılaştırıldığında, Flannel'in kurulumu ve yapılandırması nispeten kolaydır. Flannel adı verilen tek bir ikili dosya olarak paketlenmiştir ve varsayılan olarak pek çok Kubernetes küme dağıtım aracı tarafından yüklenebilir. Flannel, özel bir veri deposu sağlamak zorunda kalmamak için API kullanarak durum bilgilerini saklamak için Kubernetes kümesinin mevcut `etcd` kümesini kullanabilir.

Flannel, katman 3 IPv4 `overlay` ağını yapılandırır. Küme içindeki her düğüme yayılan büyük bir iç ağ oluşturulur. Bu bindirme ağı içinde, her düğüme IP adreslerini dahili olarak tahsis etmek için bir alt ağ verilir. Bölmeler sağlandığında, her düğümdeki Docker köprü arabirimi her yeni konteyner için bir adres ayırır. Aynı ana bilgisayar içindeki bölmeler Docker köprüsünü kullanarak iletişim kurabilirken, farklı ana bilgisayarlardaki bölmeler trafiklerini UDP paketlerinde uygun hedefe yönlendirmek için kapatacaktır.

Flannel, kapsülleme ve yönlendirme için farklı tiplerde arka uçlara sahiptir. Varsayılan ve önerilen yaklaşım, hem iyi bir performans sunması hem de diğer seçeneklerden daha az manuel müdahale olması nedeniyle VXLAN'ı kullanmaktır.

Genel olarak, Flannel çoğu kullanıcı için iyi bir seçimdir. Yönetim için, yalnızca temel bilgilere ihtiyaç duyduğunuzda çoğu kullanım durumuna uygun bir ortam oluşturan basit bir ağ oluşturma modeli sunar. Genel olarak, sağlayamayacağı bir şeye ihtiyacınız olmadan Flannel ile başlamak güvenli ve kolaydır.

## Calico

![calico]({{ "/assets/img/calico.png" | absolute_url }})

Calico, Kubernetes ekosisteminde bir başka popüler seçenektir. Flannel basit seçenek olarak konumlanmış olsa da, Calico en iyi performansı, esnekliği ve gücü ile bilinir.

Sistem gereksinimlerini karşılayan yeni hazırlanmış bir Kubernetes kümesinde, Calico tek bir dosya ile hızla dağıtılabilir. Calico’nun isteğe bağlı ağ politikası özellikleriyle ilgileniyorsanız, kümenize ek bir bildirim uygulayarak bunları etkinleştirebilirsiniz.

Calico'yu dağıtmak için gereken işlemler oldukça basit gibi görünse de, oluşturduğu ağ ortamı hem basit hem de karmaşık niteliklere sahiptir. Flannel'den farklı olarak, Calico bir kaplama ağı kullanmaz. Bunun yerine Calico, paketleri ana bilgisayarlar arasında yönlendirmek için BGP yönlendirme protokolünü kullanan bir katman 3 ağı yapılandırır. Bu, ana bilgisayarlar arasında hareket ederken paketlerin ekstra bir kapsülleme tabakasına sarılmasına gerek olmadığı anlamına gelir. BGP yönlendirme mekanizması, ek bir trafik katmanında trafiği sarmak için fazladan bir adım olmaksızın paketleri yerel olarak yönlendirebilir.

Bu, sunduğu performansın yanı sıra, bunun bir yan etkisi, ağ sorunları ortaya çıktığında daha geleneksel sorun gidermeye olanak sağlamasıdır. VXLAN gibi teknolojilerin kullanıldığı kapsüllenmiş çözümler iyi sonuç verirken, süreç paketleri izlemeyi zorlaştıracak şekildedir. Calico ile standart hata ayıklama araçları, basit ortamlardaki aynı bilgilere erişebilir ve bu sayede daha fazla sayıda geliştiricinin ve yöneticinin durumu anlamasını kolaylaştırır.

Ağ bağlantısına ek olarak, Calico gelişmiş ağ özellikleriyle tanınır. Ağ politikası, en önemli özelliklerinden birisidir. Ek olarak, Calico hem hizmet ağı katmanında hem de ağ altyapısı katmanında küme içindeki iş yükleri politikasını uygulamak için bir hizmet ağı olan [Istio](https://istio.io/) ile de birlikte kullanılabilir. Bu, pod'ların nasıl trafik gönderip alabileceklerini, güvenliği artıran ve ağ ortamınız üzerinde kontrol edebileceğini açıklayan güçlü kuralları yapılandırabileceğiniz anlamına geliyor.

Calico, gereksinimlerini destekleyen ortamlar için ve ağ politikası gibi performans ve özellikler önemli olduğunda iyi bir seçimdir. Ek olarak, bir destek sözleşmesi arıyorsanız veya bu seçeneği gelecek için açık tutmak istiyorsanız Calico ticari destek sunar. Genel olarak, ağınızı yalnızca bir kez yapılandırmak ve unutmak yerine kontrol edebilmek istediğinizde bu iyi bir seçimdir.

## Canal

![Canal]({{ "/assets/img/canal.png" | absolute_url }})

Her şeyden önce, Canal, Flannel tarafından sağlanan ağ katmanını Calico'nun ağ politikası özellikleriyle bütünleştirmek isteyen bir projenin adıydı. Bununla birlikte, katkıda bulunanlar detaylar üzerinde çalıştıkça, standardizasyon ve esneklik sağlamak için her iki projede de çalışılması durumunda tam bir entegrasyonun gerekli olmadığı açıkça görmüşler. Sonuç olarak, resmi proje biraz ertelendi, ancak iki teknolojiyi bir araya getirme hedefine ulaşıldı.

Canal, Flannel ve Calico'nun bir kombinasyonu olduğu için, faydaları da bu iki teknolojinin kesiştiği noktadadır. Ağ katmanı, fazla ek yapılandırma olmadan birçok farklı dağıtım ortamında çalışan Flannel tarafından sağlanan basit bir kaplamadır. Ağ politikası yetenekleri, üst ağa en üst düzeyde güvenlik ve kontrol sağlamak için Calico’nun güçlü ağ kuralı değerlendirmesiyle birlikte daha güçlü bir seçenek sunar.

Kümelenmenin gerekli sistem gereksinimlerini karşıladığından emin olduktan sonra, Canal, iki bildirimi uygulayarak konuşlandırılabilir ve bu da yapılandırılmasının hiçbir şekilde kendi projelerinden daha zor olmamasını sağlar. Canal, ekiplerin gerçek ağlarını değiştirmeyi denemeye hazır olmadan önce ağ politikasını denemeye ve deneyim kazanmaya başlamasının iyi bir yoludur.

Ağ politikası kurallarını tanımlama yeteneği, güvenlik açısından büyük bir avantajdır ve birçok açıdan Calico’nun katil özelliğidir. Bu teknolojiyi tanıdık bir ağ katmanına uygulayabilmek, bir geçiş sürecinden geçmeden daha yetenekli bir ortam elde edebileceğiniz anlamına gelir.

## Weave Net

![Weave]({{ "/assets/img/weave.jpg" | absolute_url }})
 
[Weaveworks](https://www.weave.works/) tarafından yaratılmış [Weave Net](https://www.weave.works/oss/net/), bahsettiğimiz diğerlerinden farklı bir paradigma sunar. Weave kümedeki düğümlerin her biri arasında `mesh overlay` ağı oluşturur ve böylece katılımcılar arasında esnek bir yönlendirme sağlar.

Ağını oluşturmak için, Weave ağdaki her ana bilgisayara yüklenen bir yönlendirme bileşenini kullanır. Bu yönlendiriciler daha sonra mevcut ağ ortamının güncel bir görünümünü korumak için topoloji bilgilerini paylaşır.

Calico gibi Weave de kümeniz için ağ politikası yetenekleri sağlar. Weave'i kurduğunuzda bu otomatik olarak kurulur ve yapılandırılır, bu nedenle ağ kurallarınızı eklemenin ötesinde ek bir yapılandırma gerekmez. Weave de diğerlerinin yapmadığı bir şey, tüm ağ için kolay şifrelemedir.

Weave, büyük miktarda karmaşıklık veya yönetim eklemeden zengin özellikli ağ arayanlar için mükemmel bir seçenektir. Kurulumu nispeten kolaydır, birçok yerleşik ve otomatik olarak yapılandırılmış özellikler sunar ve diğer çözümlerin başarısız olabileceği senaryolarda yönlendirme sağlayabilir. Ek olarak, Weave yardım ve sorun giderme için birisiyle bağlantıya geçmeyi tercih eden kuruluşlara ücretli destek sunar.

[Kaynak 1](https://www.beyaz.net/tr/network/makaleler/ag_sanallastirma_teknigi_vxlan_nedir.html)     
[Kaynak 2](https://rancher.com/blog/2019/2019-03-21-comparing-kubernetes-cni-providers-flannel-calico-canal-and-weave/)