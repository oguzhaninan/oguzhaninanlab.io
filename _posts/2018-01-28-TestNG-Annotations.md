---
layout: post
title: "TestNG Annotations (Ek Açıklamalar)"
author: "Oğuzhan İNAN"
date: 2018-01-28
categories: java testng
---

### Suite Nedir ?
Suite, test paketidir. Testler suite'ler aracılığıyla paketlenir ve çalıştırılırlar.

| Annotation   | Açıklama |
|--------------|:--------------------------------------------------------------------------|
|`@BeforeSuite`  | Test paketinde her şeyden önce bir kez çalışır.|
|`@AfterSuite`   | Test paketinde her şeyden sonra en son bir kez çalışır.|
|`@BeforeClass`  | Geçerli sınıfın ilk test yöntemi çalışmadan önce bir kez çalışır.|
|`@AfterClass`   | Geçerli sınıfın son test yöntemi de çalıştıktan sonra bir kez çalışır.|
|`@BeforeTest`   | @Test açıklamasına sahip herhangi bir test yöntemi çalışmadan önce bir kez çalışır. Her test öncesi çalışmaz. |
|`@AfterTest`    | @Test açıklamasına sahip bütün testler çalıştıktan sonra bir kez çalışır. Her test sonrası çalışmaz. |
|`@BeforeGroups` | Parametre olarak verilen grup ismine sahip testlerin ilkinden önce bir kez çalışır. Her test için çalışmaz. |
|`@AfterGroups`  | Parametre olarak verilen grup ismine sahip testlerin sonuncusundan sonra bir kez çalışır. |
|`@BeforeMethod` | Her test metodundan önce çalışır. |
|`@AfterMethod`  | Her test metodundan sonra çalışır. |

Aşağıda açıklamaların örnek bir kullanımı var.

```java
public class TestngAnnotation {
	
    @Test(groups = "grup")
    public void testCase1() {
        System.out.println("TEST 1");
    }

    @Test(groups = "grup")
    public void testCase2() {
        System.out.println("TEST 2");
    }

    @BeforeGroups("grup")
    public void beforeGroup() {
        System.out.println("BeforeGroups");
    }

    @AfterGroups("grup")
    public void afterGroup() {
        System.out.println("AfterGroups");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("BeforeMethod");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("AfterMethod");
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("BeforeClass");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("AfterClass");
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("BeforeTest");
    }

    @AfterTest
    public void afterTest() {
        System.out.println("AfterTest");
    }

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("BeforeSuite");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("AfterSuite");
    }

}
```

Çıktı
```
BeforeSuite
BeforeTest
BeforeClass
BeforeGroups
BeforeMethod
TEST 1
AfterMethod
BeforeMethod
TEST 2
AfterMethod
AfterGroups
AfterClass
AfterTest
AfterSuite
```