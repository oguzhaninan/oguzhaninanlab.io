---
layout: post
title: "Systemd ve Unit (Birim) Dosyaları Nedir?"
author: "Oğuzhan İNAN"
date: 2018-02-13
categories: linux administration
---

## İçerik
1. [Giriş](#begin)
2. [Systemd Unit'leri Neler Sağlar](#unit-advantages)
3. [Systemd Unit Dosyaları Nerede Bulunur?](#where-is-unit-files)
4. [Unit Dosya Türleri](#types-of-units)
5. [[Unit] Bölümü Direktifleri](#unit-section-directives)

<a name="begin"></a>
## Giriş
Linux dağıtımları `systemd` init sistemini benimsiyor. Bu yazılım paketi servislerden, sistem durumlarına kadar sunucunun bir çok yönünü yönetebiliyor. Systemd'de bir __unit__ (birim), sistemin üzerinde nasıl çalışacağını ve yönetileceğini bildiği herhangi bir kaynağa denk gelir. Bu kaynaklar, __unit__ dosyaları adı verilen yapılandırma dosyaları kullanılarak tanımlanır.

![Systemd-Components]({{ "/assets/img/systemd-components.png" | absolut_url }})

<a name="unit-advantages"></a>
## Systemd Unit'leri Neler Sağlar
Unit'ler görevlerine göre farklı kategorilere ayrılabilirler. Böylece kolayca etkinleştirilebilir, devre dışı bırakılabilir veya genişletilebilirler.

- __Soket Bazlı Etkinleştirme:__  
Bir servisle ilişkili soketler, ayrı ayrı ele alınması için servisin kendisinden en iyi şekilde koparılır. Bu, ilişkili sokete ilk erişime kadar bir hizmetin başlamasını geciktirmek gibi bir takım avantajlar sağlar. Bu aynı zamanda sistemin önyükleme işleminin başlangıcında tüm soketleri oluşturmasına olanak tanır ve ilişkili servisleri paralel olarak önyüklemeyi mümkün kılar.

- __Bus Bazlı Etkinleştirme:__  
D-Bus tarafından sağlanan veri yolu üzerinde etkinleştirilebilir. Unit, birleşik bir veri yolu yayınlandığında başlatılabilir.

- __Yol Bazlı Etkinleştirme:__  
Unit, belirli dosya sistemi yollarının etkinliğini veya kullanılabilirliğini temel alarak başlatılabilir.

- __Aygıt Bazlı Etkinleştirme:__  
Unit'ler, `udev events` (udev olaylarını) kullanarak ilişkili donanımın ilk kullanılabilir olduğu zamanda başlatılabilir.

- __Bağımlılık Eklenmesi:__  
Unit'ler için bağımlılıklar __systemd__ tarafında oluşturulabilir. Servis için bağımlılıklar eklenebilir. __systemd__ tarafından belirtilen bağımlılıklar başlatılır.

- __Kolay Güvenlik:__  
Basit talimatlar ile Unit'ler için güvenlik özellikleri eklenebilir. Örneğin, dosya sisteminin bir bölümüne salt okunabilir erişim tanımlama veya sistem yetkilerini sınırlama gibi.

<a name="where-is-unit-files"></a>
## Systemd Unit Dosyaları Nerede Bulunur?
Systemd Unit dosyaları, her biri farklı önceliklere ve etkilere sahip bir çok konumda saklanırlar.
System Unit dosyalarının kopyası genellikle `/lib/systemd/system` dizininde saklanır. Yazılım Unit dosyalarını sisteme kurduğunda, bu varsayılan olarak yerleştirildiği yerdir. Burada depolanan Unit dosyaları, oturum sırasında isteğe bağlı olarak başlatılabilir veya durdurulabilir.  
Unit dosyalarının yerini değiştirmek istendiğinde, en uygun yer `/etc/systemd/system` dizinidir. Bu dizinde bulunan Unit dosyaları, diğer dizinlerden daha önceliklidir. Bir Unit dosyasının sistem kopyası değiştirilmek isteniyor ise, bunu yapmanın güvenli yolu bu dizinde yapmaktır. Unit dosyasının adında sonuna ".d" eklenmiş bir dizin oluşturarak bu dizin içine bir _conf_ dosyası oluşturarak Unit dosyasının özelliklerini geçersiz kılmak veya genişletmek için kullanılır.   
`/run/systemd/system` dizininde çalışma zamanı Unit tanımlamaları için de bir yer vardır. Bu dizinde bulunan Unit dosyaları, diğer dizindekilerden daha fazla önceliğe sahiptir. systemd, çalışma zamanında dinamik olarak oluşturulan Unit dosyaları için bu konumu kullanır. Bu dizin, oturum boyunca sistemin Unit davranışını değiştirmek için kullanılabilir. Sistem yeniden başlatılınca, bu dizindeki değişiklikler kaybolur.

<a name="types-of-units"></a>
## Unit Dosya Türleri
__systemd__, tanımladıkları kaynağın türüne göre Unit dosyalarını sınıflandırır. Unit türü dosyanın sonuna eklenen sonek ile belirlenir.

- __.service__  
Bir servis Unit dosyası, bir servisin sunucuda nasıl yönetileceğini belirler. Bu, servisin başlatılacağı veya durdurulacağı, hangi koşullarda otomatik olarak başlatılması gerektiği ve ilgili yazılım için bağımlılık bilgilerini içerir.

- __.socket__   
Bir soket Unit dosyası, bir ağ veya IPC soketi yada systemd'nin soket tabanlı etkinleştirme için kullandığı bir FIFO arabelleiğini tanımlar. Bu dosyaların, her zaman bu birim tarafından tanımlanan soket üzerinde etkinlik görüldüğünde başlatılacak ilişkili bir __.service__ dosyası bulunur.

- __.device__  
__udev__ veya __sysfs__ dosya sistemi tarafından systemd yönetimine ihtiyaç duyulan bir aygıtı tanımlayan bir birim. Tüm cihazların _.device_ dosyaları olmayabilir. Cihaz birimlerinin gerekli olabileceği bazı senaryolar, cihazların bağlanması veya erişilmesi olabilir.

- __.mount__  
Bu birim systemd tarafından yönetilebilecek bir bağlama noktası sağlar. 

- __.automount__  
Bu Unit türü, otomatik olarak bağlanacak bir bağlama noktasını yapılandırmayı sağlar. Bu dosyaların adı belirttikleri bağlama noktasında sonra verilmelidir ve özelliklerini tanımlayacağı bir _.mount_ dosyası olmalıdır.

- __.target__  
Durumlar başlatırken veya değişitirirken diğer birimler için senkronizasyon noktaları sağlamak için kullanılır. Ayrıca sistemi yeni bir hale getirmek için kullanılabilir.

- __.path__  
Yol bazlı etkinleştirme için kullanılabilecek bir yolu tanımlar. Varsayılan olarak, yol belirtilen duruma geldiğinde, aynı taban adının bir _.service_ uzantılı Unit'i başlatılacaktır. 

- __.timer__  
Bir timer, systemd tarafından yönetilecek, gecikmeli veya zamanlanmış etkinleştirme için bir cron benzeri zamanlayıcı sağlar. Zamanlayıcı süresi dolduğundan aynı isimde bir Unit başlatılacaktır.

- __.snapshot__  
_.snapshot_ Unit dosyası, `systemctl` snapshot komutuyla otomatik olarak oluşturulur. Değişiklik yapıldıktan sonra sistemin mevcut durumunu yeniden oluşturmanıza olanak tanır. 

- __.scope__   
Bus arayüzlerinden alınan bilgilerden systemd tarafından otomatik olarak oluşturulur. Bunlar, harici olarak oluşturulan sistem süreç setlerini yönetmek için kullanılır.

<a name="unit-section-directives"><a/>
## [Unit] Bölümü Direktifleri
Unit dosyalarının çoğunda bulunan ilk bölüm [Unit] bölümüdür. Bu genellikle Unit için tanımlayıcı verileri ve birimin diğer birimlerle ilişkisini yapılandırmak için kullanılır. Dosyanının okunması sırasında, bu bölüm Unit ile ilgili genel bir bakış sunduğu için genelde ilk sırada yer alır. Bu bölümde bulunan bazı önemli direktifler:   

- __Description__   
Bu direktif, Unit dosyasının adının ve temel işlevini belirtir. Kısa ve açıklayıcı olması daha doğrudur.

- __Documentation__   
Dökümantasyon için bir URI listesi eklenebilir. man sayfaları veya web üzerinden bir URL olabilir. `systemctl status servicename` komutu ile görüntülenir.

- __Requires__   
Unit dosyasının bağımlı olduğu Unit'leri listeler. Geçerli Unit etkinleştirilirse, burada listelenen Unit'lerd de başarılı bir şekilde etkinleştirilmelidir. Bu Unit'ler geçerli Unit ile paralel olarak başlatılır. 

- __Wants__   
__Requires__ ile benzerdir ancak daha az önceliği vardır. Systemd, bu Unit etkinleştirildiğinde, burada belirtilen Unit'leri etkinleştirmeye çalışır. Buradaki Unit'ler başlatılamaz ise geçerli Unit çalışmaya devam eder. Önerilen yol bunu kullanmaktır.

- __BindsTo__   
__Requires__ ile benzerdir ancak ilgili Unit sonlandığında geçerli birimin durmasına sebep olur.

- __Before__  
Burada belirtilen Unit'ler, aynı anda etkinleştirildiyse, geçerli Unit başlatılmadan başlatılmazlar. Bu, bir bağımlılık ilişkisi (Requires) anlamına gelmez.

- __After__   
Burada belirtilen Unit'ler geçerli Unit başlatılmadan önce başlatılırlar.

