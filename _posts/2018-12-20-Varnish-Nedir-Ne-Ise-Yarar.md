---
layout: post
title: "Varnish Nedir Ne işe Yarar?"
author: "Oğuzhan İNAN"
date: 2018-12-20
categories: linux varnish
---

![What is varnish]({{ "/assets/img/varnish-reverse-proxy.png" | absolute_url }})

Varnish web sitelerini hızlandırmak için kullanılan bir HTTP ters vekil (reverse proxy) dir. Ters proxy istemcilere asıl sunucu gibi görünen bir ara sunucudur. Varnish web sayfalarını önbellekde (ram veya dosya) tutarak istemci istek yaptığında eğer cevap önceden istenmişse aynı cevabı dönerek asıl sunucuya gitmeden kullanıcıya cevap verir. Bu sayede veritabanı işlemleri gibi sunucuyu yoran durumlar azalmış olur.  

Klasik bir wordpress sitesinde açılan her sayfa için birden fazla veritabanı sorgusu yapılır. Her veritabanı sorgusu ayrı bir process (süreç) demek olduğu için sunucu aşırı istek karşısında yavaşlar ve sonunda cevap veremez duruma gelir. Bu tür durumları engellemek için Varnish kullanılabilir. Örneğin bir blog yazısını görüntülemek isteyen kullanıcı için varnish isteği uygulama sunucusuna iletir ve dönen cevabı belirtilen süre (varsayılan 120 saniye) önbellekte tutar. İstek kullanıcıya verildikten sonra aynı veya başka bir kullanıcı aynı sayfayı istediği zaman önbellekte bulunan içerik iletilir.

Varnish sadece cache için değil başka amaçlar içinde kullanılabilir.
- Web Application Firewall (Web Güvenlik Duvarı)
- DDoS Attack Defender (DDoS Saldırı Koruma)
- Load Balancer (Yük Dengeleyici)
- HTTP Router   
.....

### VCL (Varnish Configuration Language)
Varnish, VCL olarak adlandırılan konfigürasyon dilini kullanır. Varnish, bu dil sayesinde çok esnek bir şekilde kullanılabilir. VCL'i kullanarak Varnish'i kendinize göre özelleştirebilirsiniz. Bu konfigürasyon dosyası C koduna çevrilir ve derlenerek binary dosyasına dönüşür. Bu sayede Varnish çok daha hızlı çalışır. Ayrıca VCL dosyası içinde C kodu direk olarak da kullanılabilir.

VCL'in yetmediği durumlarda Varnish için modül yazılabilir. C ile kendi Varnish modülünüzü yazabilirsiniz.