---
layout: post
title: "Java Jackson Annotations (Ek Aciklamalar)"
author: "Oğuzhan İNAN"
date: 2018-05-11
categories: java jackson
---

Java için JSON kütüphanesi olan __Jackson__'ın annotationları hakkında genel bilgi. 

## Jackson Serialization (Serileştirme) Annotations   

### @JsonAnyGetter   
Java'da bulunan _Map_ veri tipini JSON'a dönüştürmek için kullanılır.

```java
public class Model {
    public Integer id;
    private Map<String, String> settings;

    @JsonAnyGetter
    public Map<String, String> getSettings() {
        return settings;
    }
}
```
Çıktı:   
```json
{
    "id": 2,
    "key1": "value1",
    "key2": "value2"
}
```

### @JsonPropertyOrder  
Json çıktısının anahtarlarını sıralamak için kullanılır.

```java
@JsonPropertyOrder({"name", "id", "age"})
public class Model {
    public Integer id;
    public String name;
    public Integer age;
}
```
Çıktı:
```json
{
    "name": "Jack",
    "id": 2,
    "age": 23
}
```

### @JsonRawValue  
Verilen String değeri JSON olarak kullanmak için kullanılır.

```java
public class Model {
    public int id;

    @JsonRawValue
    public String json;

    Model(int id, String json) {
        this.id = id;
        this.json = json;
    }
}

Model model = new Model(1, "{\"age\": 22}");
```
Çıktı:
```json
{
    "id": 1,
    "json": {
        "age": 22
    }
}
```

### @JsonValue   
Tüm örneği serileştirmek için kullanılacak tek bir yöntem belirtmek için kullanılır.

```java
public class Model {
    private int id;
    private String name;

    @JsonValue
    public String toJson() {
        return name + " - " + id;
    }
}
```
Çıktı:
```json
"Jack - 1"
```

### @JsonRootName   
JSON çıktısına ana anahtar vermek için kullanılır.

```java
@JsonRootName(value = "customer")
public class Model {
    private int id;
    private String name;
}
```
Çıktı:
```json
{
    "customer": {
        "id": 1,
        "name": "Jack"
    }
}
```

## Jackson Deserialization Annotations   

### @JsonCreator   
Okumak istediğimiz JSON değeri modelimizdeki anahtarlardan farklı bir anahtar içeriyorsa, modelin kurucusu aracılığıyla okuduğumuz JSON'un doğru şekilde alınmasını sağlar.

Okumak istediğimiz JSON:
```json
{
    "id": 1,
    "theName": "Jack"
}
```

__theName__ alanı modelimizde __name__ olarak geçtiği için okuma gerçekleşmez. Bunu düzeltmek için: 
```java
public class Model {
    public int id;
    public String name;
    
    @JsonCreator
    public Model(@JsonProperty("id") int id, @JsonProperty("theName") String name) {
        this.id = id;
        this.name = name;
    }
}
```

### @JsonAnySetter   
@JsonAnyGetter açıklamasının yaptığı işin tersini yapar. Okunan JSON'daki modelde bulunan anahatarları doğru şekilde atadıktan sonra, kalan anahtarları ve değerleri bir __Map__ nesnesine atar.

```java
public class Model {
    public int id;
    private Map<String, String> settings;

    @JsonAnySetter
    public void add(String key, String value) {
        settings.put(key, value);
    }
}
```
Okunan JSON:
```json
{
    "id": 1,
    "key1": "value1",
    "key2": "value2"
}
```
`id` haricindeki anahtarlar ve değerler `settings`'e atanır.

### @JsonIgnoreProperties   
JSON verisinde gözükmesi istenmeyen anahtarlar belirtilir.

```java 
@JsonIgnoreProperties({"id"})
public class Model {
    public int id;
    public String name;
}
```
Çıktı: 
```json
{
    "name": "Jack"
}
```

### @JsonIgnore   
JSON verisinde gözükmesi istenmeyen anahtarlar özel olarak belirtmek istenirse kullanılır.

```java 
public class Model {
    @JsonIgnore
    public int id;

    public String name;
}
```
Çıktı: 
```json
{
    "name": "Jack"
}
```

### @JsonInclude  
JSON verisinde hangi tür verilerin bulunacağını belirtmek için kullanılır.

```java
@JsonInclude(Include.NON_NULL)
public class Model {
    public int id;
    public String name;
}
Model model = new Model(1, null);
```
Çıktı:
```json
{
    "id": 1
}
```
`name` anahtarı null değere sahip olduğu için çıktıda gözükmeyecek. Yukarıda sadece NON_NULL yani null olmayan değerler gözükmesini istedik.
