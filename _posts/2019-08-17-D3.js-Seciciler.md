---
layout: post
title: "D3.js Seçiciler (Selections)"
author: "Oğuzhan İNAN"
date: 2019-08-17
categories: js chart d3js selections 
---

D3.js ile grafik oluşturmanın ilk aşaması element seçimi yapmak. Elinizdeki verileri sayfada göstermek için hangi elementleri kullanacağınızı seçdikten sonra tasarımlarını ve göstereceğiniz verileri eklersiniz.
D3.js'in sağladığı seçici fonksiyonları kullanabilmek için CSS seçicilerini doğru bilmek önemli.      

Seçilen elementler sayfada mevcutsa yapılan işlemler onların üstünde etkili olur. Fakat seçtiğiniz elementler sayfada yoksa belirttiğiniz şekilde elinizdeki veri sayısına bağlı olarak oluşturulur.    

Örneğin bir isim listeniz var ve listede 5 adet isim var. `body` içerisinde ise 3 adet `span` elementi var. `span` elementlerini seçip bu verileri eklediğinizde ilk 3 veri sayfadaki `span`'lara yazılır. Diğer veriler için yeni elementler oluşturulur. Önceki elementlerin başka birşey için kullanılmadığından emin olmalısınız.    

Örnek:
```html
<html>
  <body>
    <script src="http://d3js.org/d3.v4.min.js" charset="utf-8"></script>
    <script>
      var nums = [10, 30, 50, 20, 80, 90]; 
      var svg = d3.select('body').append('svg');
    </script>
  </body>
</html> 
```
Yukarıda `nums` adında bir veri listemiz var ve adı **svg** olan yerel bir değişken oluşturduk. Bu kullanım `d3.select('svg')` ifadesi ile eşdeğerdir. SVG öğesi diğer tüm öğeleri içereceğinden, genişliğini ve yüksekliğini vb. ayarlayabiliriz. `<rect>` elementine ihtiyacımız var. Veri listesinde altı değer var, bu yüzden **svg** öğesini eklediğimiz şekilde ekleyebilirz. Bu şeikilde, `d3.append('rect')` kodunı altı defa yazmamız gerekir.   
Böyle yapmak kodu uzatacak ve anlamsız olacak. Seçimler bunu doğru bir şekilde yapmamızı sağlar: 
```js
var bars = d3.selectAll('rect'); 
```
Seçimler hem önceden var olan öğelerde çalışmanıza hem de ihtiyacınız olan öğelerin "temsilini" kullanmanıza olanak tanır. Seçimleri anlama D3 ile çalışmak için kritik bir giriş noktasıdır. Seçimler sayesinde, D3'ten, dizileri, seçili öğelere uygulamanızı sağlayacak farklı yöntemleri kullanabilirsiniz.
```js
var nums = [10, 30, 50, 20, 80, 90]; 
var bars = svg.selectAll('rect').data(nums);
```
Yukarıdaki örnekde seçilen `rect` elementlerine elimizdeki verileri ekledik fakat `body` içerisinde rect olmadığı için bir çıktı alamayız. Bu durumda `.enter()` fonksiyonundan sonra element olmadığında ne yapması gerektiğini yazmamız gerekli.   

```js
var bars = svg.selectAll('rect').data(nums)
  .enter()
  .append('rect');
```

[https://www.manning.com/books/d3js-in-action-second-edition](https://www.manning.com/books/d3js-in-action-second-edition)
[https://www.apress.com/gp/book/9781484219270](https://www.apress.com/gp/book/9781484219270)