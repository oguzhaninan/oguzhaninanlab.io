---
layout: post
title: "Web Önbellekleme (Cache): Terminoloji, HTTP Headers"
author: "Oğuzhan İNAN"
date: 2018-12-24
categories: linux cache web http-headers
---

### Önbellekleme Nedir?
Önbelleğe alma, daha sonraki istekleri daha hızlı yapmak için dönen yanıtların saklanmasıdır. Birbirinden farklı birden çok önbellekleme türü vardır.   

Web önbellekleme kullanıcıya hızlı veri aktarımı yapabilme ve ağ trafiğini en aza indirmek için yapılır.

### Terminoloji
- __Kaynak Sunucu (Origin Server):__   
İçeriğin orjinal konumudur. Web uygulamasını bulunduğu sunucudur. Önbellekten sunulamayan içeriğin alındığı yerdir.

- __Önbellek Tutturma Oranı (Cache Hit Ratio):__   
Önbellekten alınabilen isteklerin toplam istek sayısına oranıdır. Ne kadar yüksek ise okadar doğru önbellekleme yapıldığını gösterir.

- __Tezelik  (Freshness):__    
Önbellekteki bir öğenin istemciye sunulup sunulamayacağını belirlemek için kullanılır. Belirtilen süreyi geçmediği sürece içerikler taze sayılır.

- __Eski İçerik (Stale Content):__   
Öğelerin saklanması için belirtilen süre dolmuş ise eski sayılır ve istemciye eski içerik sunulmaz. Eski içerikler tekrar kaynak sunucudan istenir. Yeni öğeler tekrar belirtilen süre boyunca geçerli olacak şekilde önbellekte saklanır.

### Web İçeriğinin Önbelleğe Alındığı Yerler
Web içerikleri birden çok noktada önbelleğe alınabilirler.

- __Tarayıcı Önbelleği (Browser Cache):__    
Web tarayıcıları her web sitesi için belli durumlarda önbellekleme yaparlar. Tekrar indirilmesi uzun süren içerikler veya sık sık istenmesi muhtemel içerikleri saklarlar.

- __Aracı Önbellekleme (Intermediary caching proxies):__   
İstemci ile kaynak sunucu arasındaki herhangi bir sunucu istenilen içierikleri önbelleğe alabilir. Böylece kaynak sunucuya gitmeden içeriğe ulaşılabilir.

- __Ters Önbellek (Reverse Cache):__   
Sunucu altyapısı kendi önbelleğini uygulayabilir. Bu şekilde her istek için veritabanı gibi kaynaklara başvurmadan önceden saklı tutulan içerik istemciye sunulur.

### HTTP Cache Heraders  

- __Expires:__   
İçeriğin süresinin ne zaman biteceğini belirtir. Süre bittikten sonra içerik için kaynak sunucuya gidilir.

- __Cache-Control:__    
_Expires_ başlığının daha gelişmiş ve modern halidir. İkisini birlikte kullanmak sorun çıkarmayabilir. Fakat genel olarak bu başlık tercih edilir.

- __Etag (Entity Tag):__   
Önbellek doğrulaması için kullanılır. Etag ile içeriğe özel benzersiz bir değer atanır. İçeriğin kullanım süresi dolduğunda eldeki içeriği doğrulamak istediğinde, içerik için sahip olduğu Etag değerini gönderebilir. Kaynak önbelleğin aynı olduğunu ya da güncel içeriği yeni Etag ile birlikte geri gönderir.

- __Last-Modified:__   
Öğenin en son değiştirildiği zamanı belirtirir. Bu, içeriği taze tutmak için kullanılır.

- __Content-Length:__   
Önbelleğe alma özelliğine dahil olmasa da, Önbellek ilkelerini tanımlarken İçerik Uzunluğu başlığı önemlidir. Bazı yazılımlar, önceden yer ayırması gereken içeriğin boyutunu önceden bilmiyorsa, içeriği önbelleğe almayı reddedecektir.

- __Vary:__   
Önbellek genellikle istenen ana bilgisayarı ve kaynağın yolunu önbellek öğesinin depolanacağı anahtar olarak kullanır. Vary üstbilgisi, bir isteğin aynı öğe için olup olmadığına karar verirken önbelleklere ek bir üstbilgiye dikkat etmelerini bildirmek için kullanılabilir. Bu, en çok _Accept-Encoding_ başlığı tarafından anahtarlanacak önbellekleri belirtmek için kullanılır, böylece önbellek sıkıştırılmış ve sıkıştırılmamış içerik arasında ayrım yapabilir.

### Cache-Control Header'ının Alabileceği Değerler  

- __no-cache__  
Bir istemciye sunulmadan önce her istek için önbelleğe alınmış içeriğin yeniden doğrulanması gerektiğini belirtir. Bu, aslında, içeriği hemen eski olarak işaretler, ancak tüm öğenin yeniden indirilmesini önlemek için yeniden doğrulama tekniklerini kullanmasına izin verir.

- __no-store__   
İçeriğin hiçbir şekilde önbelleğe alnmaması gerektiğini gösterir. Hassas veriler için kullanılabilir.

- __public__  
Tarayıcı ve herhangi bir önbellek tarafından önbelleğe alınabileceği anlamına gelir. 

- __private__   
Kullanıcı tarayıcısı tarafından saklanabilir fakat başka hiçbir ara önbellek tarafından saklanmamalıdır. Kullanıcıya özel bilgiler için kullanılabilir.

- __max-age__   
Bu ayar, içeriğin orijinal sunucudan yeniden doğrulanması veya yeniden indirilmesi gerekmeden önce içeriğin önbelleğe alınabileceği maksimum yaşı yapılandırır. Temelde bu Expires, başlığının yerini alır ve içeriğin tazeliğini belirlemek için kullanılır. Bu seçenek, bir yıllık maksimum geçerli tazelik süresiyle (31536000 saniye) saniye cinsinden değerini alır.

- __s-maxage__   
Bu, max-age ile aynı işlevi görür. Farkı ise, bu seçeneğin yalnızca aracı önbelleklere uygulanmasıdır.

- __must-revalidate__   
Bu, `max-age`, `s-max-age` veya `expires` başlığında belirtilen tazelik bilgilerine kesinlikle uyulması gerektiğini belirtir. Eski içerik hiçbir koşulda sunulamaz. Bu, önbelleğe alınmış içeriğin ağ kesintileri ve benzeri senaryolar durumunda kullanılmasını önler.

- __proxy-revalidate__   
Bu, yukarıdaki ayar ile aynı şekilde çalışır, ancak yalnızca aracı proxy'ler için geçerlidir. Bu durumda, kullanıcının tarayıcısı, bir ağ kesintisi durumunda eski içerik sunmak için potansiyel olarak kullanılabilir, ancak ara önbellekler bu amaç için kullanılamaz.

- __no-transform__   
Bu seçenek, önbelleklere, herhangi bir koşul altında performans nedeniyle alınan içeriği değiştirmelerine izin verilmediğini söyler.

[Kaynak](https://www.digitalocean.com/community/tutorials/web-caching-basics-terminology-http-headers-and-caching-strategies)