---
layout: post
title:  "Java TestNG Nedir ?"
author: "Oğuzhan İNAN"
date: 2018-01-28
categories: java
---

## Test Nedir?
Test, bir uygulamanının gereksinimlere uygun şekilde çalışıp çalışmadığından emin olmak için yapılan kontrol sürecidir. Tek bir test bir sınıf, metot, vs test edilmesini sağlayacak şekilde hazırlanır. 

### TestNG (Next Generation)
TestNG, JUnit ve NUnit test frameworklerinden etkilenerek [Cedric Beust](https://twitter.com/cbeust) tarafından geliştirilen açık kaynak bir test frameworküdür. Ancak eksikleri kapatmış ve kullanımı daha kolay hale getiren bazı özellikleri vardır.
Eski çerçevelerdeki kısıtlamaları ortadan kaldırarak daha esnek ve kolay test yazmaya imkan verir.

TestNG tüm test kategorilerini kapsayacak şekilde tasarlanmıştır. Kullanım için JDK 5 ve üst versiyonları gerektirir.

#### TestNG Özellikleri
- Annotation (ek açıklama) desteği.
- Daha fazla nesne yönelimli geliştirme.
- Entegre sınıfları test etmeyi destekler.
- Esnek çalışma zamanı yapılandırması sağlar.
- Bağımlılık testi desteği.
- Multi thread (Çoklu Akış) test desteği.

## Başlangıç için bir örnek

Testleri çalıştırmak için IDE'lerin eklentileri kullanılabilir. 
Komut satırından maven aracını kullanarak da çalıştırabiliriz.

TestNG ile ilk örneğimiz için __Makale__ adında bir POJO oluşturduk.
```java
public class Makale {
    private String baslik;
    private String icerik;

    public String getBaslik() {
        return this.baslik;
    }
    public void setBaslik(String baslik) {
        this.baslik = baslik;
    }

    public String getIcerik() {
		return this.icerik;
    }
    public void setIcerik(String icerik) {
    	  this.icerik = icerik;
    }
}
```

Şimdi __Makale__ sinifimizin _set_ ve _get_ metodunu test ediyoruz.
```java
import org.testng.Assert;
import org.testng.annotations.Test;

public class MakaleTest {
    Makale makale = new Makale();

    @Test
    public void makaleTest() {
        makale.setBaslik("Bugün okullar tatil");
        makale.setIcerik("Kar yağışından dolayı okullar tatil edildi.");

        Assert.assertEquals(makale.getBaslik(), "Bugün okullar tatil");
        Assert.assertEquals(makale.getIcerik(), "Kar yağışından dolayı okullar tatil edildi.");
    }
}
```

Şimdi __Makale__ sınıfı için __MakaleIslem__ adında bir sınıf oluşturalım.
Bu sınıf makalenin içeriğindeki kelime sayısını versin.

```java
public class MakaleIslem {

    public int kelimeSayisi(Makale makale) {
        return makale.getIcerik().split(" ").length;
    }
		
}
```

Şimdi de __MakaleIslem__ sınıfının doğru çalışıp çalışmadığını test edelim.

```java
import org.testng.Assert;
import org.testng.annotations.Test;

public class MakaleIslemTest {
    Makale makale = new Makale();
    MakaleIslem makaleIslem = new MakaleIslem();

    @Test
    public void kelimeSayisiTest() {
        makale.setBaslik("Bugün okullar tatil");
        makale.setIcerik("Kar yağışından dolayı okullar tatil edildi.");
		
        int kelimeSayisi = makaleIslem.kelimeSayisi(makale);
        Assert.assertEquals(kelimeSayisi, 6);
    }
}
```