---
layout: post
title: "HDFS (Hadoop Distributed File System) Nedir?"
author: "Oğuzhan İNAN"
date: 2018-02-09
categories: bigdata hadoop
---
Bazı kullanım örnekleri: 
- 2010 yılında Facebook, 21 Petabayt (1 PB = 1024 TB) veri HDFS kümesinden birine sahip olduğunu açıkladı.
- 2012'de Facebook, 100 PB'den fazla veri içeren en büyük tek HDFS kümesine sahip olduklarını açıkladı.
- Yahoo! __40.000 sunucu__ üzerinde çalışan __100.000 CPU'ya__ sahip __4500 düğümlü__ Hadoop kümesine sahip olduğunu açıkladı.

Hadoop'un iki bölümü vardır: __Storage__ (Depolama) ve __Processing__ (İşleme). Depolama'dan kastımız HDFS'dir. 
Dağıtılmış Dosya Sistemi, birden fazla bilgisayar veya sunucu üzerinde veri depolama anlamına gelir. Birden fazla sunucuyu tek bir sunucu gibi kullanmanıza olanak sağlar. Tek bir sunucu üzerinde çalışıyormuş gibi düşünülebilir. 

HDFS'nin Avantajları
* __Dağıtık Depolama Alanı__ 
![Dağıtık Depolama]({{ "/assets/img/hdfs-dagitik-depolama.png" | absolute_url }})
10 sunucudan her biri 1 TB disk kapasitesine sahip olduğunda bu sunuculara Hadoop'u kurarsanız 10 TB kapasiteli tek bir sunucu kullanıyormuş gibi dosyalarınıza erişebilirsiniz. Bu şekilde yatay ölçekleme gerçekleştirmiş olursunuz.

* __Dağıtık ve Paralel Hesaplama__
![Dağıtık ve Paralel Hesaplama]({{ "/assets/img/hdfs-dagitik-ve-paralel-hesaplama.png" | absolute_url}})
Veriler makinelerde bölünmüş olduğundan, __Dağıtılmış ve Paralel Hesaplamadan__ yararlanmamızı sağlar. Tek bir makinede 1 TB dosyanın işlenmesi 43 dakika sürdüğünü varsayalım. Öyleyse aynı özelliğe sahip 10 makinede 1 TB dosya işlemek istediğimizde 4,3 dakika sürecek. Düğümlerin her biri paralel olarak 1 TB dosyanın bir kısmı ile çalışıyor olacak.

* __Yatay Ölçeklenebilirlik__ 
![Yatay ve Dikey Ölçeklenebilirlik]({{ "/assets/img/hdfs-yatay-ve-dikey-olcekleme.png" | absolute_url }})
İki tür ölçeklendirme vardır: __Dikey__ ve __Yatay__.  
Dikey ölçeklendirme yapmak istediğimizde sistemin donanım kapasitesini artırırız. Daha fazla RAM veya CPU ekleyerek yapılır. Fakat her makinenin belli bir sınırı vardır. Ve dikey ölçeklendirme yapmak istediğinizde makineniz çalışırken bunu yapamayacağınız için makinenin durdurulup, kapasitesinin artırılıp tekrar başlatılması gerekir.   
Yatay ölçeklendirme yapmak için yeni bir makine alınarak ağa bağlanır. Herhangi bir makineyi durdurmanıza gerek yoktur. Belli bir sınırınız yoktur. 

