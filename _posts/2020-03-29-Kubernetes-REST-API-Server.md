---
layout: post
title: "Kubernetes REST API Yapısı"
author: "Oğuzhan İNAN"
date: 2020-03-29
categories: docker kubernetes network api
---

## Kubernetes REST API Server Giriş

Temel olarak, Kubernetes farklı rollere sahip bir grup düğümden oluşur. Ana düğümlerdeki kontrol mekanizması API Sunucusu, Controller Manager ve Scheduler dan oluşur. API Sunucusu merkezi yönetim mekanizmasıdır ve dağıtılmış depolama bileşeni vb. ile doğrudan görüşen tek bileşendir. Aşağıdaki temel işlevleri sağlar: 
- Küme dahili olarak çalışan düğümler tarafından ve harici olarak kubectl tarafından kullanılan Kubernetes API'sine hizmet eder 
- Kubernetes UI küme bileşenlerine proxy sağlar
- Nesnelerin durumunun değiştirilmesine izin verir, örneğin pod'lar ve service'ler
- Dağıtılmış bir depolama birimindeki ([etcd](https://etcd.io/)) nesnelere müdahele eder.

![k8s-server]({{ "/assets/img/k8s-apiserver.png" | absolute_url }})

## API Versiyonları  
 
Kubernetes API, bir HTTP REST API'sıdır, ancak aynı zamanda küme iç iletişimi için [Protocol Buffers](https://developers.google.com/protocol-buffers) da destekler. Genişletilebilirlik nedenleriyle Kubernetes, `/api/v1` veya `/apis/extensions/v1beta1` gibi farklı API yollarında birden fazla API sürümünü destekler. Farklı API sürümleri farklı düzeylerde kararlılık ve destek anlamına gelir:

- **Alfa seviyesi**   
    - Sürüm adları `alfa` (ör. `v1alpha1`) içerir.
    - Bug olabilir. Özelliğin etkinleştirilmesi hataları ortaya çıkarabilir. Varsayılan olarak devre dışıdır.
    - Özellik desteği, önceden bildirilmeksizin herhangi bir zamanda bırakılabilir.
    - API, sonraki bir yazılım sürümünde önceden bildirimde bulunmaksızın uyumsuz şekillerde değişebilir.
    - Artan hata riski ve uzun süreli destek eksikliği nedeniyle sadece kısa ömürlü test kümelerinde kullanılması önerilir.
- **Beta seviyesi**    
    - Sürüm adları `beta` (ör. `v2beta3`) içerir.
    - Kod iyi test edilmiştir. Özelliğin etkinleştirilmesi güvenli kabul edilir. Varsayılan olarak etkindir.
    - Ayrıntılar değişse de, genel özellik için destek bırakılmaz.
    - Nesnelerin şeması ve/veya semantiği, sonraki bir beta veya kararlı sürümde uyumsuz yollarla değişebilir. Bu olduğunda, bir sonraki sürüme geçmek için talimatlar verilir. Bu, API nesnelerini silmeyi, düzenlemeyi ve yeniden oluşturmayı gerektirebilir. Bu, özelliğe dayanan uygulamalar için aksama süresine neden olabilir.
    - Sonraki sürümlerde uyumsuz değişiklikler potansiyeli nedeniyle yalnızca iş açısından kritik olmayan kullanımlar için önerilir.
- **Stable seviyesi**     
    - Sürüm adı `vX`'dir; burada `X` bir tamsayıdır.
    - Özelliklerin kararlı sürümleri, sonraki birçok sürüm için yayınlanan yazılımda görünecektir.

API Swagger dosyasına [burdan](https://raw.githubusercontent.com/kubernetes/kubernetes/release-1.17/api/openapi-spec/swagger.json) ulaşılabilir.    
Aşağıdaki adres kullanılarak yukarıdaki linkde bulunan JSON dosyası ile tüm API incelenebilir.    
https://entrystore.org/api/  

## API Grupları
![k8s-server]({{ "/assets/img/k8s-api-groups.png" | absolute_url }})

Kubernetes API'sını genişletmeyi kolaylaştırmak için [API grupları](https://git.k8s.io/community/contributors/design-proposals/api-machinery/api-group.md) kullanılır. API grubu bir REST yolunda ve serileştirilmiş bir nesnenin `apiVersion` alanında belirtilir.

Şu anda kullanımda olan birkaç API grubu vardır:
    1. Genellikle `legacy group` olarak adlandırılan `core group`, REST yolu `/api/v1`'de bulunur ve `apiVersion: v1` kullanır.
    2. Adı belirtilen gruplar REST yolunda `/apis/$GROUP_NAME/$VERSION` adresindedir ve `apiVersion: $GROUP_NAME/$VERSION` (ör. `ApiVersion: batch/v1`) kullanın. Desteklenen API gruplarının tam listesi (Kubernetes API Reference)[https://kubernetes.io/docs/reference/] adresinde görülebilir.

API'yı özel kaynaklarla genişletmenin desteklenen iki yolu vardır:

- [CustomResourceDefinition](https://kubernetes.io/docs/tasks/access-kubernetes-api/extend-api-custom-resource-definitions/), çok temel CRUD ihtiyaçları olan kullanıcılar içindir.
- Kubernetes API semantiğinin tam desteğine ihtiyaç duyan kullanıcılar kendi apiserver'lerini uygulayabilir ve sorunsuz hale getirmek için [aggregator](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/api-machinery/aggregated-api-servers.md) kullanabilir.

## API Gruplarını Etkinleştirme veya Devre Dışı Bırakma    
Belirli kaynaklar ve API grupları varsayılan olarak etkindir. Apiserver'da `--runtime-config` ayarını yapıp etkinleştirebilirsiniz. `--runtime-config` virgülle ayrılmış değerleri kabul eder. Örneğin:

- `batch/v1` devre dışı bırakmak için, `--runtime-config=batch/v1=false` ayarlanabilir.
- `batch/v2alpha1` aktifleştirmek , `--runtime-config=batch/v2alpha1` ayarlanabilir.

----

[Kaynak 1](https://kubernetes.io/docs/concepts/overview/kubernetes-api/) - 
[Kaynak 2](https://git.k8s.io/community/contributors/design-proposals/api-machinery/api-group.md) - 
[Kaynak 3](https://developers.google.com/protocol-buffers) - 
[Kaynak 4](https://blog.openshift.com/kubernetes-deep-dive-api-server-part-1/) - 
[Kaynak 5](https://kubernetes.io/docs/reference/) - 
[Kaynak 6](https://kubernetes.io/docs/concepts/api-extension/custom-resources/) - 
[Kaynak 7](https://pt.slideshare.net/sttts/kubernetes-api-deep-dive-into-the-kubeapiserver/8)
