---
layout: post
title: "Kubernetes Ingress Controllers"
author: "Oğuzhan İNAN"
date: 2019-10-15
categories: docker kubernetes ingress
---

![]({{ "/assets/img/ingress-controllers.svg" | absolute_url }})

Ingress, Kubernetes'de çalıştırdığınız uygulamanın kullanıcılar tarafından erişilmesine izin vererek dış trafiği Kubernetes servislerine bağlar. Bazı diğer bileşenlerin, nesnelerin veya hizmetlerin aksine, ingress controller bir küme başlatıldığında otomatik olarak başlamaz. Doğru ingress controller seçmek için, Kubernetes kümenize gelen trafiği ve yükü göz önünde bulundurarak karar verilmelidir. 

### [Kubernetes Ingress Controller](https://github.com/kubernetes/ingress-nginx)

Resmi Kubernetes denetleyicisi. Topluluk tarafından geliştiriliyor. Nginx'e dayanıyor ve ekstra özellikler uygulamak için kullanılan bir dizi Lua eklentisi ile destekleniyor. NGINX'in popülaritesi ve denetleyici olarak kullanıldığında minimum değişiklik yapması sayesinde K8'le çalışan ortalama bir mühendis için en basit seçenek olabilir.

### [Kong Ingress](github.com/Kong/kubernetes-ingress-controller)

![Kong Ingress]({{ "/assets/img/kong.png" | absolute_url }})

Kong Ingress, [Kong Inc](https://konghq.com/about-kong-inc/) tarafından geliştirilmektedir ve iki versiyonu vardır: ticari ve ücretsiz. Kong Ingress, yeteneklerini artıran Lua modüllerinin eklenmesiyle NGINX üzerine inşa edilmiştir. Başlangıçta, bir API ağ geçidi görevi gören API isteklerinin işlenmesi ve yönlendirilmesine odaklanmıştı. Ancak, şu an itibariyle, tam teşekküllü bir ingress controller oldu. Başlıca avantajı, kurulumu ve yapılandırması kolay olan çok sayıda ek modül / eklentidir (üçüncü taraf geliştiricilerinkiler de dahil). Çok çeşitli ek özelliklere imkan sağlar.
    
Bir API ağ geçidi (API Gateway) istiyorsanız bu kullanılacak ingress controller'dır. Tek dezavantajı, Kong, HTTP trafiği için yük dengeleyici olarak kullanıldığında optimizasyon eksikliğidir, ancak ayarları düzeltebilir veya çalışması için gerekli eklentileri kullanabilirsiniz.

### [Traefik](https://traefik.io/)

![Traefik]({{ "/assets/img/trafeik.png" | absolute_url }})

Traefik, geliştirici topluluğunda çok fazla popüler hale geldi ve bunun için bazı nedenler var. Yeni başlayanlar için, herhangi bir Kubernetes ortamının kullanımı ve entegrasyonu çok kolaydır. Dinamik arka uç hizmeti ve TCP, HTTP, HTTPS ve GRPC için tam destek alırsınız. Yük dengeleme için round robin ve ağırlıklı round robin destekler. [Let's Encrypt](https://letsencrypt.org/) desteğinge sahip olması büyük bir avantajdır.

Traefik'i kullanırken herhangi bir sorunla karşılaşırsanız, bu giriş kontrolörü için sunulan premium desteğe güvenebilirsiniz.

### [HAProxy Ingress](https://github.com/jcmoraisjr/haproxy-ingress)

![HAProxy Ingress]({{ "/assets/img/haproxy.webp" | absolute_url }})

HAProxy, proxy sunucusu ve yük dengeleyici olarak bilinir. Genel olarak, geliştiriciler, tüketilen kaynaklarda yüksek hıza, optimizasyona ve verimliliğe önem verir. HAProxy'nin avantajlarından biri, çok sayıda desteklenen balans algoritmasıdır.

Bazı ince ayarlarla, HTTP trafiğinden TCP yük dengelemeye kadar her şeyi işleyebilen yetenekli bir Kubernetes kümesini çalıştırabilirsiniz. HAProxy monitoring için Grafana veya Datadog kullanabilirsiniz.

### [Istio Ingress Gateway](https://istio.io/docs/tasks/traffic-management/ingress/)

![Istio]({{ "/assets/img/istio.png" | absolute_url }})

Istio - IBM, Google ve Lyft'in ortak projesi olarak oluşturulan (Envoy'un orijinal yazarları) - kapsamlı bir servis ağı çözümüdür. Sadece gelen tüm dış trafiği (Giriş denetleyicisi olarak) değil, küme içindeki tüm trafiği de kontrol edebilir. Istio Envoy'u bir sidecar-proxy olarak kullanıyor. Temel olarak maksimum kontrol, genişletilebilirlik, güvenlik ve şeffaflıktır. Istio Ingress ile trafik yönlendirme, servisler arasında erişim, dengeleme, izleme ve daha pek çok konuda ince ayar yapabilirsiniz.

[Jaeger](https://www.jaegertracing.io/) veya [Zipkin UI](https://zipkin.io/) kullandığınızda da takip edebilirsiniz.

### [Nginx](https://github.com/kubernetes/ingress-nginx)

![Nginx]({{ "/assets/img/nginx.png" | absolute_url }})

Kendi giriş denetleyicinizi yapılandırma konusunda endişelenmek istemiyorsanız, bu ingress en güvenli seçimdir. Nginx'te göz önünde bulundurulması gereken bir şey, ConfigMaps aracılığıyla, güçlü ve ayrıntılı Virtualhost Trafik İstatistikleri (VTS) ve farklı başlık seçeneklerinin etkinleştirilmesi vb. gibi, bölmenin içinde çalışan Nginx web sunucusuna çok fazla ayar yapabiliriz. Nginx, genelde "varsayılan" giriş denetleyicisi olarak seçilir, ancak daha kapsamlı özellikler için geliştirme ortamındaki diğer denetleyicilere bakabilirsiniz.

### [Voyager](https://github.com/appscode/voyager)

![Voyager]({{ "/assets/img/voyager.png" | absolute_url }})

Voyager, HAProxy temelli ve çok sayıda sağlayıcı için bir çözüm olarak sunulur. Kayda değer özellikleri arasında L7 ve L4'teki trafik dengelemesi var. Elbette, Voyager’ın TCP L4 trafik dengelemesi bu çözümün temel özelliklerinden biri olarak adlandırılabilir. Tam HTTP/2 ve gRPC protokolleri desteği ve Let’s Encrypt sertifika yönetimine de sahiptir.

### [Contour](https://github.com/heptio/contour)

![Contour]({{ "/assets/img/contour.png" | absolute_url }})

Contour sadece Envoy'a dayanmaktadır. Ingress kaynaklarını özel CRD(IngressRoute adı verilen API) ile yönetebilme özelliğine sahiptir. Aynı anda bir kümeyi kullanan birden fazla geliştirme ekibine sahip kuruluşlar için bu, komşu ortamlardaki trafiğin korunmasına ve Giriş kaynaklarında değişiklik olduğunda ortaya çıkan hatalardan korunmasına yardımcı olur. 

Ayrıca genişletilmiş bir dengeleme algoritması trafik akışı ve arızalarının detaylı izlenmesini sağlar. Belki de bazıları için **sticky-session desteği olmaması** ciddi bir kusur olabilir.

### [Ambassador](https://github.com/datawire/ambassador)

![Ambassador]({{ "/assets/img/ambassador.png" | absolute_url }})

Ambassador, Envoy merkezli başka bir çözüm. Ücretsiz ve ticari sürümleri vardır. Ambassador, "Mikro hizmetler için Kubernetes-yerel API ağ geçidi" (Kubernetes-native API gateway for microservices) olarak tanımlanmaktadır. 

Bir Ingress controller'dan beklediğiniz temel özelliklere sahip olduğu gibi, çeşitli servis ağ çözümleriyle de kullanılabilir (Consul, Linkerd, Istio). Ambassador, HAProxy ve NGINX'in performans karşılaştırmasına [buradan](https://www.getambassador.io/resources/envoyproxy-performance-on-k8s/) ulaşabilirsiniz.

[Kaynak 1](https://github.com/datawire/ambassador)    
[Kaynak 2](https://github.com/kubernetes/ingress-nginx)    
[Kaynak 3](https://caylent.com/kubernetes-top-ingress-controllers)    
[Kaynak 4](https://medium.com/flant-com/comparing-ingress-controllers-for-kubernetes-9b397483b46b)    
[Kaynak 5](https://kubernetes.io/docs/concepts/services-networking/ingress/)    