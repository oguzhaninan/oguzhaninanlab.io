---
layout: post
title: "Kubernetes Service Mesh Araçları"
author: "Oğuzhan İNAN"
date: 2019-10-18
categories: docker kubernetes ingress service mesh istio
---

![mesh]({{ "/assets/img/service-mesh-tools.png" | absolute_url }})

## Service Mesh Araçları Ne Yapar?

Bir Kubernetes ortamında servis ağını yönetmek için kullanabileceğiniz çeşitli araçları bulmaktan çok, ne yapıp ve yapamayacağını anlamak daha önemlidir. Service Mesh genellikle gelişmiş bir esneklik sağlar. Bir mikro hizmet başarısızlığı durumunda, Service Mesh, tüm uygulamanın çalışır durumda kalmasını sağlamaya çalışır. 

Bu özellik, mikro servislerin yükünü ve aralarındaki iletişimi dengelemektir. Ana işlevi iletişim kontrolüdür. İstek yönlendirmesini, örnekleri ve iletişim uç noktalarını kontrol eder. Bir CI/CD döngüsünde Service Mesh, konteynerlerin manuel olarak yeniden yapılandırılması ihtiyacını ortadan kaldırır. Service Mesh'in kullanım durumunuz için ne kadar yararlı olabileceğini anladıkça, bu katmanda daha fazla araç kullanabilirsiniz.

### [Linkerd](https://linkerd.io/)

![linkerd]({{ "/assets/img/linkerd.png" | absolute_url }})

Linkerd, "Service Mesh" terimini kullanan ilk araç olarak tanınır ve popüler olma nedenlerinden biri de budur. Service Mesh aracı, Twitter'ın [Finagle](https://twitter.github.io/finagle/) Kütüphanesi kullanılarak geliştirilmiştir ve Scala'da yazılmıştır. 

Linkerd, saniyede büyük miktarda istek yerine getirebilir, ayrıca hizmet ağını birden çok düğüm kullanarak ölçekleme olanağınız vardır. Docker ve Kubernetes de dahil olmak üzere birden fazla konteyner mimarisiyle uyumludur. Tek bir düğüm gerektiren küçük bir uygulama kullanıyorsanız, Linkerd, gidilecek en etkili seçenek olabilir. TCP isteklerini ve websocket desteklemiyor, ancak üstün trafik kontrolü ve başka bir kümedeki harici servislere bağlanma yeteneğine sahip oluyorsunuz. Bu avantajlar Linkerd'i farklı konteynerler çalıştıran çoklu kümeleri yönetmek için ideal yapıyor. Aslında Linkerd, yönetmeye çalıştığınız ortam buysa tek seçenek olabilir.

### [Linkerd2](https://github.com/linkerd/linkerd2/releases)

Linkerd2 Kubernetes için sıfırdan yazılmıştır. Linkerd gibi Scala ve JVM kullanmak yerine, Linkerd2 Golang ve Rust dilinde yazılmıştır. Ölçeklenebilirlik ve performans açısından daha fazla esneklik sağlar. Bu, bellek yönetiminde önemli bir fayda sağlıyor.

Linkerd2’nin ana avantajı, veri ve kontrol düzlemlerinin sıkı yapılandırılmasıdır. Linkerd2 geniş bir konfigürasyon sahip. Bununla birlikte, Linkerd2 hala başlangıç ​​aşamasındadır, bu nedenle diğer daha olgun Service Mesh araçlarıyla aynı gelişmiş özellikleri elde edemeyebilirsiniz. 

Örneğin, Linkerd2 henüz yerel olarak şifrelemeyi desteklemiyor. Dahili bir şifreleme özelliği vardır, ancak yine de deneyseldir ve yerel şifreleme ile aynı düzeyde güvenlik sağlayamayabilir. Ayrıca trafik kontrol özellikleri yoktur ve harici kümeler için desteği yoktur. Linkerd2 yetişiyor. Kayda değer bir şey, seçtikleri hizmet ağı olarak Linkerd2 üzerinde çalışan dev geliştiriciler topluluğu. Takımın çok çeşitli özellikler sunması zaman alabilir, ancak gelecek Linkerd2 için parlak.

### [Consul Connect](https://learn.hashicorp.com/consul/getting-started/connect)

![Consul]({{ "/assets/img/consul-connect.jpg" | absolute_url }})

Consul, Kubernetes için bir başka popüler Service Mesh aracıdır. Son derecede stabil ve servis-hizmet iletişimini yönetmek için doğru özellikleri sunar; bu nedenle yöneticiler ve geliştiriciler arasında yaygın olarak kullanılır. Kubernetes de dahil olmak üzere farklı konteyner ortamlarını destekler. Consul tek bir entegre araç olarak konumlandırılmıştır. Birden fazla hizmete sahip iki veya daha fazla Kubernetes örneği varsa (ve her ikisi de Consul ile çalışıyorsa) bu hizmetleri yapılandırmak için  iletişim kurmasını sağlayabilirsiniz. 

Varsayılan data-proxy olarak Envoy'u destekliyor, ayrıca kullanıcı dostu bir yapılandırma arayüzüne sahip; Consul, geniş bulut yönetimi deneyimi olmayan birçok geliştiriciye uyacak bir Service Mesh aracıdır. Consul da göreceli olarak yeni olduğu için, bu araca eklenen daha birçok iyileştirme yapılmaya devam ediyor.


### [Istio](https://istio.io/)

![Consul]({{ "/assets/img/istio.png" | absolute_url }})

Hiç şüphe yok ki Istio, tüm Service Mesh araçlarının en bilineni ve en çok kullanılanıdır. Istio, IBM, Google ve Lyft dahil olmak üzere Kubernetes de en iyi isimlerle tamamen desteklenir; Google’ın, büyük ölçekli kullanım için güvenilir olmayan bir Service Mesh aracını desteklediğini düşünmek zor. 

Varsayılan data-proxy olarak Envoy'u destekliyor. Büyük bir dezavantajı, küme dışı güvenli bağlantılar için destek eksikliğidir. Aynı zamanda kullanımı en kolay servis ağ aracı değildir; Bu servis ağından tam olarak yararlanmadan önce Istio'yu nasıl optimize edeceğinizi ve özelliklerini nasıl kullanacağınızı öğrenmek için biraz araşatırma gerekiyor. Istio'yu daha erişilebilir hale getirmek için, varsayılan yapılandırma çoğu durumda yeterince iyidir. Bir kubernetes örneğini minikube, [Helm](https://helm.sh/) ve Istio kullanarak tek bir adımda yapılandırabilirsiniz.


[Kaynak 1](https://learn.hashicorp.com/consul/getting-started/connect)    
[Kaynak 2](https://linkerd.io/)    
[Kaynak 3](https://caylent.com/kubernetes-top-ingress-controllers)    
[Kaynak 4](https://github.com/linkerd/linkerd2/releases)    
[Kaynak 5](https://istio.io/)    
