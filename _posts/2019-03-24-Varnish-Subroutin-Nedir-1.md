---
layout: post
title: "Varnish Subroutin Nedir - 1"
author: "Oğuzhan İNAN"
date: 2019-03-24
categories: linux varnish varnishd subroutin
---

Aşağıdaki çizelgede Varnish'in bir HTTP isteğini nasıl karşıladığı gösterilmiştir. 

![varnish subroutines]({{ "/assets/img/varnish_subroutines.png" | absolute_url }})

### vcl_recv
Bu subroutin isteği ilk karşıladığı yerdir. Burada gelen isteği bazı kontrollerden geçirebilir veya isteği istediğimiz hale getirebiliriz. Örneğin _Host_ header değerindeki www değerini kaldırabiliriz. Başka bir örnek gelen istek HTTPS değilse isteği HTTPS olarak tekrar gönderebiliriz. vcl_recv içinde kullanabileceğimiz eylemler:   
- __pass__   
Bu eylem kullanıldığında istek önbellekte aranmaz, ancak nesneyi arka uçtan almak için Varnish akışının kalanını gerçekleştirir. “Pass” işaretli bir istek, kullanıcıya arka uçtan teslim edildiğinde önbellekte saklanmayacak ve bu yanıt kişiselleştirilmiş bilgiler gibi önbelleğe almak istemediğiniz nesneler için kullanılacaktır. Örneğin GET ve HEAD istekleri hariç önbelleğe alınmaması istendiğinde gelen istek POST ise __return (pass)__ yapılarak isteğin önbelleğe alınmaması gerektiği belirtilir.

- __pipe__   
Bu eylem ile yapılan talepler doğrudan arka uca (backend) gider ve önbelleğe alınmaz. Varnish akışını atlayarak diğer subroutinlere uğramadan arka uca ulaşır. Bir istek pipe'lanırsa aynı bağlantıyı kullanan (keep-alive) diğer isteklerde pipe'lanır. Örneğin bir müzik akışı olabilir.

- __hash__    
Varnish'in arama eylemidir. Varnish her isteğe benzersiz bir kimlik atar. Bu eylem önbellekte aranması için vcl_hash subroutinine gönderir.

-  __synth__   
Bu, bir hata mesajı gibi Varnish'ten bir yanıt oluşturmak veya vcl_synth subroutinin de belirtilen istekleri yönlendirmek için kullanılır. Belirli isteklere erişimi reddetmek içinde kullanılabilir. Örneğin belli IP adreslerini bloklamak istediğinizde kullanılabilir.

Belirli IP adreslerini engellemek için örnek bir kullanım:   
```
acl istenmeyen_ipler { 
    "69.60.116.0"/24;
    "69.90.119.207";
}

sub vcl_recv {
    if (client.ip ~ istenmeyen_ipler) {
        return(synth(403, "Erişim engellendi."));
    }
}
```

Varnish'de kendi subroutinlerimizide oluşturabiliriz.
```
sub check_access {
    ....
}
```

Kullanmak için ise `call check_access;`kullanılır.