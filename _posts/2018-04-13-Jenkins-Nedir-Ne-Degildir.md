---
layout: post
title: "Jenkins Nedir Ne Değildir"
author: "Oğuzhan İNAN"
date: 2018-04-13
categories: java jenkins ci
---

![Jenkins]({{ "/assets/img/jenkins.jpg" | absolute_url }})
## Jenkins Nedir?   
Jenkins, Continuous Integration amacıyla oluşturulmuş eklentilerle Java dili ile yazılmış açık kaynaklı bir otomasyon aracıdır. Jenkins, yazılım projelerinizi sürekli olarak geliştirip test etmekte ve geliştiricilerin projeye değişiklikleri entegre etmelerini kolaylaştırır. Ayrıca, çok sayıda test ve dağıtım teknolojisiyle entegre ederek yazılımınızı sürekli olarak test etmenizi sağlar.

Jenkins ile yazılım geliştiriciler, geliştirme sürecini otomasyon yoluyla hızlandırabilirler. Jenkins, build, documentation, test, paket, dağıtım, statik analiz ve çok daha fazlası dahil olmak üzere her türlü geliştirme yaşam döngüsü süreçlerini entegre eder.   
  
![Jenkins Integration]({{ "/assets/img/jenkins-integration.jpg" | absolute_url }})

### Jenkins Avantajları:
* Ücretsizdir.
* Kolay kurulum.
* Topluluk desteği ile açık kaynak kodlu bir araçtır
* İşleri kolaylaştırmak için 1000+ eklentiye sahiptir. Aranan eklenti yoksa, oluşturularak toplulukla paylaşılabilir.
* Java ile geliştirildiği için tüm platformlarda çalışabilir.


### Jenkins ile Continuous Integration (Sürekli Entegrasyon)   

* İlk olarak, bir geliştirici yaptığı değişikliği git deposuna gönderirir. Bu arada, Jenkins değişiklikleri algılamak için düzenli olarak git deposunu kontrol eder.
* Jenkins değişiklikleri algıladıktan sonra değişiklikleri çekecek ve build sürecini başlatacak.
* İşlem başarısız olursa değişikliği yapan geliştirici bilgilendirilecek.
* Eğer bir sorun olmamışsa, o zaman Jenkins gerekli testleri başlatır.
* Test edildikten sonra, Jenkins bir geri bildirim üretir ve daha sonra test sonuçları hakkında geliştiricileri bilgilendirir.
* Bu süreç her değişiklik için tekrarlanır.

| Jenkins'den önce | Jenkins'den sonra |
|------------------|-------------------|
|Tüm kaynak kod oluşturulur ve daha sonra test edilirdi. Oluşturulma ve test başarısızlığı durumunda hataları bulma ve düzeltme zor ve uzun bir süreçti. | Kaynak kodda yapılan her değişiklik build edilmiş ve test edilmiştir. Bu nedenle bir sorun olduğunda ilgili geliştirici kendi kodunu kontrol eder. |
|Geliştiriciler test sonuçlarını beklemek zorundaydı. | Geliştiriciler, yapılan her değişikliğin test sonucunu bilirler. |
|Tüm süreç manueldi. | Sadece kodu değiştirmeniz yeterli, Jenkins tüm süreci üstlenir. | 

